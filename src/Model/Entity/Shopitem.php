<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Shopitem Entity
 *
 * @property int $id
 * @property string $itemtype
 * @property string $itemname
 * @property string $storeid
 * @property int $coin
 * @property int $more
 * @property int $discount
 * @property int $price
 * @property int $priceafterdiscount
 * @property string $currency
 * @property string $storename
 * @property int $consumable
 * @property int $newitem
 * @property int $active
 * @property int $sort
 */
class Shopitem extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'itemtype' => true,
        'itemname' => true,
        'storeid' => true,
        'coin' => true,
        'more' => true,
        'discount' => true,
        'price' => true,
        'priceafterdiscount' => true,
        'currency' => true,
        'storename' => true,
        'consumable' => true,
        'newitem' => true,
        'active' => true,
        'sort' => true,
    ];
}
