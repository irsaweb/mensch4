<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Clan Entity
 *
 * @property int $id
 * @property int $require_trophy
 * @property string $name
 * @property string|null $description
 * @property string $badge
 * @property int $trophy
 * @property int $member
 * @property int $promote
 *
 * @property \App\Model\Entity\Userclankicked[] $userclankickeds
 * @property \App\Model\Entity\Userclan[] $userclans
 */
class Clan extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'require_trophy' => true,
        'name' => true,
        'description' => true,
        'badge' => true,
        'trophy' => true,
        'member' => true,
        'promote' => true,
        'friendlymatches' => true,
        'userclankickeds' => true,
        'userclans' => true,
    ];
}
