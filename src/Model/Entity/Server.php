<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Server Entity
 *
 * @property int $id
 * @property string $ip
 * @property int $pid
 * @property int $port
 * @property int $starttime
 * @property int $time
 * @property int $networkversion
 * @property int $test
 * @property int $backup
 * @property int $close
 * @property int $active
 *
 * @property \App\Model\Entity\Match[] $matches
 */
class Server extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ip' => true,
        'pid' => true,
        'port' => true,
        'starttime' => true,
        'time' => true,
        'networkversion' => true,
        'test' => true,
        'backup' => true,
        'close' => true,
        'active' => true,
        'matches' => true,
    ];
}
