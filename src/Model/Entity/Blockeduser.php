<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Blockeduser Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $blocked_user_id
 * @property int $time
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\User $blocked
 */
class Blockeduser extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'blocked_user_id' => true,
        'time' => true,
        'user' => true,
        'blocked_user' => true,
    ];
}
