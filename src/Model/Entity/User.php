<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property int $group_id
 * @property string|null $unique_id
 * @property string|null $phone_number
 * @property string|null $unique_id_sms
 * @property string $gmail_account
 * @property string|null $name
 * @property string|null $uuid
 * @property int $coin
 * @property int $xp
 * @property int $vip
 * @property int $mute
 * @property int $in_game_chat
 * @property int $promote_clan
 * @property int $dailytrophy
 * @property int $weeklytrophy
 * @property int $totaltrophy
 * @property int $colour4
 * @property int $colour6
 * @property string $dice
 * @property string $symbol
 * @property string $ring
 * @property int $spintime
 * @property int $spinvideo
 * @property bool $telegram_claimed
 * @property bool $instagram_claimed
 * @property string $language
 * @property int $registerdate
 * @property int $lastlogindate
 * @property int $promotiondate
 * @property int $ads_watched
 * @property string|null $promotion_pack
 * @property int $tutorial_step
 * @property int $tutorial_item
 * @property bool $tutorial_finish
 * @property string $ip
 *
 * @property \App\Model\Entity\Group $group
 * @property \App\Model\Entity\Finance[] $finances
 * @property \App\Model\Entity\Userclankicked[] $userclankickeds
 * @property \App\Model\Entity\Userclan|null $userclan
 * @property \App\Model\Entity\Usermatch|null $usermatch
 * @property \App\Model\Entity\Bank|null $bank
 * @property \Cake\ORM\Entity[] $friendsecondusers
 * @property \Cake\ORM\Entity[] $friendrequesterusers
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'group_id' => true,
        'unique_id' => true,
        'phone_number' => true,
        'unique_id_sms' => true,
        'gmail_account' => true,
        'name' => true,
        'uuid' => true,
        'coin' => true,
        'xp' => true,
        'vip' => true,
        'mute' => true,
        'in_game_chat' => true,
        'promote_clan' => true,
        'dailytrophy' => true,
        'weeklytrophy' => true,
        'totaltrophy' => true,
        'colour4' => true,
        'colour6' => true,
        'dice' => true,
        'symbol' => true,
        'ring' => true,
        'spintime' => true,
        'spinvideo' => true,
        'telegram_claimed' => true,
        'instagram_claimed' => true,
        'language' => true,
        'registerdate' => true,
        'lastlogindate' => true,
        'promotiondate' => true,
        'ads_watched' => true,
        'promotion_pack' => true,
        'tutorial_step' => true,
        'tutorial_item' => true,
        'tutorial_finish' => true,
        'ip' => true,
        'group' => true,
        'unique' => true,
        'awards' => true,
        'banks' => true,
        'blockedusers' => true,
        'finances' => true,
        'logs' => true,
        'reports' => true,
        'userclankickeds' => true,
        'userclans' => true,
        'usermatches' => true,
        'usernotifications' => true,
    ];
}
