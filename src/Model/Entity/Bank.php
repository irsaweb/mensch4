<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bank Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $money
 * @property string|null $fishbank
 * @property int $timereg
 * @property string $paidtime
 * @property string $bankname
 * @property string|null $tracecode
 * @property string|null $accountnum
 * @property string|null $item_store_id
 * @property int $watch_video
 * @property string|null $store_name
 * @property string|null $token
 * @property int $del
 * @property string $ip
 * @property int $state
 *
 * @property \App\Model\Entity\User $user
 */
class Bank extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'money' => true,
        'fishbank' => true,
        'timereg' => true,
        'paidtime' => true,
        'bankname' => true,
        'tracecode' => true,
        'accountnum' => true,
        'item_store_id' => true,
        'watch_video' => true,
        'store_name' => true,
        'token' => true,
        'del' => true,
        'ip' => true,
        'state' => true,
        'user' => true,
        'item_store' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token',
    ];
}
