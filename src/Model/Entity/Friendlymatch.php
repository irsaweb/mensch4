<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Friendlymatch Entity
 *
 * @property int $id
 * @property int $match_id
 * @property int $owner_id
 * @property int $clan_id
 * @property int $networkversion
 * @property int $time
 * @property int $state
 *
 * @property \App\Model\Entity\Match $match
 * @property \App\Model\Entity\Clan $clan
 * @property \App\Model\Entity\User $user
 */
class Friendlymatch extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'match_id' => true,
        'owner_id' => true,
        'clan_id' => true,
        'networkversion' => true,
        'time' => true,
        'state' => true,
        'match' => true,
        'owner' => true,
        'clan' => true,
    ];
}
