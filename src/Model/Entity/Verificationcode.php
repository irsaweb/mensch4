<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Verificationcode Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $phone_number
 * @property int $code
 * @property int $time
 *
 * @property \App\Model\Entity\User $user
 */
class Verificationcode extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'phone_number' => true,
        'code' => true,
        'time' => true,
        'user' => true,
        'fail_try' => true
    ];
}
