<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Match Entity
 *
 * @property int $id
 * @property int $server_id
 * @property string $type
 * @property string $table_type
 * @property string $group_mode
 * @property int $player
 * @property int $require_player
 * @property int $starttime
 * @property int $endtime
 * @property int $state
 *
 * @property \App\Model\Entity\Server $server
 * @property \App\Model\Entity\Usermatch[] $usermatches
 * @property \App\Model\Entity\Friendlymatch $friendlymatch
 */
class Match extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'server_id' => true,
        'type' => true,
        'table_type' => true,
        'group_mode' => true,
        'player' => true,
        'require_player' => true,
        'starttime' => true,
        'endtime' => true,
        'state' => true,
        'server' => true,
        'finances' => true,
        'friendlymatches' => true,
        'usermatches' => true,
    ];
}
