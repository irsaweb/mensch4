<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Report Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $reported_user_id
 * @property int $reported_clan_id
 * @property string $issue
 * @property string $content
 * @property int $time
 * @property int $state
 *
 * @property \App\Model\Entity\User $user
 */
class Report extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'reported_user_id' => true,
        'reported_clan_id' => true,
        'issue' => true,
        'content' => true,
        'time' => true,
        'state' => true,
        'user' => true,
        'reported_user' => true,
        'reported_clan' => true,
    ];
}
