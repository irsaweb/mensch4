<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Usermatch Entity
 *
 * @property int $id
 * @property int $match_id
 * @property int $user_id
 * @property string|null $table_type
 * @property int $group_number
 * @property int $in_game_chat
 * @property string|null $name
 * @property string $status
 * @property int $rank
 * @property int $state
 *
 * @property \App\Model\Entity\Match $match
 * @property \App\Model\Entity\User $user
 */
class Usermatch extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'match_id' => true,
        'user_id' => true,
        'table_type' => true,
        'group_number' => true,
        'in_game_chat' => true,
        'name' => true,
        'status' => true,
        'rank' => true,
        'state' => true,
        'match' => true,
        'user' => true,
    ];
}
