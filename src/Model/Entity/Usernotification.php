<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Usernotification Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $event
 * @property int $time
 * @property int $push
 * @property int $state
 *
 * @property \App\Model\Entity\User $user
 */
class Usernotification extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'event' => true,
        'time' => true,
        'push' => true,
        'state' => true,
        'user' => true,
    ];
}
