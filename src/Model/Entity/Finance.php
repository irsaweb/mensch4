<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Finance Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $match_id
 * @property int $amount
 * @property int $newamount
 * @property string $currency
 * @property string|null $description
 * @property string $type
 * @property string $service
 * @property int $time
 *
 * @property \App\Model\Entity\User $user
 */
class Finance extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'match_id' => true,
        'amount' => true,
        'newamount' => true,
        'currency' => true,
        'description' => true,
        'type' => true,
        'service' => true,
        'time' => true,
        'user' => true,
        'match' => true,
    ];
}
