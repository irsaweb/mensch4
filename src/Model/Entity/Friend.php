<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Friend Entity
 *
 * @property int $id
 * @property int $requester_user_id
 * @property int $second_user_id
 * @property int $date
 * @property int $state
 *
 * @property \App\Model\Entity\User $requesteruser
 * @property \App\Model\Entity\User $seconduser
 */
class Friend extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'requester_user_id' => true,
        'second_user_id' => true,
        'date' => true,
        'state' => true,
        'requester_user' => true,
        'second_user' => true,
    ];
}
