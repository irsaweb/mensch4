<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Userclan Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $clan_id
 * @property string $role
 * @property string $pass
 * @property int $repair
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Clan $clan
 */
class Userclan extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'clan_id' => true,
        'role' => true,
        'pass' => true,
        'repair' => true,
        'user' => true,
        'clan' => true,
    ];
}
