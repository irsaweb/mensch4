<?php

namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\Cache\Cache;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\EntityInterface;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\GroupsTable&\Cake\ORM\Association\BelongsTo $Groups
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class AuthTable extends UsersTable
{

    /**
     * @var array
     */
    protected $_newUserSettings = [
        'group_id' => 5,
        'coin' => 200,
        'language' => 'fa',
        'gmail_account' => '',
        'lastlogindate' => '0'
    ];


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);
    }

    /**
     * @return string
     */
    public static function getClientIP()
    {
        return \Cake\Routing\Router::getRequest()->clientIp();
    }

    /**
     * @return EntityInterface
     */
    protected function getNewUserEntity(): EntityInterface
    {
        $newUser = $this->newEntity($this->_newUserSettings, ['validate' => false]);
        $newUser->registerdate = time();
        $newUser->ip = self::getClientIP();
        return $newUser;
    }


    /**
     * @param string $email
     * @return User
     */
    public function userRegisterByEmail(string $email)
    {
        $user = $this->getNewUserEntity();
        $user->gmail_account = $email;
        $user->registerdate = time();

        $this->saveOrFail($user);
        return $user->id;
    }


    /**
     * @param string $phoneNumber
     * @return User
     */
    public function userRegisterByPhone(string $phoneNumber)
    {
        $user = $this->getNewUserEntity();
        $user->phone_number = $phoneNumber;
        $user->registerdate = time();

        $this->saveOrFail($user);
        return $user->id;
    }


    /**
     * @param string $uniqueId
     * @return User
     */
    public function userRegisterByUniqueID(string $uniqueId)
    {
        $user = $this->getNewUserEntity();
        $user->unique_id = $uniqueId;
        $user->registerdate = time();

        return $this->saveOrFail($user);

    }

    /**
     * @param string $email
     * @return array|EntityInterface|null
     */
    public function userExistByEmail(string $email)
    {
        return $this->find()->select(['id', 'language', 'group_id'])->where(['gmail_account' => $email])->first();
    }

    /**
     * @param string $phoneNumber
     * @return array|EntityInterface|null
     */
    public function userExistByPhone(string $phoneNumber)
    {

        return $this->find()
            ->select(['id', 'language', 'group_id', 'unique_id_sms', 'xp'])
            ->where(['phone_number' => $phoneNumber])->first();
    }


    /**
     * @param string $uniqueId
     * @return array|EntityInterface|null
     */
    public function userExistByUniqueID(string $uniqueId)
    {
        return $this->find()->select(['id', 'language', 'group_id'])->where(['unique_id' => $uniqueId])->first();
    }


    /**
     * @param $userID
     * @param $email
     * @param false $uniqueIdClean
     * @return User|false
     */
    public function userLinkToEmail($userID, $email, $uniqueIdClear = true)
    {
        $User = $this->get($userID);
        $User->gmail_account = $email;
        if ($uniqueIdClear) {
            $User->unique_id = null;
        }
        return $this->save($User);
    }

    /**
     * @param $userID
     * @param $phoneNumber
     * @param false $uniqueIdClean
     * @return User|false
     */
    public function userLinkToPhone($userID, $phoneNumber, $uniqueIdSms, $uniqueIdClear = true)
    {
        $User = $this->get($userID);
        $User->phone_number = $phoneNumber;
        $User->unique_id_sms = $uniqueIdSms;
        if ($uniqueIdClear) {
            $User->unique_id = null;
        }
        return $this->save($User);
    }

    /**
     * @param $userID
     * @return int
     */
    public function userUnLinkToPhone($userID)
    {
        return $this->updateAll(['phone_number' => null], ['id' => $userID]);
    }

    /**
     * @param $userID
     * @return int
     */
    public function userUnLinkToEmail($userID)
    {
        return $this->updateAll(['gmail_account' => null], ['id' => $userID]);
    }

    /**
     * @param $userID
     * @return int
     */
    public function setLastLoginIn($userID)
    {
        return $this->updateAll([
            'ip' => self::getClientIP(),
            'lastlogindate' => time()
        ], [
            'id' => $userID
        ]);

    }
}
