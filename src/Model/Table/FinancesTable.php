<?php

namespace App\Model\Table;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Finances Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Finance get($primaryKey, $options = [])
 * @method \App\Model\Entity\Finance newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Finance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Finance|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Finance saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Finance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Finance[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Finance findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Finance newEmptyEntity()
 * @method \App\Model\Entity\Finance[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Finance[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Finance[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Finance[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class FinancesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('finances');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('amount')
            ->requirePresence('amount', 'create')
            ->notEmptyString('amount');

        $validator
            ->integer('newamount')
            ->requirePresence('newamount', 'create')
            ->notEmptyString('newamount');

        $validator
            ->scalar('currency')
            ->requirePresence('currency', 'create')
            ->notEmptyString('currency');

        $validator
            ->scalar('description')
            ->maxLength('description', 250)
            ->allowEmptyString('description');

        $validator
            ->scalar('type')
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('service')
            ->requirePresence('service', 'create')
            ->notEmptyString('service');

        $validator
            ->integer('time')
            ->requirePresence('time', 'create')
            ->notEmptyString('time');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }


    /**
     * @return int Returns the number of affected rows.
     * @version 1.0.0
     */
    public function financeRemover()
    {
        $finance = $this->find('all')
            ->where(['time <' => (time() - 24 * 3600 * 30)])
            ->select(['id'])
            ->limit(100)
            ->disableHydration()
            ->toArray();

        $ids = Hash::extract($finance, '{n}.id');
        return $this->deleteAll(['id IN' => $ids]);
    }

    /**
     * @param $userId
     * @param $matchId
     * @return int
     * @version 1.0.0
     */
    public function matchEntranceFee($userId, $matchId)
    {
        $entrance = $this->find('all')
            ->where(['user_id' => $userId, 'match_id' => $matchId])
            ->select(['amount'])
            ->first();

        $entranceFee = 0;
        if ($entrance) {
            $entranceFee = $entrance->amount;
        }
        return $entranceFee;
    }

    /**
     * @param $userIds
     * @param $startTime
     * @param $endTime
     * @return array
     * @version 1.0.0
     */
    public function financeReport($userIds, $startTime, $endTime)
    {

        $financeInfos = $this->find()
            ->where(function (QueryExpression $exp) use ($userIds, $startTime, $endTime) {
                return $exp->in('user_id', $userIds)
                    ->between('time', $startTime, $endTime);
            })
            ->disableHydration()
            ->toArray();

        $adsReport = $this->adsReport($userIds, $financeInfos);
        $matchReport = $this->matchReport($userIds, $financeInfos);

        return ['adsReport' => $adsReport, 'matchReport' => $matchReport];
    }

    /**
     * @param $userIds
     * @param $financeInfos
     * @return array
     * @version 1.0.0
     */
    public function adsReport($userIds, $financeInfos)
    {
        $watchedSpinAds = [];
        foreach ($financeInfos as $key => $financeInfo) {

            if ($financeInfo['service'] == 'spin_ads') {
                $watchedSpinAds[] = $financeInfo['user_id'];
            } elseif ($financeInfo['service'] == 'watch_ads') {
                $watchedAds[] = $financeInfo['user_id'];
            }
            $usersWatchedAds[] = $financeInfo['user_id'];
        }

        $userIdsWatchAdsUnique = array_unique($usersWatchedAds);
        $userIdsDontWatchAds = array_diff($userIds, $userIdsWatchAdsUnique);
        $userIdsWatchAdsUniqueCount = count($userIdsWatchAdsUnique);
        $userIdsDontWatchAdsUniqueCount = count($userIdsDontWatchAds);

        $watchAdsCountStatistics = array();
        foreach ($usersWatchedAds as $key => $userWatchedAds) {
            if (isset($watchAdsCountStatistics[$userWatchedAds])) {
                $watchAdsCountStatistics[$userWatchedAds]++;
            } else {
                $watchAdsCountStatistics[$userWatchedAds] = 1;  //initialize
            }
        }

        arsort($watchAdsCountStatistics);
        $howManyAdsWatched = count($watchedAds);
        $howManySpinAdsWatched = count($watchedSpinAds);
        $totalWatchedAds = $howManyAdsWatched + $howManySpinAdsWatched;

        $adsReport = array(
            'userWatched' => $userIdsWatchAdsUniqueCount,
            'userDontWatched' => $userIdsDontWatchAdsUniqueCount,
            'watchAdsRate' => number_format($userIdsWatchAdsUniqueCount / count($userIds), 2) * 100,
            'adsWatched' => $howManyAdsWatched,
            'spinAdsWatched' => $howManySpinAdsWatched,
            'totalWatchedAds' => $totalWatchedAds,
            'avgWatchAds' => number_format($totalWatchedAds / count($userIds), 2),
            'watchAdsCountStatistics' => $watchAdsCountStatistics
        );
        return $adsReport;
    }

    /**
     * @param $userIds
     * @param $financeInfos
     * @return array
     * @version 1.0.0
     */
    private function matchReport($userIds, $financeInfos)
    {
        foreach ($financeInfos as $key => $financeInfo) {

            if ($financeInfo['service'] == 'entrance_fee') {
                $userIdFinance = $financeInfo['user_id'];
                $takeApartMatchUserIds[] = $userIdFinance;
                $takeApartMatch[$userIdFinance][] = $financeInfo['description'];

                if (isset($takeApartMatch[$userIdFinance][$financeInfo['description']])) {
                    $takeApartMatch[$userIdFinance][$financeInfo['description']]++;
                } else {
                    $takeApartMatch[$userIdFinance][$financeInfo['description']] = 1;  //initialize
                }
            }
        }

        $howManyTakeApartMatchUserIdsUnique = array_unique($takeApartMatchUserIds);
        $userIdsDontTakeApartMatchUnique = array_diff($userIds, $howManyTakeApartMatchUserIdsUnique);
        $howManyTakeApartMatchUserIdsUniqueCount = count($howManyTakeApartMatchUserIdsUnique);
        $userIdsDontTakeApartMatchUniqueCount = count($userIdsDontTakeApartMatchUnique);

        $dupesMatch = array_diff_key($takeApartMatchUserIds, $howManyTakeApartMatchUserIdsUnique);
        $matchCountStatistics = array_count_values($dupesMatch);

        arsort($matchCountStatistics);
        $totalTakeApartMatch = array_sum($matchCountStatistics);

        $matchReport = array(
            'userTakeApartMatch' => $howManyTakeApartMatchUserIdsUniqueCount,
            'userDontTakeApartMatch' => $userIdsDontTakeApartMatchUniqueCount,
            'takeApartMatchRate' => number_format($howManyTakeApartMatchUserIdsUniqueCount / count($userIds), 2) * 100,
            'totalTakeApartMatch' => $totalTakeApartMatch,
            'matchTakeApartCountStatistics' => $matchCountStatistics,
        );
        return $matchReport;
    }


    /**
     *   //Enhanced cost-per-click
     * @param $startTime
     * @param $endTime
     * @param int $ECPC
     * @return float|int
     * @version 1.0.0
     */
    public function adsIncome($startTime, $endTime, $ECPC = 40)
    {
        $adsInfosCount = $this->find()
            ->where(function (QueryExpression $exp) use ($startTime, $endTime) {
                return $exp->in('service', ['watch_ads', 'spin_ads'])
                    ->between('time', $startTime, $endTime);
            })
            ->count();
        $income = $ECPC * $adsInfosCount;
        return $income;
    }

}
