<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Userclankickeds Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ClansTable&\Cake\ORM\Association\BelongsTo $Clans
 *
 * @method \App\Model\Entity\Userclankicked get($primaryKey, $options = [])
 * @method \App\Model\Entity\Userclankicked newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Userclankicked[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Userclankicked|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Userclankicked saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Userclankicked patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Userclankicked[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Userclankicked findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Userclankicked newEmptyEntity()
 * @method \App\Model\Entity\Userclankicked[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Userclankicked[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Userclankicked[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Userclankicked[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class UserclankickedsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('userclankickeds');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Clans', [
            'foreignKey' => 'clan_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['clan_id'], 'Clans'));

        return $rules;
    }

    /**
     * @param $clanId
     * @return array
     * @version 1.0.0
     */
    public function getKickedUserClan($clanId)
    {
        $kickedUsers = $this->find()
            ->where(['Userclankickeds.clan_id' => $clanId])
            ->contain([
                'Users' => ['fields' => UsersTable::UserFields]
            ])->disableHydration()->all()->toArray();

        if (isset($kickedUsers) and $kickedUsers) {
            foreach ($kickedUsers as $key => $val) {
                $temp = $val['user'];
                unset($val['user']);
                $kickedUsers[$key] = $val + $temp;
            }
        } else {
            $kickedUsers = array();
        }

        return $kickedUsers;
    }

    /**
     * @param $userId
     * @param $clanId
     * @version 1.0.0
     */
    public function addToKickList($userId, $clanId)
    {
        $kickedBefore = $this->find('all')
            ->where(['clan_id' => $clanId, 'user_id' => $userId])->first();

        if (empty($kickedBefore)) {
            $entity = $this->newEntity(['user_id' => $userId, 'clan_id' => $clanId]);
            return $this->saveOrFail($entity);
        }

    }

    /**
     * @param $kickedUserId
     * @param $clanId
     * @version 1.0.0
     */
    public function removeFromkickList($kickedUserId, $clanId)
    {
        $this->deleteAll(['clan_id' => $clanId, 'user_id' => $kickedUserId]);
    }

}
