<?php

namespace App\Model\Table;

use App\Config\AppSettings;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Matches Model
 *
 * @property \App\Model\Table\ServersTable&\Cake\ORM\Association\BelongsTo $Servers
 * @property \App\Model\Table\FriendlymatchesTable&\Cake\ORM\Association\BelongsTo $Friendlymatches
 * @property \App\Model\Table\UsermatchesTable&\Cake\ORM\Association\HasMany $Usermatches
 *
 * @method \App\Model\Entity\Match get($primaryKey, $options = [])
 * @method \App\Model\Entity\Match newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Match[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Match|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Match saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Match patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Match[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Match findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Match newEmptyEntity()
 * @method \App\Model\Entity\Match[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Match[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Match[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Match[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MatchesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('matches');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Servers', [
            'foreignKey' => 'server_id',
            'joinType' => 'INNER',
        ]);
        $this->hasOne('Friendlymatches');

//        $this->belongsTo('Friendlymatches', [
//            'foreignKey' => 'match_id',
//            'joinType' => 'INNER',
//        ]);

        $this->hasMany('Usermatches', [
            'foreignKey' => 'match_id',

        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->add($rules->existsIn(['server_id'], 'Servers'));

        return $rules;
    }

    /**
     * @param int $id Matches id
     * @param int $state [-1: friendly match (created) | 0: started (lobby) | 1: finished | 2 : Canceled]
     * @return int
     * version 1.0.0
     */
    public function changeState(int $id, int $state)
    {
        $record = $this->get($id);
        $record->state = $state;
        try {
            $this->saveOrFail($record);
        } catch (\Cake\ORM\Exception\PersistenceFailedException $e) {
            echo $e->getEntity();
        }
        return true;
    }

    /**
     * @param $matchId
     * @param int[] $state
     * @return array|\Cake\Datasource\EntityInterface|\Cake\ORM\Query|null
     * @version 1.0.0
     */
    public function matchInfo($matchId,array $state = array(0, 1, 2))
    {
        return $this->find()->where(['id' => $matchId, 'state IN' => $state])
            ->contain('Usermatches')->disableHydration()->first();
    }

    /**
     * @param $matchId
     * @param int[] $state
     * @return array|\Cake\Datasource\EntityInterface|\Cake\ORM\Query|null
     * @version 1.0.0
     */
    public function matchInfoSimple($matchId, $state = array(0, 1, 2))
    {
        return $this->find()->where(['id' => $matchId, 'state' => $state])->first()->toArray();
    }


    /**
     * @param $serverId
     * @return \Cake\ORM\Query|int
     * @version 1.0.0
     */
    public function howManyMatchIsRun($serverId)
    {
        return $this->find()->where(['server_id' => $serverId, 'state' => 0])->select(['id'])->count();
    }

    /**
     * @param $type
     * @param $serverId
     * @param $requirePlayer
     * @param $player
     * @param $tableType
     * @param $groupMode
     * @param $userMatch
     * @param $userDatas
     * @param int $state
     * @return mixed
     * @throws \Exception
     * @version 1.0.0
     *
     */
    public function createMatch($type, $serverId, $requirePlayer, $player, $tableType, $groupMode, $userMatch, $userDatas, $state = 0)
    {
        $data = [
            'type' => $type,
            'server_id' => $serverId,
            'require_player' => $requirePlayer,
            'player' => $player,
            'table_type' => $tableType,
            'group_mode' => $groupMode,
            'starttime' => time(),
            'state' => $state,
            'usermatches' => $userMatch
        ];

        try {
            $matchEntity = $this->newEntity($data, ['associated' => ['Usermatches']]);
            $this->saveOrFail($matchEntity);
        } catch (\Cake\ORM\Exception\PersistenceFailedException $e) {
            echo $e->getEntity();
        }

        return $matchEntity->id;
    }

    /**
     * @return int|void
     * @version 1.0.0
     */
    public function matchFix()
    {
        $matchInfos = $this->find()
            ->where(['state' => 0, 'starttime <' => time() - 2 * 3600])
            ->contain(['Usermatches'])
            ->limit(100)
            ->all();

        $howManyFixed = 0;

        if (!$matchInfos) {
            return $howManyFixed;
        }

        //debug($matchInfos);


        /*
         *  Preparing a list of tasks to be processed
         */
        $taskLists = [];
        foreach ($matchInfos as $matchInfo) {
            $matchId = $matchInfo->id;
            foreach ($matchInfo->usermatches as $key => $user) {
                $userId = $user->user_id;
                $tableEntranceFee = $this->Usermatches->Users->Finances->matchEntranceFee($userId, $matchId);

                if ($tableEntranceFee == 0) {
                    $tableType = $matchInfo->table_type;
                    $tableEntranceFee = AppSettings::TableEntranceCost[$tableType]['coin'];
                }
                $taskLists[$matchId][] = [
                    'user_id' => $userId,
                    'match_id' => $matchId,
                    'coin' => $tableEntranceFee,
                    'service' => 'entrance_fee_refund',
                    'description' => "matchFix matchId=$matchId by cronjob"
                ];

            }
        }

        /*
         * Things to be processed
         */
        foreach ($taskLists as $matchId => $Usermatches) {
            try {
                $this->getConnection()->transactional(function () use ($matchId, $Usermatches, &$howManyFixed) {

                    foreach ($Usermatches as $key => $Usermatch) {
                        $this->Usermatches->Users->updateUserInfo($Usermatch);
                        $this->Usermatches->changeStateByUser($Usermatch['user_id'], 2);//cancel userMatch

                        //TODO(hamid) make it new fcm system
                        /*
                        $userInfo = $this->Usermatches->Users->userInfo($Usermatch['user_id']);
                        $teamname = $userInfo['User']['name'];
                        $this->Fcm = new Fcm();

                        // $this->Fcm->sendPushNotification($userId, $teamname, 'refund match');
                        */
                        $howManyFixed = $howManyFixed + 1;
                    }
                    $this->changeState($matchId, 2); //cancel  match
                    $howManyFixed = $howManyFixed + 1;
                });
                echo "$matchId fixed";

            } catch (Exception $e) {
                echo "$matchId error" . $e->getMessage();
                throw $e;
            }
        }

        return $howManyFixed;
    }


    /**
     * @param $matchId
     * @param $requestedGroupNumber
     * @return bool|string|null
     * @version 1.0.0;
     */
    public function hasEnoughCapacity($matchId, $requestedGroupNumber)
    {

        $matchInfo = $this->find()->where(['id' => $matchId])->contain('Usermatches')->first();

        $matchGroupMode = $matchInfo->group_mode;
        $matchCapacity = $matchInfo->player;

        $joinedPlayers = count($matchInfo->usermatches);

        if ($matchCapacity < $joinedPlayers) {
            return __('Match capacity is full'); //match capacity is full
        }

        $userMatch = $matchInfo->usermatches;

        if ($matchGroupMode == 'NoGroup') {
            $requestedGroupNumber = count($userMatch);
        }

        if ($joinedPlayers < 0) {
            return True;  //nobody joined yet
        }

        switch ($matchGroupMode) {
            case 'NoGroup' :
                if ($requestedGroupNumber > 5 || $requestedGroupNumber < 0) return FALSE;
                $checkGroupCapacity = $this->_checkGroupCapacity(1, $requestedGroupNumber, $userMatch);
                break;

            case 'TwoVsTwoVsTwo' :
            case 'TwoVsTwo' :
                if ($requestedGroupNumber > 2 || $requestedGroupNumber < 0) return FALSE;
                $checkGroupCapacity = $this->_checkGroupCapacity(2, $requestedGroupNumber, $userMatch);
                break;

            case 'ThreeVsThree' :
                if ($requestedGroupNumber > 1 || $requestedGroupNumber < 0) return FALSE;
                $checkGroupCapacity = $this->_checkGroupCapacity(3, $requestedGroupNumber, $userMatch);
                break;

            default:
                echo "Not defined match group mode";
                break;
        }

        return $checkGroupCapacity;

    }


    /**
     * @param $matchGroupCapacity
     * @param $requestedGroupNumber
     * @param $userMatches
     * @return bool|string|null
     * @version 1.0.1
     */
    private function _checkGroupCapacity($matchGroupCapacity, $requestedGroupNumber, $userMatches)
    {
        $joined = 0;
        foreach ($userMatches as $userMatch) {
            if ($userMatch['group_number'] == $requestedGroupNumber) {
                $joined++;
            }
        }

        if ($joined < $matchGroupCapacity) {
            return True;
        } else {
            return __('Group capacity is full');
        }
    }

    /**
     * means start the friendly match
     * @param $matchId
     * @version 1.0.0
     */
    public function startFriendlyMatch($matchId)
    {
        $aMatch = $this->find()->where(['Matches.id' => $matchId])->contain(['Friendlymatches'])->first();

        $return = $this->getConnection()->transactional(function () use ($aMatch) {
            $this->changeState($aMatch->id, 0);  //means start the friendly match
            $this->Friendlymatches->changeState($aMatch->friendlymatch->id, 1); //means start the friendly match
            return true;
        });
        return $aMatch;
    }
}
