<?php

namespace App\Model\Table;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Banks Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Bank get($primaryKey, $options = [])
 * @method \App\Model\Entity\Bank newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Bank[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Bank|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bank saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bank patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Bank[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Bank findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Bank newEmptyEntity()
 * @method \App\Model\Entity\Bank[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Bank[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Bank[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Bank[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class BanksTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('banks');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     *
     */
    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('money');
            //->requirePresence('money', 'create')
            //->notEmptyString('money');

        $validator
            ->scalar('fishbank')
            ->maxLength('fishbank', 50)
            ->allowEmptyString('fishbank');

        $validator
            ->nonNegativeInteger('timereg');
            //->requirePresence('timereg', 'create')
            //->notEmptyString('timereg');

        $validator
            ->scalar('paidtime')
//            ->maxLength('paidtime', 50)
  //          ->requirePresence('paidtime', 'create')
            ->notEmptyString('paidtime');

        $validator
            ->scalar('bankname');
//            ->maxLength('bankname', 50)
//            ->requirePresence('bankname', 'create')
//            ->notEmptyString('bankname');

        $validator
            ->scalar('tracecode')
            ->maxLength('tracecode', 50)
            ->allowEmptyString('tracecode');

        $validator
            ->scalar('accountnum')
            ->maxLength('accountnum', 50)
            ->allowEmptyString('accountnum');

        $validator
            ->notEmptyString('watch_video');

        $validator
            ->scalar('store_name')
            ->maxLength('store_name', 30)
            ->allowEmptyString('store_name');

        $validator
            ->scalar('token')
            ->maxLength('token', 70)
            ->allowEmptyString('token');

        $validator
            ->notEmptyString('del');

        $validator
            ->scalar('ip')
            ->notEmptyString('ip');

        $validator
            ->notEmptyString('state');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     * @param array $data
     * @return array
     * @version 1.0.0
     */
    public function getPaymentList($data = array())
    {
        $defaults = [
            'state' => 0,
            'starttime' => 0,
            'endtime' => 0
        ];

        $data = array_merge($defaults, $data);
        $userId = $data['user_id'];
        $condition = [];
        $condition['user_id'] = $userId;

        if (is_array($data['state'])) {
            $condition['state IN'] = $data['state'];
        } else {
            $condition['state'] = $data['state'];
        }

        if ($data['starttime'] > 0) {
            $condition['timereg'] = 'BETWEEN ' . $data['starttime'] . ' AND ' . $data['endtime'];
        }

        if (isset($data['item_store_id'])) {
            $itemStoreIdArray = explode(',', $data['item_store_id']);
            $condition['item_store_id IN'] = $itemStoreIdArray;
        }

        $bankItems = $this->find()
            ->select(['id', 'user_id', 'money', 'item_store_id', 'paidtime', 'bankname', 'tracecode', 'accountnum', 'watch_video', 'state'])
            ->where($condition)
            ->order(['id' => 'DESC'])
            ->disableHydration()
            ->all()
            ->toArray();

        //in_game_chat check
        $userMatchInfo = $this->Users->Usermatches->userMatchInfoSimple($userId);

        if ($userMatchInfo && $userMatchInfo['state'] == 0) {
            $inGameChat = $userMatchInfo['in_game_chat'];
        }

        $userInfo = $this->Users->userInfo($userId);
        $vip = $userInfo['vip'];
        $inGameChatTime = $userInfo['in_game_chat'];
        $time = time();

        if ((isset($inGameChat) && $inGameChat == 1) || $vip > $time || $inGameChatTime > $time) {
            $bankItems[] = [
                'id' => 0,
                'user_id' => $userId,
                'money' => 0,
                'item_store_id' => 'in_game_chat',
                'paidtime' => 0,
                'bankname' => 0,
                'tracecode' => 0,
                'accountnum' => 0,
                'watch_video' => 0,
                'state' => 1
            ];
        }

        return ['bankItems' => $bankItems, 'userInfo' => $userInfo];
    }


    /**
     * @param $data
     * @throws \Exception
     * @version 1.0.0
     */
    public function saveToBank($data)
    {
        $entities = $this->newEntities($data);
        return $this->saveManyOrFail($entities);

    }

    /**
     * @param $userId
     * @param $storeItemId
     * @return array|\Cake\Datasource\EntityInterface|false
     * @version 1.0.0
     */
    public function findItem($userId, $storeItemId)
    {
        $records = $this->find()->where(['user_id' => $userId, 'item_store_id' => $storeItemId])->first();
        if ($records === null) {
            return false;
        }
        return $records;
    }


    /**
     * @param $userId
     * @return array|bool|\Cake\Datasource\EntityInterface
     * @version 1.0.0
     */
    public function buyFromStore($userId)
    {
        $records = $this->find()->where(['user_id' => $userId, 'money >' => 0])->first();

        if ($records === null) {
            return false;
        }
        return $records;
    }

    /**
     * @param $userIds
     * @param $startTime
     * @param $endTime
     * @version 1.0.0
     */
    public function bankReport($userIds, $startTime, $endTime)
    {
        $realMoney = TRUE;
        $cond = array();

        $bankInfos = $this->find()
            ->where(function (QueryExpression $exp) use ($userIds, $startTime, $endTime) {
                return $exp->in('user_id', $userIds)
                    ->between('paidtime', $startTime, $endTime);
            })
            ->disableHydration()
            ->toArray();


        $bankInfos = $this->find()
            ->where(function (QueryExpression $exp) use ($userIds, $realMoney, $startTime, $endTime) {
                $cond = [];
                if ($realMoney)
                    $cond[] = $exp->gt('money', 0);
                if ($startTime && $endTime)
                    $cond[] = $exp->between('paidtime', $startTime, $endTime);
                if ($userIds)
                    $cond[] = $exp->in('user_id', $userIds);
                return $exp;
            })->disableHydration()
            ->toArray();

        $bankItemBoughts = array();

        foreach ($bankInfos as $bankInfo) {
            $storeName = $bankInfo['store_name'];
            $itemStoreId = $bankInfo['item_store_id'];

            if (isset($bankItemBoughts[$storeName][$itemStoreId]['count'])) {
                $bankItemBoughts[$storeName][$itemStoreId]['count']++;
            } else {
                $bankItemBoughts[$storeName][$itemStoreId]['count'] = 1;  //initialize
            }

        }
        $Shopitem = TableRegistry::getTableLocator()->get('Shopitems');

        foreach ($bankItemBoughts as $keyStoreName => $stores) {
            foreach ($stores as $storeId => $bankItemBought) {
                $item = $Shopitem->getItem($storeId, $keyStoreName);
                $bankItemBoughts[$keyStoreName][$storeId]['itemInfo'] = $item;
            }
        }
        return $bankItemBoughts;
    }

    /**
     * @param $startTime
     * @param $endTime
     * @return array
     * @todo hatman bayad baznegari beshe
     */
    public function bankReport2($startTime, $endTime)
    {

        $realMoney = TRUE;
        $cond = array();
        $bankInfos = $this->find()
            ->where(function (QueryExpression $exp) use ($realMoney, $startTime, $endTime) {
                $cond = [];
                if ($realMoney)
                    $cond[] = $exp->gt('money', 0);
                if ($startTime && $endTime)
                    $cond[] = $exp->between('paidtime', $startTime, $endTime);

                return $exp;
            })->disableHydration()
            ->toArray();


        $bankItemBoughts = array();
        foreach ($bankInfos as $bankInfo) {

            $storeName = $bankInfo['store_name'];
            $userId = $bankInfo['user_id'];
            $bankItemBoughts[$storeName]['Users'][$userId][] = $bankInfo;
            if (isset($bankItemBoughts[$storeName]['countTotalBought'])) $bankItemBoughts[$storeName]['countTotalBought']++; else $bankItemBoughts[$storeName]['countTotalBought'] = 1;

        }

        foreach ($bankItemBoughts as $storeName => $bankItemBought) {
            $totalIncomeStore = 0;
            $bankItemBoughts[$storeName]['countUniqueBought'] = count($bankItemBoughts[$storeName]['Users']);

            foreach ($bankItemBought['Users'] as $userBoughts) {

                $howManyAUserBuy = count($userBoughts);

                if (isset($bankItemBoughts[$storeName]['howManyTimesBought'][$howManyAUserBuy])) {
                    $bankItemBoughts[$storeName]['howManyTimesBought'][$howManyAUserBuy]++;
                } else {
                    $bankItemBoughts[$storeName]['howManyTimesBought'][$howManyAUserBuy] = 1;
                }

                foreach ($userBoughts as $userBought) {
                    $pack = $userBought['item_store_id'];

                    if (isset($bankItemBoughts[$storeName]['packBought'][$pack])) {
                        $bankItemBoughts[$storeName]['packBought'][$pack]++;
                    } else {
                        $bankItemBoughts[$storeName]['packBought'][$pack] = 1;
                    }

                    $totalIncomeStore += $userBought['money'];

                }

            }

            $userIds = $this->Users->activeUser($startTime, $endTime);
            $userIdsCount = count($userIds);

            $adsIncome = $this->Users->Finances->adsIncome($startTime, $endTime);
            $totalIncome = $adsIncome + $totalIncomeStore;

            $bankItemBoughts[$storeName]['ARPU'] = (int)($totalIncome / $userIdsCount);
            $bankItemBoughts[$storeName]['ARPPU'] = (int)($totalIncomeStore / $bankItemBoughts[$storeName]['countUniqueBought']);
            $bankItemBoughts[$storeName]['totalMoney'] = $totalIncome;

        }
        //@todo in vase chie
        $churnRate = $this->Users->churnRate($startTime, $endTime);

        return $bankItemBoughts;

    }

}
