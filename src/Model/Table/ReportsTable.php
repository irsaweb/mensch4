<?php

namespace App\Model\Table;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Reports Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Report get($primaryKey, $options = [])
 * @method \App\Model\Entity\Report newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Report[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Report|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Report saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Report patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Report[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Report findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Report newEmptyEntity()
 * @method \App\Model\Entity\Report[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Report[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Report[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Report[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ReportsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('reports');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('issue')
            ->requirePresence('issue', 'create')
            ->notEmptyString('issue');

        $validator
            ->scalar('content')
            ->maxLength('content', 255)
            ->requirePresence('content', 'create')
            ->notEmptyString('content');

        $validator
            ->integer('time')
            ->requirePresence('time', 'create')
            ->notEmptyString('time');

        $validator
            ->notEmptyString('state');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }


    /**
     * @param $userId
     * @param $reportedUserId
     * @param $issue
     * @return bool
     * @version 1.0.0
     */
    public function submittedBefore($userId, $reportedUserId, $issue)
    {
        $time = time();
        $startTime = $time - 3600 * 48;
        $endTime = $time;

        $reportedBefore = $this->find()
            ->select('id')
            ->where(['user_id' => $userId, 'reported_user_id' => $reportedUserId, 'issue' => $issue,])
            ->where(function (QueryExpression $exp) use ($startTime, $endTime) {
                return $exp->between('time', $startTime, $endTime);
            })->first();

        if ($reportedBefore) {
            return TRUE;
        }
        return FALSE;
    }


    /**
     *  run by cronjob
     * @version 1.0.0
     */
    public function muteReportedUser()
    {
        $time = time();
        // $startTime = $time - 3600 * 48;
        $startTime = 1400456050;
        $endTime = $time;

        $reportedBeforeQuery = $this->find();
        $reportedBeforeQuery->select([
            'reported_user_id',
            'total_reports' => $reportedBeforeQuery->func()->count('*')
        ])
            ->where([
                'issue !=' => 'name',
                'state' => 0
            ])
            ->where(function (QueryExpression $exp) use ($startTime, $endTime) {
                return $exp->between('time', $startTime, $endTime);
            })
            ->group('reported_user_id')
            ->having(['total_reports >=' => 3]);
        $reportedBefore = $reportedBeforeQuery->all()->toArray();

        foreach ($reportedBefore as $reportedUser) {

            $reportedUserId = $reportedUser['reported_user_id'];

            if ($reportedUserId > 0) {
                $howManyDistinctReport = $this->find()
                    ->select(['user_id', 'reported_user_id'])
                    ->distinct('user_id')
                    ->where(['reported_user_id' => $reportedUserId])
                    ->where(function (QueryExpression $exp) use ($startTime, $endTime) {
                        return $exp->between('time', $startTime, $endTime);
                    })
                    ->all()->toArray();
                $howManyDistinctReportCount = count($howManyDistinctReport);

                //if 3 different people reported mute him for 24 hours
                if ($howManyDistinctReportCount >= 3) {

                    $data = array(
                        'user_id' => $reportedUserId,
                        'mute' => 48 * 3600
                    );

                    $this->Users->updateUserInfo($data);
                    $query = $this->query()
                        ->update()
                        ->set(['state' => 1])
                        ->where(['reported_user_id' => $reportedUserId])
                        ->where(function (QueryExpression $exp) use ($startTime, $endTime) {
                            return $exp->between('time', $startTime, $endTime);
                        });
                }

            }

        }

    }

}
