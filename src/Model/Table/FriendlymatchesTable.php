<?php

namespace App\Model\Table;

use App\Lib\Fcm;
use Cake\Database\Expression\QueryExpression;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Friendlymatches Model
 *
 * @property \App\Model\Table\MatchesTable&\Cake\ORM\Association\BelongsTo $Matches
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ClansTable&\Cake\ORM\Association\BelongsTo $Clans
 *
 * @method \App\Model\Entity\Friendlymatch get($primaryKey, $options = [])
 * @method \App\Model\Entity\Friendlymatch newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Friendlymatch[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Friendlymatch|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Friendlymatch saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Friendlymatch patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Friendlymatch[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Friendlymatch findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Friendlymatch newEmptyEntity()
 * @method \App\Model\Entity\Friendlymatch[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Friendlymatch[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Friendlymatch[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Friendlymatch[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class FriendlymatchesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('friendlymatches');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Matches', [
            'foreignKey' => 'match_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'owner_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Clans', [
            'foreignKey' => 'clan_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->notEmptyString('networkversion');

        $validator
            ->integer('time')
            ->requirePresence('time', 'create')
            ->notEmptyString('time');

        $validator
            ->notEmptyString('state');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->add($rules->existsIn(['match_id'], 'Matches'));
        $rules->add($rules->existsIn(['owner_id'], 'Users'));
        $rules->add($rules->existsIn(['clan_id'], 'Clans'));

        return $rules;
    }


    /**
     * @param int $id Friendlymatches id
     * @param int $state [0: Created(Clan)| 1: Started| 2: Canceled by Owner| 3: Canceled by cron job]
     * @return int
     * version 1.0.0
     */
    public function changeState(int $id, int $state)
    {
        $record = $this->get($id);
        $record->state = $state;
        try {
            $this->saveOrFail($record);
        } catch (\Cake\ORM\Exception\PersistenceFailedException $e) {
            echo $e->getEntity();
        }
        return true;
    }

    /**
     * @param $friendlyMatchId
     * @return array|\Cake\Datasource\EntityInterface|\Cake\ORM\Query
     * @version 1.0.0
     */
    public function findFriendlyMatch($friendlyMatchId)
    {
        return $this->find()->where(['id' => $friendlyMatchId])->first();
    }


    /**
     * @param $MatchId
     * @return array|\Cake\Datasource\EntityInterface|\Cake\ORM\Query
     * @version 1.0.0
     */
    public function findFriendlyMatchByMatchId($MatchId)
    {
        return $this->find()->where(['match_id' => $MatchId])->firstOrFail();
    }


    /**
     * @param $clanId
     * @param null $matchId
     * @return array
     * @version 1.0.0
     */
    public function getFriendlyMatch($clanId, $matchId = NULL)
    {
        $time = time();
        $startTime = $time - 3600 * 48;
        $endTime = $time;
        $Query = $this->find()->where(['clan_id' => $clanId])->order(['Friendlymatches.id' => 'DESC'])->limit(5);

        if ($matchId != NULL) {
            $Query->where(['match_id' => $matchId]);
        }

        $Query->where(function (QueryExpression $exp, Query $q) use ($startTime, $endTime) {
            return $exp->between('time', $startTime, $endTime);
        });

        $Query->contain([
            'Matches' => function (Query $q) {
                $q->where(['Matches.state !=' => 2]);
                return $q;
            },
            'Matches.Usermatches.Users' => [
                'fields' => UsersTable::UserFields
            ]
        ]);

        $friendlyMatches = $Query->disableHydration()->toArray();

        //Map the data suitable for unity
        $mappedResponse = [];

        foreach ($friendlyMatches as $key => $friendlyMatch) {
            $temp = $friendlyMatch["match"]['usermatches'];
            unset($friendlyMatch["match"]['usermatches']);
            $mappedResponse[$key] = $friendlyMatch;
            $mappedResponse[$key]["usermatches"] = $temp;
        }

        return $mappedResponse;
    }


    /**
     * @param $clanId
     * @param $type
     * @param null $matchCreatorTeamName
     * @param $matchId
     * @version 1.0.0
     *
     */
    public function sendPushToActiveClanUsers($clanId, $type, $matchCreatorTeamName = NULL, $matchId)
    {
        $Fcm = new Fcm();
        $time = time() - 72 * 3600;  //3 days ago

        $mappedFriendlyMatchInfo = $this->getFriendlyMatch($clanId, $matchId);
        $clanMembers = $this->Users->Userclans->getUserClan($clanId);
        $pushData = [];
        foreach ($clanMembers as $clanMember) {
            $clanMemberLastLogin = $clanMember['lastlogindate'];

            if ($clanMemberLastLogin > $time) {
                $pushData[] = [
                    'userId' => $clanMember['id'],
                    'teamName' => $matchCreatorTeamName,
                    'tipsy' => $type,
                    'page' => 'clan',
                    'customData' => $mappedFriendlyMatchInfo,  //matchInfo
                ];
            }
        }
        $Fcm->sendPushNotification($pushData);
    }


    /**
     * @version 1.0.0
     */
    public function closeExpiredFriendlyMatch()
    {
        $expiredFriendlyMatches = $this->find()->where(['state' => 0, 'time <' => time() - 20])->all();

        if ($expiredFriendlyMatches) {
            foreach ($expiredFriendlyMatches as $expiredFriendlyMatch) {
                $FriendlymatchId = $expiredFriendlyMatch->id;
                $clanId = $expiredFriendlyMatch->clan_id;
                $matchId = $expiredFriendlyMatch->match_id;

                $this->changeState($FriendlymatchId, 3); //means closed by cronjob
                $this->Matches->Usermatches->changeStateByMatch($matchId, 2);
                $this->Matches->changeState($matchId, 2);

                $this->sendPushToActiveClanUsers($clanId, 'friendlyMatch expired', NULL, $matchId);
            }
        }
    }

}
