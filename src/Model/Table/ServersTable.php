<?php

namespace App\Model\Table;

use Cake\Mailer\Email;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Servers Model
 *
 * @property \App\Model\Table\MatchesTable&\Cake\ORM\Association\HasMany $Matches
 *
 * @method \App\Model\Entity\Server get($primaryKey, $options = [])
 * @method \App\Model\Entity\Server newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Server[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Server|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Server saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Server patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Server[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Server findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Server newEmptyEntity()
 * @method \App\Model\Entity\Server[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Server[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Server[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Server[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ServersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('servers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Matches', [
            'foreignKey' => 'server_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('ip')
            ->maxLength('ip', 25)
            ->requirePresence('ip', 'create')
            ->notEmptyString('ip');

        $validator
            ->integer('pid')
            ->notEmptyString('pid');

        $validator
            ->nonNegativeInteger('port')
            ->requirePresence('port', 'create')
            ->notEmptyString('port');

        $validator
            ->integer('time')
            ->requirePresence('time', 'create')
            ->notEmptyString('time');

        $validator
            ->requirePresence('networkversion', 'create')
            ->notEmptyString('networkversion');

        $validator
            ->notEmptyString('test');

        $validator
            ->notEmptyString('backup');

        $validator
            ->notEmptyString('close');

        $validator
            ->notEmptyString('active');

        return $validator;
    }

    /**
     * @return string
     * @version 1.0.0
     */
    public static function getServerIP()
    {
        $serverIP4local = '185.112.33.109';
        $serverIP = \Cake\Routing\Router::getRequest()->clientIp();
        if ($serverIP = '127.0.0.1') {
            return $serverIP4local;
        }
        return $serverIP;
    }

    /**
     * @param $ip
     * @param $port
     * @param $networkVersion
     * @param int $backup
     * @param bool $start
     * @param int $pid
     * @param int $test
     * @version 1.0.0
     */
    public function updateServer($ip, $port, $networkVersion, $backup = 0, $start = FALSE, $pid = 0, $test = 0)
    {
        $ServerEntity = $this->find()->where(['ip' => $ip, 'port' => $port, 'networkversion' => $networkVersion])->first();

        if ($ServerEntity === null) {
            $ServerEntity = $this->newEntity([]);
        }

        $ServerEntity->port = $port;
        $ServerEntity->ip = $ip;
        $ServerEntity->backup = $backup;
        $ServerEntity->test = $test;
        $ServerEntity->time = time();
        $ServerEntity->networkversion = $networkVersion;

        if ($pid > 0)
            $ServerEntity->pid = $pid;

        if ($start) {
            $ServerEntity->starttime = time();
            $ServerEntity->time = 0;
            $ServerEntity->close = 1;
        }

        return $this->saveOrFail($ServerEntity);
    }

    /**
     * @param $ip
     * @param $port
     * @param $networkVersion
     * @return array|\Cake\Datasource\EntityInterface|\Cake\ORM\Query
     * version @1.0.0
     */
    public function serverInfo($ip, $port, $networkVersion)
    {
        return $this->find()->where(['ip' => $ip, 'port' => $port, 'networkversion' => $networkVersion])->first();
    }

    /**
     * @param $serverId
     * @return bool
     * @version 1.0.0
     */
    public function isServerAlive($serverId)
    {
        $aliveTime = time() - 91;
        $gameServer = $this->find()->where(['id' => $serverId, 'time >= ' => $aliveTime])->first();

        if ($gameServer) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


    /**
     * if game servers server a lot of match clone it
     * @version 1.0.0
     */
    public function createNewGameServer($maxMatchOnServer = 20)
    {
        $this->_checkServer();
        $aliveTime = time() - 60;

        $cond = [
            'time >= ' => $aliveTime,
            'networkversion >=' => 11,
            'backup' => 0,
            'close' => 0,
            'active' => 1
        ];

        $gameServers = $this->find()->where($cond)->all();

        if ($gameServers) {
            foreach ($gameServers as $gameServer) {
                $gameServerId = $gameServer['id'];
                $networkVersion = $gameServer['networkversion'];
                $serverIp = $gameServer['ip'];
                $port = $this->_lastServerPort() + 1;
                $howManyMatchIsRunOnServer = $this->Match->howManyMatchIsRun($gameServerId);

                if ($howManyMatchIsRunOnServer > $maxMatchOnServer) {
                    $canOpenServer = $this->_openServer($networkVersion);
                    //if we can open previous servers do it otherwise create a new server
                    if (!$canOpenServer) {
                        //create new server
                        shell_exec("nohup /var/www/clients/client0/web1/private/mensch/server/$networkVersion/gameserver -ip $serverIp -port $port  > /var/log/unity/server$port.log &");
                    }
                    $this->updateAll(['close' => 1], ['id' => $gameServerId]);
                }
            }
        }
    }


    /**
     * @param $networkVersion
     * @return bool|mixed
     * @version 1.0.0
     */
    private function _openServer($networkVersion)
    {
        $closedGameServers = $this->find()
            ->where(['networkversion' => $networkVersion, 'close' => 1, 'backup' => 0, 'active' => 1])->all();

        foreach ($closedGameServers as $gameServer) {
            $gameServerId = $gameServer['id'];
            $howManyMatchIsRunOnServer = $this->Matches->howManyMatchIsRun($gameServerId);

            if ($howManyMatchIsRunOnServer == 0) {
                $this->updateAll(['close' => 0], ['id' => $gameServerId]);
                return $gameServer;
            }
        }
        return FALSE;
    }


    /**
     * @return mixed
     * @version 1.0.0
     */
    private function _lastServerPort()
    {
        return $this->find()->select(['port'])->order(['port' => 'DESC'])->firstOrFail()->port;
    }


    /**
     * if all servers on a specific network version is close, open it
     * @version 1.0.0
     */
    public function _checkServer()
    {
        $aliveTime = time() - 91;
        $gameServersNetworkVersions = $this->find()
            ->where(['backup' => 0, 'active' => 1])
            ->group(['networkversion'])
            ->select(['networkversion'])
            ->disableHydration()
            ->all()->toArray();

        foreach ($gameServersNetworkVersions as $gameServerNetworkVersion) {
            $networkVersion = $gameServerNetworkVersion['networkversion'];
            $currentOpenServer = $this->find()
                ->where(['networkversion' => $networkVersion, 'close' => 0, 'backup' => 0, 'active' => 1])
                ->first();

            if (!$currentOpenServer) {
                $openIt = $this->find()
                    ->where(['networkversion' => $networkVersion, 'close' => 1, 'backup' => 0, 'time >= ' => $aliveTime, 'active' => 1])
                    ->select(['id'])
                    ->first();

                if ($openIt) {
                    $serverId = $openIt['id'];
                    $this->updateAll(['close' => 0], ['id' => $serverId]);
                }

            } elseif ($currentOpenServer['time'] < $aliveTime) {
                $this->updateAll(['close' => 1], ['id' => $currentOpenServer['id']]);
                //    pr($openServer);
            }
        }
    }

    /**
     * @param $userId
     * @param $networkVersion
     * @param $aliveTime
     * @return bool|mixed
     * @version 1.0.0
     */
    public function findServer($userId, $networkVersion, $aliveTime)
    {
        $cond = [
            'time >= ' => $aliveTime,
            'networkversion' => $networkVersion,
            'close' => 0,
            'active' => 1
        ];

        $matchInfo = $this->Matches->Usermatches->userMatchInfo($userId, array(0, -1));

        if ($matchInfo) {
            $serverId = $matchInfo['server_id'];
            $isAlive = $this->isServerAlive($serverId);

            if ($isAlive) {
                $cond = ['id' => $serverId];
            }
        }

        $gameServers = $this->find()->where($cond)->disableHydration()->all();

        if (empty($gameServers)) {
            return False;
        }

        foreach ($gameServers as $gameServer) {
            if ($gameServer['backup'] == 1) {
                $backUpServers[] = $gameServer;
            } else {
                $primaryGameServers[] = $gameServer;
            }
        }

        if (isset($primaryGameServers)) {
            $gameServers = $primaryGameServers;
        } else {
            $gameServers = $backUpServers;
        }

        $randIndex = array_rand($gameServers);
        $gameServer = $gameServers[$randIndex];

        return $gameServer;

    }


    /**
     * @param $backupMatch
     * @param $matchOnInstance
     * @version 1.0.0
     */
    public function serverNotificationSender($backupMatch, $matchOnInstance)
    {
        $emailsTo = [
            'naimi.hamid@gmail.com',
            'aleali.Sina@gmail.com',
            'baghdadi.mo@gmail.com',
            'alireza3078@gmail.com '
        ];
        $servers = $this->find()->all();

        foreach ($servers as $server) {
            $serverIp = $server['ip'];
            $serverPid = $server['pid'];
            $serverId = $server['id'];
            $networkVersion = $server['networkversion'];
            $isBackup = (int)$server['backup'];
            $howManyMatchIsRun = $this->Matches->howManyMatchIsRun($serverId);

            if (($isBackup && $howManyMatchIsRun > $backupMatch) ||
                (!$isBackup && $howManyMatchIsRun > $matchOnInstance)) {

                $email = new Email('default');
                $from = $email->getFrom();
                $email->viewBuilder()->setTemplate('serverNotification');
                $email->setViewVars([
                    'serverId' => $serverId,
                    'isBackup' => $isBackup,
                    'networkVersion' => $networkVersion,
                    'serverIp' => $serverIp,
                    'serverPid' => $serverPid,
                    'howManyMatchIsRun' => $howManyMatchIsRun
                ]);
                $email->setEmailFormat('html')
                    ->setSubject("Menchico Server")
                    ->setTo($emailsTo)
                    ->setFrom($from)
                    ->send();
            }
        }
    }

    /**
     * @return mixed
     * @version 1.0.0
     */
    public function latestServerNetworkVersion()
    {
        $servers = $this->find()
            ->where(['test' => 0, 'active' => 1])
            ->order(['networkversion' => 'DESC'])
            ->disableHydration()
            ->all()->toArray();

        $latestNetworkVersion = 0;
        foreach ($servers as $server) {
            $serverId = $server['id'];
            $howManyMatchIsRun = $this->Matches->howManyMatchIsRun($serverId);
            if ($howManyMatchIsRun > 5) {       //means production server
                $latestNetworkVersion = $server['networkversion'];
                break;
            }
        }
        return $latestNetworkVersion;
    }


}
