<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Usermatches Model
 *
 * @property \App\Model\Table\MatchesTable&\Cake\ORM\Association\BelongsTo $Matches
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Usermatch get($primaryKey, $options = [])
 * @method \App\Model\Entity\Usermatch newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Usermatch[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Usermatch|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usermatch saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usermatch patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Usermatch[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Usermatch findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Usermatch newEmptyEntity()
 * @method \App\Model\Entity\Usermatch[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Usermatch[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Usermatch[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Usermatch[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class UsermatchesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('usermatches');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Matches', [
            'foreignKey' => 'match_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('table_type')
            ->allowEmptyString('table_type');

        $validator
            ->notEmptyString('group_number');

        $validator
            ->notEmptyString('in_game_chat');

        $validator
            ->scalar('name')
            ->maxLength('name', 25)
            ->allowEmptyString('name');

        $validator
            ->scalar('status')
            ->notEmptyString('status');

        $validator
            ->notEmptyString('rank');

        $validator
            ->notEmptyString('state');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->add($rules->existsIn(['match_id'], 'Matches'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    /**
     * @param int $matchId Matches id
     * @param int $state [-1: friendly match (created) | 0: started  (lobby) | 1: finished | 2: Canceled]
     * @return int
     * version 1.0.0
     */
    public function changeStateByMatch(int $matchId, int $state)
    {
        return $this->updateAll(['state' => $state], ['match_id' => $matchId]);
    }

    /**
     * @param int $userId
     * @param int $state
     * @return int
     */
    public function changeStateByUser(int $userId, int $state)
    {
        return $this->updateAll(['state' => $state], ['user_id' => $userId]);
    }

    /**
     * @param $userId
     * @param int[] $state
     * @return |null
     * @version 1.0.0
     */
    public function userMatchInfo($userId, $state = array(0, 1, 2))
    {
        $userMatch = $this->find()->where(['user_id' => $userId, 'state IN' => $state])
            ->select('match_id')->order(['id' => 'DESC'])->first();

        if ($userMatch === null) {
            return false;
        }
        return $this->Matches->matchInfo($userMatch->match_id);

    }

    /**
     * @param $userId
     * @return array|\Cake\Datasource\EntityInterface
     * @version 1.0.0
     */
    public function userMatchInfoSimple($userId)
    {
        $records = $this->find()->where(['user_id' => $userId])->order(['id' => 'DESC'])->disableHydration()->first();
        if ($records === null) {
            return false;
        }
        return $records;
    }


    /**
     * @param $matchId
     * @return \Cake\Datasource\ResultSetInterface|\Cake\ORM\Query
     * @version 1.0.0
     */
    public function userTakeApartInMatch($matchId)
    {
        return $this->find()->where(['match_id' => $matchId])->select(['user_id'])->all();
    }

    /**
     * @param $userId
     * @return float|int
     * @version 1.0.0
     */
    public function avgWinRank($userId)
    {
        $userMatches = $this->find()
            ->where([
                'user_id' => $userId,
                'table_type !=' => 'friendly',
                'rank >' => 0,
                //'state'=>1
            ])
            ->order(['id' => 'DESC'])
            ->limit(5)->all();

        if ($userMatches) {
            $count = count($userMatches);

            if ($count == 5) {
                $winRate = 0;
                foreach ($userMatches as $userMatch) {
                    $winRate += $userMatch->rank;
                }
                $avgRank = $winRate / $count;
            } else {
                //age zire 5 ta mosabeghe dade bud intermediate ya beginner
                $avgRank = rand(16, 40) / 10;
            }
        } else {
            //first match
            $avgRank = 4;
        }
        return $avgRank;
    }


    /**
     * @param $userId
     * @param $groupMode
     * @param $rank
     * @return int
     * @version 1.0.0
     *
     */
    public function howManyWin($userId, $groupMode, $rank)
    {
        $cond = ['user_id' => $userId, 'group_mode' => $groupMode];

        if (is_array($rank)) {
            $cond['rank IN'] = $rank;
        } else {
            $cond['rank'] = $rank;
        }

        return $this->find()->where($cond)->contain(['Matches'])->select(['id'])->count();
    }

    /**
     * @param $serverId
     * @return \Cake\ORM\Query|int
     * @version 1.0.0
     */
    public function howManyUserOnServer($serverId)
    {
        return $this->find()
            ->where(['Usermatches.state' => 0, 'Matches.server_id' => $serverId,])
            ->contain(['Matches'])
            ->select(['id'])->count();
    }


    /**
     *
     * @version 1.0.0
     */
    public function userMatchFix()
    {
        $userMatchInfos = $this->find()
            ->where(['usermatches.state' => 0])
            ->contain([
                'Matches' => function (Query $q) {
                    return $q->where(['Matches.state IN' => [1, 2]]);
                }])
            ->all();

        if ($userMatchInfos) {
            return false;
        }

        foreach ($userMatchInfos as $userMatchInfo) {
            try {
                $this->getConnection()->transactional(function () use ($userMatchInfo) {
                    $userMatchId = $userMatchInfo['id'];
                    $matchId = $userMatchInfo['match_id'];
                    $userId = $userMatchInfo['user_id'];

                    $tableEntranceFee = $this->Users->Finances->matchEntranceFee($userId, $matchId);

                    $userData = [
                        'user_id' => $userId,
                        'match_id' => $matchId,
                        'coin' => $tableEntranceFee,
                        'service' => 'entrance_fee_refund',
                        'description' => "userMatchFix userMatchId=$userMatchId by cronjob"
                    ];

                    $this->Users->updateUserInfo($userData);
                    $this->updateAll(['state' => 2], ['id' => $userMatchId]);  //cancel userMatch

                    $userInfo = $this->User->userInfo($userId);
                    $teamname = $userInfo['name'];

                    $this->Fcm = new Fcm();

                    //TODO(hamid) make it new fcm

                    // $this->Fcm->sendPushNotification($userId, $teamname, 'refund match');;

                });
                echo "$userMatchId fixed";
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

}
