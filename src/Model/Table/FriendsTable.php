<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Friends Model
 *
 *
 * @method \App\Model\Entity\Friend get($primaryKey, $options = [])
 * @method \App\Model\Entity\Friend newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Friend[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Friend|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Friend saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Friend patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Friend[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Friend findOrCreate($search, ?callable $callback = null, $options = [])
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Requesterusers
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Secondusers
 * @method \App\Model\Entity\Friend newEmptyEntity()
 * @method \App\Model\Entity\Friend[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Friend[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Friend[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Friend[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class FriendsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('friends');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Requesterusers', [
            'className' => 'Users',
            'foreignKey' => 'requester_user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Secondusers', [
            'className' => 'Users',
            'foreignKey' => 'second_user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('date')
            ->requirePresence('date', 'create')
            ->notEmptyString('date');

        $validator
            ->notEmptyString('state');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->add($rules->existsIn(['requester_user_id'], 'Requesterusers'));
        $rules->add($rules->existsIn(['second_user_id'], 'Secondusers'));

        return $rules;
    }

    /**
     * @param $userId
     * @param $start
     * @param $length
     * @return array
     * @version 1.0.0
     */
    public function getFriendList($userId, $start, $length)
    {
        $friendsList = $this->find()
            ->where([
                'OR' => [
                    'Friends.requester_user_id' => $userId,
                    'Friends.second_user_id' => $userId
                ],
            ])
            ->select([
                'Friends.id',
                'Friends.state',
                'Requesterusers.id',
                'Requesterusers.name',
                'Requesterusers.totaltrophy',
                'Requesterusers.colour6',
                'Requesterusers.ring',
                'Requesterusers.symbol',
                'Requesterusers.vip',
                'Secondusers.id',
                'Secondusers.name',
                'Secondusers.totaltrophy',
                'Secondusers.colour6',
                'Secondusers.ring',
                'Secondusers.symbol',
                'Secondusers.vip'
            ])
            ->contain(['Requesterusers', 'Secondusers'])
            ->limit($length)
            ->offset($start)
            ->disableHydration()
            ->all()->toArray();


        foreach ($friendsList as $key => $friend) {
            if ($friend['requesteruser']['id'] != $userId) {
                $temp = $friend['requesteruser'];
                $temp['canApprove'] = false;
                if ($friend['state'] == 0) {
                    $temp['canApprove'] = true;
                }
            } else {
                $temp = $friend['seconduser'];
                $temp['canApprove'] = false;
            }

            $temp['request_id'] = $friend['id'];
            $temp['state'] = $friend['state'];

            $friendsList[$key] = $temp;
        }
        return $friendsList;
    }

    /**
     * @param $userId
     * @return int
     */
    public function getFriendCounts($userId)
    {
        return $this->find()
            ->where(['OR' => ['second_user_id' => $userId, 'requester_user_id' => $userId]])
            ->count();
    }
}
