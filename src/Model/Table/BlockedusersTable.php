<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Blockedusers Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Blockeduser get($primaryKey, $options = [])
 * @method \App\Model\Entity\Blockeduser newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Blockeduser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Blockeduser|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Blockeduser saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Blockeduser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Blockeduser[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Blockeduser findOrCreate($search, ?callable $callback = null, $options = [])
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Blocked
 * @method \App\Model\Entity\Blockeduser newEmptyEntity()
 * @method \App\Model\Entity\Blockeduser[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Blockeduser[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Blockeduser[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Blockeduser[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class BlockedusersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('blockedusers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Blocked', [
            'className' => 'Users',
            'foreignKey' => 'blocked_user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('time')
            ->requirePresence('time', 'create')
            ->notEmptyString('time');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['blocked_user_id'], 'Blocked'));

        return $rules;
    }

    /**
     * @param $userId
     * @param $takeApartUserIdsArray
     * @return array |null
     * @version 1.0.0
     */
    public function getBlockedUserList($userId, $takeApartUserIdsArray)
    {
        $blockedUserList = $this->find('all')
            ->where(['user_id' => $userId, 'blocked_user_id IN' => $takeApartUserIdsArray])->all();

        if ($blockedUserList) {
            return $blockedUserList;
        } else {
            return NULL;
        }
    }
}
