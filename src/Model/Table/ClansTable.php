<?php

namespace App\Model\Table;

use Cake\Cache\Cache;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\Validation\Validator;
use phpDocumentor\Reflection\Types\Integer;

/**
 * Clans Model
 *
 * @property \App\Model\Table\UserclankickedsTable&\Cake\ORM\Association\HasMany $Userclankickeds
 * @property \App\Model\Table\UserclansTable&\Cake\ORM\Association\HasMany $Userclans
 *
 * @method \App\Model\Entity\Clan get($primaryKey, $options = [])
 * @method \App\Model\Entity\Clan newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Clan[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Clan|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Clan saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Clan patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Clan[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Clan findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Clan newEmptyEntity()
 * @method \App\Model\Entity\Clan[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Clan[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Clan[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Clan[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ClansTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('clans');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Userclankickeds', [
            'foreignKey' => 'clan_id',
        ]);
        $this->hasMany('Userclans', [
            'foreignKey' => 'clan_id',
        ]);
    }


    /**
     * @param $name
     * @param $badge
     * @param $require_trophy
     * @param $description
     * @param $userCreatorTrophy
     * @param $userIdCreator
     * @return bool|\Cake\ORM\Association
     * @version 1.0.0
     */
    public function createClan($name, $badge, $require_trophy, $description, $userCreatorTrophy, $userIdCreator)
    {
        $clanExist = $this->Userclans->getClanByUser($userIdCreator);

        if ($clanExist) {
            return false;
        }

        $ClanEntity = $this->newEntity([
            'name' => $name,
            'badge' => $badge,
            'require_trophy' => $require_trophy,
            'description' => $description,
            'trophy' => $userCreatorTrophy,
            'member' => 0
        ]);

        $clanId = $this->getConnection()->transactional(function () use ($ClanEntity, $userIdCreator) {
            $clan = $this->saveOrFail($ClanEntity);
            $this->Userclans->addUserClan($userIdCreator, $clan->id, 'superadmin', true);
            return $clan->id;
        });

        return $clanId;
    }

    /**
     * @param $clanId
     * @param $name
     * @param $badge
     * @param $require_trophy
     * @param $description
     * @param $userIdSuperadmin
     * @version 1.0.0
     */
    public function editClan($clanId, $name, $badge, $require_trophy, $description, $userIdSuperadmin)
    {
        $isSuperadmin = $this->Userclans->getUserRole($userIdSuperadmin, $clanId);

        if ($isSuperadmin !== 'superadmin') {
            return false;
        }

        $clan = $this->get($clanId);
        $clan->badge = $badge;
        $clan->name = $name;
        $clan->require_trophy = $require_trophy;
        $clan->description = $description;

        if ($this->saveOrFail($clan)) {
            Cache::delete("clan_$clanId", 'veryshort');
            return true;
        }

        return false;
    }


    /**
     * note: faghat 1 bar mitune clan ro promote kone ba vip +
     * check beshe pol dare ya na age vip bud promote kone
     *
     * @param $clanId
     * @param $userIdSuperadmin
     * @param int $promoteTime
     * @version 1.0.0
     */
    public function promoteClan($clanId, $userIdSuperadmin, $promoteTime = 0, $clanPromotePrice)
    {
        $UserRoll = $this->Userclans->getUserRole($userIdSuperadmin, $clanId);

        if (!in_array($UserRoll, ['superadmin', 'admin'])) {
            return false;
        }

        $userData = [
            'user_id' => $userIdSuperadmin,
            'coin' => -$clanPromotePrice,
            'promote_clan' => 1,
            'service' => 'promote_clan_fee',
            'description' => "claId=$clanId"
        ];

        $this->Userclans->Users->updateUserInfo($userData);
        $this->updateAll(['promote' => $promoteTime], ['id' => $clanId]);
        Cache::delete("clan_$clanId", 'veryshort');
    }

    /**
     * @param $clanId
     * @version 1.0.0
     */
    public function updateTotalThrophy($clanId)
    {
        $clanUsers = $this->Userclans->find()
            ->where(['clan_id' => $clanId])
            ->select(['user_id'])
            ->disableHydration()
            ->all()->toArray();

        // todo: aya in doroste?
        if (empty($clanUsers)) {
            return false;
        }

        $userClanIds = Hash::extract($clanUsers, "{n}.user_id");

        $userClans = $this->Userclans->Users->find()
            ->where(['id IN' => $userClanIds])
            ->select(['totaltrophy'])
            ->disableHydration()
            ->all()
            ->toArray();

        $totalTrophy = 0;
        foreach ($userClans as $key => $user) {
            $totalTrophy += $user['totaltrophy'];
        }

        $clan = $this->get($clanId);
        $clan->trophy = $totalTrophy;
        if ($this->save($clan)) {
            Cache::delete("clan_$clanId", 'veryshort');
            return true;
        } else {
            return false;
        }
    }


    /**
     * @param $clanId
     * @version 1.0.0
     */
    public function clanMember($clanId)
    {
        $userClanCount = count($this->Userclans->getUserClan($clanId));
        $this->updateAll(['member' => $userClanCount], ['id' => $clanId]);
        Cache::delete("clan_$clanId", 'veryshort');
    }

    /**
     * @param $clanId
     * @version 1.0.0
     */
    public function clanMemberFix($clanId)
    {
        $userClanCount = count($this->Userclans->getUserClan($clanId));
        $this->updateAll(['member' => $userClanCount], ['id' => $clanId]);
        Cache::delete("clan_$clanId", 'veryshort');
    }


    /**
     * @param $myClan
     * @param $start
     * @param $length
     * @return array
     * @version 1.0.0
     */
    public function getClanRanking($myClan, $start, $length)
    {
        if ($myClan) {
            $clanTrophy = $myClan['trophy'];
            $rankCount = $this->find('all')
                ->where(["trophy >" => $clanTrophy])->count();
            $rankCount = $rankCount + 1;
        } else {
            $rankCount = 0;
        }

        $clans = $this->find()->order(['trophy' => 'DESC'])->limit($start)->offset($length)->all();
        return ['list' => $clans, 'myClan' => $myClan, 'clanRank' => $rankCount];
    }

    /**
     * @param $clanId
     * @return \Cake\ORM\Query|mixed
     * @version 1.0.0
     */
    public function getClanInfo(int $clanId)
    {
        $clanInfo = Cache::read("clan_$clanId", 'veryshort');

        if ($clanInfo) {
            return $clanInfo;
        }
        $time = time();
        $clanInfo = $this->find()
            ->where(['id' => $clanId])
            ->contain([
                'Userclans' => [
                    'fields' => ['Userclans.id', 'Userclans.clan_id', 'Userclans.user_id', 'Userclans.role'],
                    'sort' => ['totaltrophy' => 'DESC'],
                ],
                'Userclans.Users' => [
                    'fields' => UsersTable::UserFields,
                ]
            ])->disableHydration()->first();


        if ($clanInfo === null) {
            return false;
        }


        foreach ($clanInfo['userclans'] as $key => $userClan) {
            $clanInfo['userclans'][$key]['totaltrophy'] = $userClan['user']['totaltrophy'];

            $userId = $userClan['user']['id'];
            $lastLogin = $userClan['user']['lastlogindate'];

            if ($lastLogin > $time - 3600) {
                $clanInfo['userclans'][$key]['matchInfo'] = $this->Userclans->Users->Usermatches->userMatchInfo($userId, 0);
            } else {
                $clanInfo['userclans'][$key]['matchInfo'] = NULL;
            }
        }
        $clanInfo['userclans'] = Hash::sort($clanInfo['userclans'], '{n}.totaltrophy', 'DESC');

        Cache::write("clan_$clanId", $clanInfo, 'veryshort');
        return $clanInfo;
    }


    /**
     * @param $userId
     */
    public function deleteUserClan($userId)
    {
        $clan = $this->Userclans->getClanByUser($userId);
        if (!$clan) {
            return false;
        }
        $this->delete($clan->clan);
        return $this->Userclans->delete($clan);

    }

}
