<?php

namespace App\Model\Table;

use Cake\ORM\Table;

/**
 * Awards Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Award get($primaryKey, $options = [])
 * @method \App\Model\Entity\Award newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Award[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Award|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Award saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Award patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Award[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Award findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Award newEmptyEntity()
 * @method \App\Model\Entity\Award[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Award[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Award[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Award[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class AwardsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('awards');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }


    /**
     * @param $userId
     * @param $type
     * @return array|\Cake\Datasource\EntityInterface|\Cake\ORM\Query
     * @version 1.0.0
     */
    public function winners($userId, $type)
    {
       return $this->find()->where(['user_id' => $userId, 'type' => $type, 'done' => 0])->first();
    }


    /**
     * @param $type
     * @version 1.0.0
     */
    public function setWinners($type)
    {
        $users = $this->Users->whoIsWinners($type);
        $winnerAward = ['5000', '2500', '1000'];
        $winners = [];
        foreach ($users as $key => $user) {
            $winners[] = [
                'user_id' => $user['id'],
                'type' => 'weekly',
                'trophy' => $user[$type],
                'coin' => $winnerAward[$key],
                'time' => time()
            ];
        }
        $entities = $this->newEntities($winners);
        return $this->saveManyOrFail($entities);
    }

}
