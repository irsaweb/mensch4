<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Userclans Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ClansTable&\Cake\ORM\Association\BelongsTo $Clans
 *
 * @method \App\Model\Entity\Userclan get($primaryKey, $options = [])
 * @method \App\Model\Entity\Userclan newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Userclan[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Userclan|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Userclan saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Userclan patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Userclan[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Userclan findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Userclan newEmptyEntity()
 * @method \App\Model\Entity\Userclan[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Userclan[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Userclan[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Userclan[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class UserclansTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('userclans');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Clans', [
            'foreignKey' => 'clan_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('role')
            ->notEmptyString('role');

        $validator
            ->scalar('pass')
            ->maxLength('pass', 32)
            ->requirePresence('pass', 'create')
            ->notEmptyString('pass');

        $validator
            ->notEmptyString('repair');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['clan_id'], 'Clans'));

        return $rules;
    }

    /**
     * @param $userId
     * @param $clanId
     * @return mixed user|admin|superadmin|false
     * @version 1.0.0
     */
    public function getUserRole($userId, $clanId)
    {
        $res = $this->find()->where(['user_id' => $userId, 'clan_id' => $clanId])->first();
        if ($res !== null) {
            return $res->role;
        }
        return false;
    }

    /**
     * @param $clanId
     * @return array|\Cake\Datasource\ResultSetInterface|\Cake\ORM\Query
     * @version 1.0.0
     */
    public function getUserClan($clanId)
    {
        $time = time();

        $userClans = $this->find()
            ->where(['clan_id' => $clanId])
            ->select(['id', 'clan_id', 'user_id', 'role'])
            ->contain([
                'Users' => [
                    'fields' => UsersTable::UserFields,
                ]
            ])
            ->order(['Users.totaltrophy' => 'DESC'])
            ->all()
            ->toArray();

        if (isset($userClans) and $userClans) {
            foreach ($userClans as $key1 => $userClan) {
                if ($userClans[$key1]['id'] == null) {
                    unset($userClans[$key1]);
                }
            }

            foreach ($userClans as $key2 => $userClan) {
                $userId = $userClan['id'];
                $userClans[$key2]['matchInfo'] = $this->Users->Usermatches->userMatchInfo($userId, 0);
            }


            $userClans = array_values($userClans);

            return $userClans;
        } else {
            return array();
        }
    }


    /**
     * @param $userId
     * @return array|\Cake\Datasource\EntityInterface|false
     * @version 1.0.0
     */
    public function getClanByUser($userId)
    {
        $clan = $this->find()->where(['user_id' => $userId])->contain(['Clans'])->first();
        if ($clan === null) {
            return false;
        }
        return $clan->clan;
    }

    /**
     * @param $userId
     * @return false|mixed
     */
    public function getClanIdHasOwner($userId)
    {
        $res = $this->find()->where(['user_id' => $userId, 'role' => 'superadmin'])->first();
        if ($res === null) {
            return false;
        }
        return $res->clan_id;
    }


    /**
     * @param $userId
     * @param $clanId
     * @param string $role
     * @param bool $createClan
     * @return array
     * @version 1.0.0
     */
    public function addUserClan($userId, $clanId, $role = 'user', $createClan = false)
    {

        $clanInfo = $this->Clans->get($clanId);
        $userInfo = $this->Users->userInfo($userId);

        $userClanKicked = $this->Clans->Userclankickeds->find()
            ->where(['clan_id' => $clanId, 'user_id' => $userId])
            ->disableHydration()
            ->first();


        $trophy = $userInfo['totaltrophy'];

        if ($createClan === false) {
            if ($clanInfo === null || $clanInfo['require_trophy'] >= $trophy) {
                return [false, 'trophy'];
            }
        }


        if ($userClanKicked !== null) {
            return array(FALSE, 'kicked');
        }

        //todo: in userid vase chie?!
        if ($clanInfo['member'] > 50 || $userId == 46256) {
            return array(FALSE, 'full');
        }

        $myClan = $this->getClanByUser($userId);
        if ($myClan !== false) {
            return array(FALSE, 'joined');
        }

        $pass = md5("NOA-Games" . $userId);

        $data = $this->newEntity([
            'user_id' => $userId,
            'clan_id' => $clanId,
            'role' => $role,
            'pass' => $pass
        ]);

        try {
            $this->getConnection()->transactional(function () use (&$data, $clanId) {

                $this->saveOrFail($data);
                $this->Clans->updateTotalThrophy($clanId);
                $this->Clans->clanMember($clanId);
            });
        } catch (Exception $e) {
            return array(FALSE, 'rollback');
        }
        return array(TRUE, 'done');
    }


    /**
     * @param $userId
     * @param $kickUserId
     * @return bool
     * @version 1.0.0
     */
    public function kickUserClan($userId, $kickUserId)
    {

        $clanId = $this->getClanIdHasOwner($userId);

        if (!$clanId) {
            return false;
        }

        try {
            $this->getConnection()->transactional(function () use ($clanId, $kickUserId) {
                $this->deleteAll(['clan_id' => $clanId, 'user_id' => $kickUserId]);
                $this->Users->Userclankickeds->addToKickList($kickUserId, $clanId);
                $this->Clans->updateTotalThrophy($clanId);
                $this->Clans->clanMember($clanId);
            });
            return true;
        } catch (Exception $e) {
            return false;
        }
    }


    /**
     * @param $userId
     * @param $promoteUerId
     * @param $role
     * @version 1.0.0
     */
    public function changeUserclanRole($userId, $promoteUerId, $role)
    {

        $clanId = $this->getClanIdHasOwner($userId);

        if (!$clanId) {
            return false;
        }

        $userclan = $this->find()->where(['clan_id' => $clanId, 'user_id' => $promoteUerId])->first();

        if ($userclan === null) {
            return false;
        }

        $userclan->role = $role;
        return $this->save($userclan);
    }


    /**
     * @param $userId
     * @param $clanId
     * @return bool
     * @version 1.0.0
     */
    public function leaveUserClan($userId, $clanId)
    {
        $isUserInClan = $this->find()->where(['user_id' => $userId, 'clan_id' => $clanId])->first();

        if ($isUserInClan === null) {
            return false;
        }

        try {
            $this->getConnection()->transactional(function () use ($isUserInClan, $userId, $clanId) {
                $this->deleteAll(['clan_id' => $clanId, 'user_id' => $userId]);
                $this->Clans->updateTotalThrophy($clanId);
                $this->Clans->clanMember($clanId);

                $userClanCount = count($this->getUserClan($clanId));

                if ($userClanCount == 0) {
                    $this->Clans->deleteAll(['id' => $clanId]);
                    $this->Users->Userclankickeds->deleteAll(['clan_id' => $clanId]);
                } else {
                    $this->_checkLeader($clanId);
                }
            });

            return true;

        } catch (Exception $e) {
            return false;
        }
    }


    /**
     * @param $clanId
     * @version 1.0.0
     */
    public function _checkLeader($clanId)
    {
        $usersInClan = $this->find('list', ['valueField' => 'role'])->where(['clan_id' => $clanId])->toArray();
        if (in_array('superadmin', $usersInClan)) {
            return true;
        }

        $usersInClan = array_keys($usersInClan);
        $usersInClan = $usersInClan[0];

        $record = $this->newEmptyEntity();
        $record->id = $usersInClan;
        $record->role = 'superadmin';
        return $this->save($record);
    }


    /**
     * @param $userId
     * @param $kickUserId
     * @return bool
     * @version 1.0.0
     */
    public function removeFormKicklist($userId, $kickUserId)
    {
        $clanId = $this->getClanIdHasOwner($userId);
        if (!$clanId) {
            return false;
        }

        try {
            $this->getConnection()->transactional(function () use ($kickUserId, $userId, $clanId) {

                $this->deleteAll(['clan_id' => $clanId, 'user_id' => $kickUserId]);
                $this->Users->Userclankickeds->removeFromkickList($kickUserId, $clanId);
            });

            return true;

        } catch (Exception $e) {
            return false;
        }
    }


    /**
     * @version 1.0.0
     */
    public function fixClanMember()
    {
        $clans = $this->Clans->find()
            ->order('rand()')
            ->limit(100)
            ->all();

        foreach ($clans as $clan) {
            $clanId = $clan['id'];
            $userClans = $this->getUserClan($clanId);
            $userClanCount = count($userClans);

            if ($userClanCount > 50) {
                $howManyUserShouldRemove = $userClanCount - 50;
                $removeUsers = $this->find()
                    ->where(['clan_id' => $clanId, 'role !=' => 'superadmin',])
                    ->order(['id' => 'DESC'])
                    ->limit($howManyUserShouldRemove)
                    ->all()->toArray();

                $removeUserClanIds = Hash::extract($removeUsers, '{n}.id');
                $this->deleteAll(['id IN' => $removeUserClanIds]);

                $this->Clans->updateTotalThrophy($clanId);
                $this->Clans->clanMemberFix($clanId);
            }
        }
    }

}
