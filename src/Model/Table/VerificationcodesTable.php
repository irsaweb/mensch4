<?php
declare(strict_types=1);

namespace App\Model\Table;


use Cake\Database\Expression\QueryExpression;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use phpDocumentor\Reflection\Types\Integer;

/**
 * Verificationcodes Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Verificationcode newEmptyEntity()
 * @method \App\Model\Entity\Verificationcode newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Verificationcode[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Verificationcode get($primaryKey, $options = [])
 * @method \App\Model\Entity\Verificationcode findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Verificationcode patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Verificationcode[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Verificationcode|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Verificationcode saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Verificationcode[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Verificationcode[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Verificationcode[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Verificationcode[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class VerificationcodesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('verificationcodes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }


    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);

        return $rules;
    }

    /**
     * @param Integer $id
     * @return int
     */
    public function incrementFailTry(Integer $id)
    {
        $expression = new QueryExpression('fail_try = fail_try + 1');
        return $this->updateAll([$expression], ['id' => $id]);
    }
}
