<?php

namespace App\Model\Table;

use App\Lib\Fcm;
use App\Lib\Kavenegar;
use Cake\Cache\Cache;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\GroupsTable&\Cake\ORM\Association\BelongsTo $Groups
 * @property \App\Model\Table\BanksTable&\Cake\ORM\Association\HasOne $Banks
 * @property \App\Model\Table\FinancesTable&\Cake\ORM\Association\HasMany $Finances
 * @property \App\Model\Table\UserclankickedsTable&\Cake\ORM\Association\HasMany $Userclankickeds
 * @property \App\Model\Table\UserclansTable&\Cake\ORM\Association\HasOne $Userclans
 * @property \App\Model\Table\UsermatchesTable&\Cake\ORM\Association\HasOne $Usermatches
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @property \Cake\ORM\Table&\Cake\ORM\Association\HasMany $Friendsecondusers
 * @property \Cake\ORM\Table&\Cake\ORM\Association\HasMany $Friendrequesterusers
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class UsersTable extends Table
{
    public const UserFields = [
        'Users.id',
        'Users.name',
        'Users.symbol',
        'Users.ring',
        'Users.colour4',
        'Users.colour6',
        'Users.xp',
        'Users.vip',
        'Users.lastlogindate',
        'Users.promote_clan',
        'Users.in_game_chat',
        'Users.weeklytrophy',
        'Users.dailytrophy',
        'Users.totaltrophy'
    ];

    public const REG_UNIQUE_ID = 'SUCCESS';

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'joinType' => 'INNER',
        ]);

        $this->hasOne('Userclans', [
            'foreignKey' => 'user_id',
        ]);

        $this->hasOne('Usermatches', [
            'foreignKey' => 'user_id',
        ]);

        $this->hasOne('Banks', [
            'foreignKey' => 'user_id',
        ]);

        $this->hasMany('Userclankickeds', [
            'foreignKey' => 'user_id',
        ]);

        $this->hasMany('Finances', [
            'foreignKey' => 'user_id',
        ]);

        $this->hasMany('Friendsecondusers', [
            'UsersTable' => 'Friends',
            'foreignKey' => 'user_id',
        ]);

        $this->hasMany('Friendrequesterusers', [
            'UsersTable' => 'Friends',
            'foreignKey' => 'user_id',
        ]);
    }

    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('phone_number')
            ->maxLength('phone_number', 20)
            ->allowEmptyString('phone_number');

        $validator
            ->scalar('unique_id_sms')
            ->maxLength('unique_id_sms', 32)
            ->allowEmptyString('unique_id_sms');

//        $validator
//            ->scalar('gmail_account')
//            ->maxLength('gmail_account', 255)
//            ->requirePresence('gmail_account', 'create')
//            ->notEmptyString('gmail_account');

        $validator
            ->scalar('name')
            ->maxLength('name', 30)
            ->allowEmptyString('name');

        $validator
            ->scalar('uuid')
            ->maxLength('uuid', 255)
            ->allowEmptyString('uuid');

        $validator
            ->integer('coin')
            ->requirePresence('coin', 'create')
            ->notEmptyString('coin');

        $validator
            ->integer('xp')
            ->notEmptyString('xp');

        $validator
            ->integer('vip')
            ->notEmptyString('vip');

        $validator
            ->integer('mute')
            ->notEmptyString('mute');

        $validator
            ->nonNegativeInteger('in_game_chat')
            ->notEmptyString('in_game_chat');

        $validator
            ->notEmptyString('promote_clan');

        $validator
            ->nonNegativeInteger('dailytrophy')
            ->notEmptyString('dailytrophy');

        $validator
            ->nonNegativeInteger('weeklytrophy')
            ->notEmptyString('weeklytrophy');

        $validator
            ->nonNegativeInteger('totaltrophy')
            ->notEmptyString('totaltrophy');

        $validator
            ->notEmptyString('colour4');

        $validator
            ->notEmptyString('colour6');

        $validator
            ->scalar('dice')
            ->maxLength('dice', 30)
            ->notEmptyString('dice');

        $validator
            ->scalar('symbol')
            ->maxLength('symbol', 30)
            ->notEmptyString('symbol');

        $validator
            ->scalar('ring')
            ->maxLength('ring', 30)
            ->notEmptyString('ring');

        $validator
            ->integer('spintime')
            ->notEmptyString('spintime');

        $validator
            ->notEmptyString('spinvideo');

        $validator
            ->boolean('telegram_claimed')
            ->notEmptyString('telegram_claimed');

        $validator
            ->boolean('instagram_claimed')
            ->notEmptyString('instagram_claimed');

        $validator
            ->scalar('language')
            ->notEmptyString('language');

        $validator
            ->integer('registerdate')
            ->requirePresence('registerdate', 'create')
            ->notEmptyString('registerdate');

//        $validator
//            ->integer('lastlogindate')
//            ->requirePresence('lastlogindate', 'create')
//            ->notEmptyString('lastlogindate');

        $validator
            ->integer('promotiondate')
            ->notEmptyString('promotiondate');

        $validator
            ->notEmptyString('ads_watched');

        $validator
            ->scalar('promotion_pack')
            ->maxLength('promotion_pack', 40)
            ->allowEmptyString('promotion_pack');

        $validator
            ->notEmptyString('tutorial_step');

        $validator
            ->integer('tutorial_item')
            ->notEmptyString('tutorial_item');

        $validator
            ->boolean('tutorial_finish')
            ->notEmptyString('tutorial_finish');

        $validator
            ->scalar('ip')
            ->maxLength('ip', 25)
            ->requirePresence('ip', 'create')
            ->notEmptyString('ip');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->add($rules->existsIn(['group_id'], 'Groups'));

        return $rules;
    }

    /**
     * @param $email
     * @return string|null
     * @version 1.0.0
     */
    private function obfuscate_email($email)
    {
        if ($email) {
            $em = explode("@", $email);
            $name = implode('@', array_slice($em, 0, count($em) - 1));
            $len = floor(strlen($name) / 2);

            return substr($name, 0, $len) . str_repeat('*', $len) . "@" . end($em);
        } else {
            return NULL;
        }

    }

    /**
     * @param $phoneNumber
     * @return string|null
     * @version 1.0.0
     */
    private function obfuscate_phone($phoneNumber)
    {
        if ($phoneNumber) {
            $middle_string = "";
            $length = strlen($phoneNumber);
            if ($length < 3) {

                echo $length == 1 ? "*" : "*" . substr($phoneNumber, -1);

            } else {
                $part_size = floor($length / 3);
                $middle_part_size = $length - ($part_size * 2);
                for ($i = 0; $i < $middle_part_size; $i++) {
                    $middle_string .= "*";
                }
            }

            return substr($phoneNumber, 0, $part_size) . $middle_string . substr($phoneNumber, -$part_size);
        } else {
            return NULL;
        }
    }

    /**
     * @param $userId
     * @param bool $public
     * @param bool $avgWinRank
     * @return array|mixed
     * @version 1.0.0
     */
    public function userInfo($userId, $public = true, $avgWinRank = false)
    {
        $isPublic = $public == TRUE ? 'True' : 'False';
        $userInfo = Cache::read("$userId._.$isPublic", 'userInfo');

        if (!$userInfo) {
            $userInfo = $this->get($userId);


            /* cancel VIP items:
             * if value of $userInfo->vip:
             * 0: not vip user
             * time(int): is vip user
             */
            $time = time();
            $Shopitems = TableRegistry::getTableLocator()->get('Shopitems');
            if ($userInfo->vip != 0 && $userInfo->vip < $time) {


                $vipItems = $Shopitems->getItemByCurrency('Vip');

                foreach ($vipItems as $vipItem) {
                    $itemtypeVip = $vipItem['itemtype'];
                    $storeidVip = $vipItem['storeid'];

                    //Default items
                    switch ($itemtypeVip) {
                        case 'Dice':
                            if ($userInfo->dice == $storeidVip) {
                                $userInfo->dice = 'dice_1';
                            }
                        case 'Symbol':
                            if ($userInfo->symbol == $storeidVip) {
                                $userInfo->symbol = 'symbol_1';
                            }
                        case 'Ring':
                            if ($userInfo->ring == $storeidVip) {
                                $userInfo->ring = 'ring_44';
                            }
                        default:
                            break;
                    }
                }
                $userInfo->vip = 0;
                if ($userInfo->isDirty()) {
                    $userInfo = $this->saveOrFail($userInfo);
                }
            }

            /*
            *   1.6 =< data               =>pro
            *   1.6 < data < 2.4          =>intermediate
            *    data >= 2.4               =>beginner
            */
            $userInfo = $userInfo->toArray();
            if ($avgWinRank) {
                $userInfo['avg_win_rank'] = $this->Usermatches->avgWinRank($userId);
            }

            if ($public) {
                unset($userInfo['password']);
                unset($userInfo['ip']);
                $userInfo['gmail_account'] = $this->obfuscate_email($userInfo['gmail_account']);
                $userInfo['phone_number'] = $this->obfuscate_phone($userInfo['phone_number']);
                unset($userInfo['uuid']);
                unset($userInfo['unique_id']);
            }

            Cache::write("$userId._.$isPublic", $userInfo, 'userInfo');
        }

        return $userInfo;
    }

    /**
     * @param $userId
     * @param $length
     * @param $start
     * @param $scoreType
     * @version 1.0.0
     */
    public function ranking($userId, $length, $start, $scoreType)
    {
        $userinfo = $this->userInfo($userId);
        $userScore = (int)$userinfo[$scoreType];
        $rankCount = $this->find()->where(["$scoreType >" => $userScore])->count();

        $users = $this->find()
            ->select($this::UserFields)
            ->where(["$scoreType >" => 0])
            ->order(["$scoreType DESC", "Users.id" => "asc"])
            ->limit($length)
            ->offset($start)
            ->disableHydration()
            ->toArray();

        return ['list' => $users, 'userRank' => $rankCount + 1];
    }

    /**
     * @param array $data
     * @version 1.0.0
     *
     */
    public function updateUserInfo($data = array())
    {

        $defaults = array(
            'user_id' => 0,
            'match_id' => 0,
            'coin' => 0,
            'xp' => 0,
            'vip' => 0,
            'mute' => 0,
            'in_game_chat' => 0,
            'promote_clan' => 0,
            'dailytrophy' => 0,
            'weeklytrophy' => 0,
            'totaltrophy' => 0,
            'ads_watched' => 0,
            'service' => '-',
            'description' => '-'
        );

        $data = array_merge($defaults, $data);
        $time = time();
        extract($data);

        $User = $this->get($user_id);
        $User->coin = $User->coin + $coin;
        $User->xp = $User->xp + $xp;
        $User->dailytrophy = $User->dailytrophy + $dailytrophy;
        $User->weeklytrophy = $User->weeklytrophy + $weeklytrophy;
        $User->totaltrophy = $User->totaltrophy + $totaltrophy;
        $User->ads_watched = $User->ads_watched + $ads_watched;


        if ($vip > 0) {
            $User->vip = $time + $vip;
            $clanInfo = $this->Userclans->getClanByUser($user_id);

            if ($clanInfo) {
                $clanId = $clanInfo['id'];
                Cache::delete("clan_$clanId", 'veryshort');
            }
        }

        if ($in_game_chat > 0)
            $User->in_game_chat = $time + $in_game_chat;

        if ($mute > 0)
            $User->mute = $time + $mute;

        if ($promote_clan > 0) {
            $User->promote_clan = $User->promote_clan + $promote_clan;
        } elseif ($promote_clan == -1) {
            $User->promote_clan = 0;
        }

//        if ($this->save($User))
//            throw new Exception('err');
//
        try {
            $this->saveOrFail($User);
        } catch (\Cake\ORM\Exception\PersistenceFailedException $e) {
            echo $e->getEntity();
        }

        Cache::delete("$user_id._.False", 'userInfo');
        Cache::delete("$user_id._.True", 'userInfo');

        if ($coin != 0) {

            $userInfo = $this->userInfo($user_id);
            $userCoin = $userInfo['coin'];
            $type = $coin > 0 ? 'income' : 'charge';

            $financeData = array(
                'user_id' => $user_id,
                'match_id' => $match_id,
                'amount' => abs($coin),
                'newamount' => $userCoin,
                'currency' => 'coin',
                'service' => $service,
                'description' => $description,
                'type' => $type,
                'time' => time()
            );
        }

        if (isset($financeData)) {
            $financeEntity = $this->Finances->newEntity($financeData);

            try {
                $this->Finances->saveOrFail($financeEntity);
            } catch (\Cake\ORM\Exception\PersistenceFailedException $e) {
                echo $e->getEntity();
            }

//            if (!$this->Finances->save($financeEntity))
//                throw new Exception('err');
        }
    }


    /**
     * @param $userId
     * @param $storeName
     * @return |null
     * @version 1.0.0
     */
    public function hasPromotion($userId, $storeName)
    {
        
        $time = time();
        $userInfo = $this->userInfo($userId);
        $registerTime = $userInfo['registerdate'];
        $promotionTime = $userInfo['promotiondate'];
        $itemStoreId = $userInfo['promotion_pack'];
        $assignPromotion = false;
        $pack = NULL;

        if ($registerTime + 1 * 3600 * 24 < $time and $registerTime + 3 * 3600 * 24 > $time and $promotionTime < $time and $itemStoreId != 'promotion_pack_2') {

            $itemStoreId = 'promotion_pack_1';
            $assignPromotion = true;
        } elseif ($registerTime + 3 * 3600 * 24 < $time and $registerTime + 5 * 3600 * 24 > $time and $promotionTime < $time and $itemStoreId != 'promotion_pack_1') {

            $itemStoreId = 'promotion_pack_2';
            $assignPromotion = true;
        } elseif ($registerTime + 5 * 3600 * 24 < $time and $registerTime + 7 * 3600 * 24 > $time and $promotionTime < $time and $itemStoreId != 'promotion_pack_3') {

            $itemStoreId = 'promotion_pack_3';
            $assignPromotion = true;
        } elseif ($registerTime + 7 * 3600 * 24 < $time and $registerTime + 10 * 3600 * 24 > $time and $promotionTime < $time and $itemStoreId != 'promotion_pack_4') {

            $itemStoreId = 'promotion_pack_4';
            $assignPromotion = true;
        } elseif ($registerTime + 10 * 3600 * 24 < $time and $promotionTime < $time and $itemStoreId != 'promotion_pack_4' and 20 > rand(1, 100)) {

            $itemStoreId = 'promotion_pack_4';
            $assignPromotion = true;
        }

        $item = $this->Banks->findItem($userId, $itemStoreId);//user bought this pack before

        if ($assignPromotion and $item == NULL) {
            $promotionTime = $time + 3600 * 24;
            $this->updateAll([
                'promotion_pack' => $itemStoreId,
                'promotiondate' => $promotionTime
            ], ['id' => $userId]);

            Cache::delete("$userId._.False", 'userInfo');
            Cache::delete("$userId._.True", 'userInfo');
        }

        if ($promotionTime > $time) {
            $Shopitems = TableRegistry::getTableLocator()->get('Shopitems');
            $pack = $Shopitems->getItem($itemStoreId, $storeName);
        }

        return $pack;
    }


    //run by cronjob every weekend (Thursday-ّ-Friday 4-5)

    /**
     * @version 1.0.0
     */
    public function promotionPackSetter()
    {

        $itemStoreId = 'promotion_pack_5';


        $Kavenegar = new Kavenegar();
        $Fcm = new Fcm();

        $time = time();
        $promotionTime = $time + 3600 * 24;

        //means 10 days before till now
        $startTime = $time - 3600 * 24 * 10;
        $endTime = $time;

        $users = $this->find()
            ->where(['promotiondate <' => $time])
            ->where(function (QueryExpression $exp) use ($startTime, $endTime) {
                return $exp->between('lastlogindate', $startTime, $endTime);
            })
            ->limit(100)
            ->disableHydration()
            ->all();

        foreach ($users as $user) {

            $userId = $user['id'];
            $name = $user['name'];
            $phoneNumber = $user['phone_number'];
            $uuid = $user['uuid'];
            $lastlogindate = $user['lastlogindate'];

            $nowHour = gmdate("H", $time);
            $lastLoginHour = gmdate("H", $lastlogindate);

            if ($lastLoginHour <= $nowHour + 2) {

                if ($this->Banks->buyFromStore($userId)) {
                    $itemStoreId = 'promotion_pack_1';
                    $Kavenegar->send($phoneNumber, $name, 'Promotion');
                }

                if ($uuid) {
                    $pushData[] = array(
                        'userId' => $userId,
                        'teamName' => $name,
                        'tipsy' => 'promotion',
                        'page' => 'shop',
                        'customData' => $itemStoreId,
                    );
                    $Fcm->sendPushNotification($pushData);

                } elseif ($phoneNumber) {
                    $Kavenegar->send($phoneNumber, $name, 'Promotion');
                }

                $this->updateAll(['promotion_pack' => $itemStoreId, 'promotiondate' => $promotionTime], ['id' => $userId]);
                Cache::delete("$userId._.False", 'userInfo');
                Cache::delete("$userId._.True", 'userInfo');

            }

        }

    }

    /**
     * @param $trophy
     * @version 1.0.0
     */
    public function resetTrophy($trophy)
    {
        $this->updateAll([$trophy => 0],[]);
        Cache::clearGroup('user');
    }


    /**
     * @version 1.0.0
     */
    public function resetAdsWatched()
    {
        $this->updateAll(["ads_watched" => 0],[]);
        Cache::clearGroup('user');
    }

    /**
     * @param $scoreType
     * @return ResultSetInterface|\Cake\Datasource\ResultSetInterface|Query
     * @version 1.0.0
     */
    public function whoIsWinners($scoreType)
    {
        return $this->find()
            ->select(['id', 'weeklytrophy', 'dailytrophy', 'totaltrophy'])
            ->where(["$scoreType >" => 0])
            ->order(["$scoreType DESC"])
            ->limit(3)
            ->offset(0)
            ->disableHydration()
            ->all();

    }

    /**
     * @param $start
     * @param $length
     * @param $userIdentifier
     * @return array
     * @version 1.0.0
     */
    public function searchUser($start = null, $length = null, $userIdentifier)
    {
        if (filter_var($userIdentifier, FILTER_VALIDATE_INT)) {
            $condition = ['id' => $userIdentifier];
        } else {
            $condition = ['name like' => '%' . $userIdentifier . '%'];
        }
        $result = $this->find('all')
            ->where($condition)
            ->select($this::UserFields)
            ->disableHydration();

        $count = $result->count();
        $users = $result->limit($length)->offset($start)->all();

        return ['count' => $count, 'list' => $users];
    }

    /**
     * @param $startTime
     * @param $endTime
     * @return mixed
     * @version 1.0.0
     */
    public function activeUser($startTime, $endTime)
    {
        $users = $this->find()
            ->where(function (QueryExpression $exp) use ($startTime, $endTime) {
                return $exp->between('lastlogindate', $startTime, $endTime);
            })->select(['id'])
            ->disableHydration()
            ->all()
            ->toArray();

        return Hash::extract($users, '{n}.id');
    }


    /**
     * Churn rate, in its broadest sense, is a measure of the number of individuals or items moving out of a collective group over a specific period
     * @param $startTime
     * @param $endTime
     * @todo must fixed
     * @todo no return?!
     */
    public function churnRate($startTime, $endTime)
    {
        // number of churned customers / total number of customers
        return ['مشکل دارد'];
        $time = time() + 7 * 3600;

        //customer lost in a given period
        $usersLost = $this->find('count', array(
                'conditions' => array(
                    "User.registerdate BETWEEN ? AND ?" => array($startTime, $endTime),
                    "User.lastlogindate <" => $time,
                ),
                'fields' => array('User.id'),
                'recursive' => -1
            )
        );

        $usersRegister = $this->find('count', array(
                'conditions' => array(
                    "User.registerdate BETWEEN ? AND ?" => array($startTime, $endTime)
                ),
                'fields' => array('User.id'),
                'recursive' => -1
            )
        );

        //customer we have at the start of period
        $totalUsersRemain = $this->find('count', array(
                'conditions' => array("User.registerdate <" => $startTime),
                'fields' => array('User.id'),
                'recursive' => -1
            )
        );


    }

}
