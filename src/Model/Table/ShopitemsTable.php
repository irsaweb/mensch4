<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Shopitems Model
 *
 * @method \App\Model\Entity\Shopitem get($primaryKey, $options = [])
 * @method \App\Model\Entity\Shopitem newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Shopitem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Shopitem|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Shopitem saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Shopitem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Shopitem[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Shopitem findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Shopitem newEmptyEntity()
 * @method \App\Model\Entity\Shopitem[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Shopitem[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Shopitem[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Shopitem[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ShopitemsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('shopitems');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): \Cake\Validation\Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('itemtype')
            ->requirePresence('itemtype', 'create')
            ->notEmptyString('itemtype');

        $validator
            ->scalar('itemname')
            ->maxLength('itemname', 100)
            ->requirePresence('itemname', 'create')
            ->notEmptyString('itemname');

        $validator
            ->scalar('storeid')
            ->maxLength('storeid', 100)
            ->requirePresence('storeid', 'create')
            ->notEmptyString('storeid');

        $validator
            ->integer('coin')
            ->notEmptyString('coin');

        $validator
            ->notEmptyString('more');

        $validator
            ->requirePresence('discount', 'create')
            ->notEmptyString('discount');

        $validator
            ->integer('price')
            ->requirePresence('price', 'create')
            ->notEmptyString('price');

        $validator
            ->integer('priceafterdiscount')
            ->requirePresence('priceafterdiscount', 'create')
            ->notEmptyString('priceafterdiscount');

        $validator
            ->scalar('currency')
            ->requirePresence('currency', 'create')
            ->notEmptyString('currency');

        $validator
            ->scalar('storename')
            ->requirePresence('storename', 'create')
            ->notEmptyString('storename');

        $validator
            ->notEmptyString('consumable');

        $validator
            ->notEmptyString('newitem');

        $validator
            ->notEmptyString('active');

        $validator
            ->requirePresence('sort', 'create')
            ->notEmptyString('sort');

        return $validator;
    }


    /**
     * @param $productId
     * @param $storename
     * @return \Cake\Datasource\EntityInterface|array|false
     * @version 1.0.0
     */
    public function getItem($storeId, $storename)
    {
        $records = $this->find()->where(['storeid' => $storeId, 'storename' => $storename, 'active' => 1])->first();
        if ($records === null) {
            return false;
        }
        return $records;
    }

    /**
     * @param $currency
     * @return array|\Cake\Datasource\ResultSetInterface|\Cake\ORM\Query
     * @version 1.0.0
     */
    public function getItemByCurrency($currency)
    {
        return $this->find()->where(['currency' => $currency, 'active' => 1])->all();
    }
}
