<?php

namespace App\Lib;

use Cake\Core\Configure;
use Cake\Http\ServerRequest;
use Cake\Routing\Router;

class ChangeLang
{

    public $start_lg;
    public $use_session = true;
    /**
     * Instance of a Session object relative to this request
     *
     * @var \Cake\Http\Session
     */
    protected $Session;


    public function __construct()
    {
        $this->Session = Router::getRequest()->getSession();
    }

    public function startToggleLg($lg)
    {

        if ($this->Session->check("Config.language")) {
            $this->start_lg = $this->Session->read("Config.language");
            $this->use_session = TRUE;
        } else {
            $this->start_lg = Configure::read("Config.language");
        }

        if ($this->use_session) {
            $this->Session->write("Config.language", $lg);
        } else {
            Configure::write("Config.language", $lg);
        }

    }

    public function stopToggleLg($lg = NULL)
    {

        $lg = ($lg) ? $lg : $this->start_lg;

        if ($this->use_session) {
            $this->Session->write("Config.language", $lg);
        } else {
            Configure::write("Config.language", $lg);
        }

    }
}

?>
