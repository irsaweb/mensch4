<?php

namespace App\Lib;



use Exception;

class NoaEncryption
{

    public $pubkey = "-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCxboqChjK86OWQi+FfyGbxSRsF
pk0UkDCr2/vOkdX5trPCLahwY3ANHYrmADjxTNh3AH1SDJGTQoDy9Npkwjy73d5O
gjfH+gc1YwIS3W42dsfcBKLzb2r2pRjbub9se4o2rYSqk7E4XX/p/mPJIwi8emY0
LtI/qR6nxvEWO0jUowIDAQAB
-----END PUBLIC KEY-----";
    public $privkey = '-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQCxboqChjK86OWQi+FfyGbxSRsFpk0UkDCr2/vOkdX5trPCLahw
Y3ANHYrmADjxTNh3AH1SDJGTQoDy9Npkwjy73d5OgjfH+gc1YwIS3W42dsfcBKLz
b2r2pRjbub9se4o2rYSqk7E4XX/p/mPJIwi8emY0LtI/qR6nxvEWO0jUowIDAQAB
AoGAO19MBgU18TTVrJrCHbUHjXHzEt7lKB5fywJfrm3qdhcG69Evbfw4QItEMVad
t1EJljiZttzx0Z8NWz9H3TOJ8pyLM7y7Es6H8hXcQfLrIvxeSu61nr3ixt4vvjdV
Vrxa74O1Xgwq4jKyfXpgn9edE0JJ4U1le77wzWo8f0oVH1ECQQDjUawy+ak6zTOl
UTTuQYxfkKPg89Y2xQRzWWWeB+F2WRu1wRcCkrLVw4c3LuOZlEyimP7gsPWdgQ9p
qBvbf0W9AkEAx9GHHG4ahpnutyZT9ooZq/3FPK6nUxsjp8iD6sltEdOOLYWXzFD+
WxyYywLQkYuoL855RtwlOOEKYQuCyLQ53wJACext1QfbIGsG3yetZN1NsHpS/SYH
6vos2JBFhGGZsR3wvL2CMKWQ0w4nH/yXCQRVWB/PuKVhA5A3yQgCt1FWDQJBALqG
a+JtIVPOs9jB7ZTxmz9C9/BKdSWN6v2/nuz3tFn0qv7xex8d6CIzEqpHaVzECm7+
BZXRAOoUoHSwoxgprO8CQQDMKo9VHThufwmGaXt24u+pNEmhSUHpAAt/ViT3z3X8
Jpf6+yhRc64a8ik1stv2ACMMufSEvnxJBnkPLo5fDS/g
-----END RSA PRIVATE KEY-----';

    /**
     * @param $data
     * @return string
     */
    public function encrypt($data)
    {
        if (openssl_public_encrypt($data, $encrypted, $this->pubkey, OPENSSL_PKCS1_OAEP_PADDING))
            $data = base64_encode($encrypted);
        else
            throw new Exception('Unable to encrypt data. Perhaps it is bigger than the key size?');

        return $data;
    }

    /**
     * @param $data
     * @return string
     */
    public function decrypt($data)
    {
        if (openssl_private_decrypt(base64_decode($data), $decrypted, $this->privkey, OPENSSL_PKCS1_OAEP_PADDING))
            $data = $decrypted;
        else
            $data = '';

        return $data;
    }
}
