<?php

namespace App\Lib;


use Cake\Datasource\FactoryLocator;
use App\Lib\ChangeLang;
use Cake\Routing\Router;

/**
 * Class Fcm
 * @package App\Lib
 * @version 1.0.0
 */
class Fcm
{

    /**
     * @param $registration_ids
     * @param $fcmData
     */
    public function send($registration_ids, $fcmData)
    {


        $fields = [
            'registration_ids' => $registration_ids,
            "notification" => [
                "title" => $fcmData['title'],
                "body" => $fcmData['content'],
                "android_channel_id" => $fcmData['androidChannelId'],
                "icon" => 'smallnotif',
                //"color"  => 'green',
                "sound" => 'default',
                "priority" => 'high',
            ],
            "data" => $fcmData['customData']

        ];

        $data = json_encode($fields);

        shell_exec('cd /var/www/clients/client0/web3/private/mensch/app && Console/cake curl send ' . $data . ' >log1 2>error1 &');

    }


    /**
     * $userId,$teamName,$event,$customData=NULL
     * @param $pushDatas
     */
    public function sendPushNotification($pushDatas)
    {
        $usersTable = FactoryLocator::get('Table')->get('Users');
        $language = new ChangeLang();

        //todo جهت تست و عدم تولید خطا این رو گذاشتم
        $userUuidTo = null;

        foreach ($pushDatas as $pushData) {

            $userId = $pushData['userId'];
            $event = $pushData['tipsy'];
            $page = $pushData['page'];
            $customData = $pushData['customData'];
            $teamName = $pushData['teamName'];


            $userInfo = $usersTable->userInfo($userId, null, false);

            $userUuidTo = $userInfo['uuid'];
            $userLang = $userInfo['language'];
            $language->startToggleLg("$userLang");   //change language on the fly

            switch ($event) {

                case 'friend request':
                    $pushMessage = [
                        'androidChannelId' => 'friendRequest',
                        'title' => __('friendRequest'),
                        'content' => __('%s  wants to be friend with you.Do you accept ?', h($teamName)),
                    ];
                    break;

                case 'refund match':
                    $pushMessage = [
                        'androidChannelId' => 'matchRefund',
                        'title' => __('match refund entrance fee'),
                        'content' => __('%s match refunded as it has been canceled', h($teamName)),
                    ];
                    break;

                case 'friendlyMatch created':
                    $pushMessage = [
                        'androidChannelId' => 'friendlyMatch',
                        'title' => __('Friendly Match Created'),
                        'content' => __('%s create a friendly match do you wanna join?', h($teamName)),
                    ];
                    break;

                case 'friendlyMatch join':
                    $pushMessage = [
                        'androidChannelId' => 'friendlyMatch',
                        'title' => __('User joined the friendly match'),
                        'content' => __('%s joined the friendly match', h($teamName)),
                    ];
                    break;

                case 'friendlyMatch expired':
                    $pushMessage = [
                        'androidChannelId' => 'friendlyMatch',
                        'title' => __('Match creator leave the match'),
                        'content' => __('Match creator leave the match, so match cancelled', h($teamName)),
                    ];
                    break;

                case 'friendlyMatch leave':
                    $pushMessage = [
                        'androidChannelId' => 'friendlyMatch',
                        'title' => __('User leaved friendly match'),
                        'content' => __('%s leaved the friendly match', h($teamName)),
                    ];
                    break;

                case 'promotion':
                    $pushMessage = [
                        'androidChannelId' => 'promotion',
                        'title' => __('promotion title'),
                        'content' => __('%s check the shop you have promotion', h($teamName)),
                    ];
                    break;

            }

        }
        //todo $userUuidTo generates an error when $pushDatas is empty
        if ($userUuidTo != NULL) {
            $fcmUserIdTos[] = $userUuidTo;
            $fcmDatas = array(
                'title' => $pushMessage['title'],
                'content' => $pushMessage['content'],
                'androidChannelId' => $pushMessage['androidChannelId'],
                'customData' => $customData
            );
        }

        if (isset($fcmUserIdTos))
            $this->send($fcmUserIdTos, $fcmDatas);
    }

    /**
     * @return array[]
     */
    public function notficationChannelIds()
    {

        $notificationChannelIds = [
            '0' => [
                'id' => 'friendRequest',
                'name' => __('friend request channel title'),
                'desc' => __('friend request channel desc '),
                'importance' => 3
            ],
            '1' => [
                'id' => 'friendlyMatch',
                'name' => __('friendly match channel title'),
                'desc' => __('friendly match channel desc'),
                'importance' => 3
            ],
            '2' => [
                'id' => 'Match',
                'name' => __('match channel title'),
                'desc' => __('match channel desc'),
                'importance' => 3
            ]
        ];

        return $notificationChannelIds;
    }

}
