<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         1.0.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Lib\NoaAuth;

use App\Lib\NoaEncryption;
use Authentication\Identifier\AbstractIdentifier;
use Authentication\Identifier\Resolver\ResolverAwareTrait;
use Authentication\PasswordHasher\PasswordHasherTrait;
use Cake\Datasource\FactoryLocator;
use Cake\ORM\TableRegistry;

/**
 * Token Identifier
 */
class TokenIdentifier extends AbstractIdentifier
{
    use ResolverAwareTrait;

    /**
     * Default configuration.
     * - `fields` The fields to use to identify a user by:
     *   - `username`: one or many username fields.
     *   - `password`: password field.
     * - `resolver` The resolver implementation to use.
     * - `passwordHasher` Password hasher class. Can be a string specifying class name
     *    or an array containing `className` key, any other keys will be passed as
     *    config to the class. Defaults to 'Default'.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'fields' => [
            self::CREDENTIAL_USERNAME => 'usertoken'
        ],
        //    'resolver' => 'Authentication.Orm',
        'passwordHasher' => null,
    ];

    /**
     * @inheritdoc
     */
    public function identify(array $data)
    {
        $tokenField = $this->getConfig('fields')[self::CREDENTIAL_USERNAME];

        if (!isset($data[$tokenField])) {
            return null;
        }

        $token = $this->_decodeToken($data[$tokenField]);

        $identity = $this->_findIdentity($token);
        return $identity;
    }

    /**
     * @param $token
     * @return mixed
     */
    protected function _findIdentity($token)
    {
        /** @var \App\Model\Table\UsersTable $Users */
        $Users = FactoryLocator::get('Table')->get('Users');
        $user = $Users->find()->where(['unique_id' => $token])
            ->contain(['Groups'])
            //->disableHydration()
            ->first()->toArray();
        unset($user['group']['created']);
        unset($user['group']['modified']);
        return $user;
    }

    /**
     * @param $token
     * @return string
     */
    protected function _decodeToken($token)
    {
        $encryption = new \App\Lib\NoaEncryption;
        $clienIP = \Cake\Routing\Router::getRequest()->clientIp();

        if (in_array($clienIP, ['127.0.0.1', '::1'])) {
            $token = $encryption->encrypt($token);
        }

        if ($this->_isBase64($token)) {
            return $encryption->decrypt($token);
        }
        return null;
    }


    /**
     * @param $token
     * @return bool
     */
    protected function _isBase64($token)
    {
        return (bool)preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $token);
    }

}
