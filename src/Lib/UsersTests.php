<?php


namespace App\Lib;


class UsersTests
{
    static $debug = true;

    static function __updateCustomInfo($object)
    {
        return $object->testPostAction($object, 'updateCustomInfo', [
            'storeName' => 'inapp',
            'colour4' => 0,
            'colour6' => 2,
            'symbol' => 'symbol_12',
            'dice' => 'dice_12',
            'ring' => 'ring_35'
        ], static::$debug);
    }


    static function buyForUser($object)
    {
        return $object->testPostAction($object, 'buyForUser', [
            'count' => 1,
            'storeIds' => 'dice_5',
            'storeName' => 'inapp'
        ], static::$debug);

    }

    static function userReport($object)
    {
        return $object->testPostAction($object, 'userReport', [
            'startTime' => 1582852587,
            'endTime' => 1582872587,
            'startDay' => 16,
            'startMonth' => 03,
            'endDay' => 16,
            'endMonth' => 03
        ], static::$debug);

    }

}
