<?php

namespace App\Lib;

use \Kavenegar\KavenegarApi;

class Kavenegar
{


    private $apiKey = '4C625A2B6D6C397A7134664C426B424F666E4549674D3762794E64663177766778657065453962615A79513D';


    /**
     * @param $receptor
     * @param $token
     * @param $template
     * @return mixed
     */
    public function send($receptor, $token, $template)
    {
        $api = new KavenegarApi($this->apiKey);
        $result = $api->VerifyLookup($receptor, $token, null, null, $template);
        return $result;
    }
}
