<?php

namespace App\Controller;

/**
 * @property \App\Model\Table\UserclankickedsTable $Userclankickeds
 */
class UserclankickedsController extends AppController
{

    /**
     * @param $calanId
     * @version 1.0.0
     */
    public function getKickedUserclans()
    {

        $clanId = $this->request->getData("clanId");
        $userClans = $this->Userclankickeds->getKickedUserClan($clanId);
        return $this->toJsonResponse($userClans);

    }

    public function test()
    {
        $res = $this->Userclankickeds->getKickedUserClan(47);
        //$res = $this->Userclankickeds->addToKickList(12, 47);

        pj($res);
    }
}
