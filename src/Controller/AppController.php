<?php
declare(strict_types=1);

namespace App\Controller;

use App\Controller\Component\Logger;
use Cake\Controller\Controller;
use Cake\Event\EventInterface;
use Cake\I18n\I18n;
use Cake\ORM\TableRegistry;


/**
 * @property \App\Model\Table\UsersTable $Users
 */
class AppController extends Controller
{
    /**
     * @var array
     */
    public $userInfo = [];
    protected $debugOutPut = [];

    /**
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Authentication.Authentication');
        $this->loadComponent('Acl.Acl');

    }

    /**
     * beforeFilter
     *
     * @param mixed $event
     * @return void
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['display']);
        $AuthStatus = $this->Authentication->getResult();
        if ($AuthStatus->isValid()) {
            $this->setUserInfo();
        }
    }


    /**
     * toJsonResponse
     *
     * render to json output response
     *
     * @param mixed $data
     * @param mixed $status
     * @param mixed $message
     * @return \Cake\Http\Response
     */
    public function toJsonResponse($data = NULL, $status = 'success', $message = 'Done', $additions = false)
    {
        $response = array(
            'code' => 200,
            'status' => $status,
            'data' => $data,
            'message' => __($message)
        );
        if ($additions and is_array($additions)) {
            $response = $response + $additions;
        }
        $this->debugOutPut = $response;
        //$json = json_encode($response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );
        //return $this->response->withType('application/json')->withStringBody($json);
        $json = json_encode($response);
        return $this->response->withStringBody($json);
    }

    /**
     * @param $Object
     * @param $method
     * @param array $ReqestParams
     * @param bool $debug
     * @return \Cake\Http\Response|mixed|null
     */
    public function testPostAction($Object, $method, $ReqestParams = [], $debug = true)
    {
        if (method_exists($Object, $method)) {

            $this->request = $this->request->withParsedBody($ReqestParams)->withMethod('POST');
            $res = call_user_func([$Object, $method]);

            if ($debug) {
                if ($res instanceof $this->response) {
                    pr($this->debugOutPut);
                } else {
                    pr($res);
                }

                return $this->render('\pages\test');
            };
            return $res;
        }
    }

    /**
     * setUserInfo
     *
     * @return void
     */
    protected function setUserInfo()
    {
        $user = $this->request->getSession()->read('Auth');
        if (empty($user)) {
            throw new Exception('yek moshkel dar auth hast');
        }
        $this->userInfo['userId'] = $user['id'];
        $this->userInfo['groupID'] = $user['group_id'];
        //$this->userInfo['role'] = $user['group']['name'];
        $this->userInfo['language'] = $user['language'];

        //todo bayad ina update beshand?

        // if(isset($this->userInfo['userId']))
        //     $this->User->updateAllUnjoined(array('lastlogindate'=>time()),array('id'=>$this->userInfo['userId']));

    }


    /**
     * add userInfo to ajax response
     * @version 1.0.0
     */
    protected function addUserinfo($response = [])
    {
        if (isset($response)) {
            $userId = $this->userInfo['userId'];
            $Users = TableRegistry::getTableLocator()->get('Users');
            $response['userInfo'] = $Users->userInfo($userId);
            $response['serverTime'] = time();
        }

        return $response;
    }
}
