<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Cache\Cache;

/**
 * Hacks Controller
 *
 * @method \App\Model\Entity\Hack[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HacksController extends AppController
{


    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Users');
    }

    protected function toResponse($data)
    {
        return $this->response->withType('application/json')->withStringBody(json_encode($data));
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function userChange()
    {

        $user = $this->Users->get($this->userInfo['userId']);
        $user_id= $this->userInfo['userId'];
        $this->Users->patchEntity($user, $this->request->getData());
        $this->Users->save($user);
        Cache::delete("$user_id._.False", 'userInfo');
        Cache::delete("$user_id._.True", 'userInfo');
        $this->redirect('/hacks/userReport');

    }

    public function userReport()
    {
        $field = [
            'id',
            "group_id",
            "unique_id",
            "phone_number",
            "unique_id_sms",
            "gmail_account",
            "coin",
            "xp",
            "vip",
            "mute",
            "promote_clan",
            "dailytrophy",
            "weeklytrophy",
            "totaltrophy",
        ];

        $res = $this->Users->get($this->userInfo['userId'], ['fields' => $field]);

        return $this->toResponse($res);
    }


}
