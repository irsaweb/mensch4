<?php

namespace App\Controller;

use Cake\Event\Event;

/**
 * @property \App\Model\Table\FinancesTable $Finances
 */
class FinancesController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
    }

    /**
     * @param Event $event
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['duplicateCreateMatch']);

    }

    /**
     * @version 1.0.0
     * @todo ro system man run nashood
     */
    public function duplicateCreateMatch()
    {
        $startH = $this->request->getData('startH');
        // in jayyee estefade nashode
        $endH = $this->request->getData('endH');
        $d = $this->request->getData('day');

        $startTime = strtotime(date("Y-m-$d $startH:00:00"));
        $endTime = strtotime(date("Y-m-$d $endH:00:00"));


        ini_set('memory_limit', '8048M');
        set_time_limit(0);

        $data = NULL;

        do {
            $refundFinances = $this->Finances->find()
                ->where([
                    'service' => 'entrance_fee',
                    'time >=' => $startTime,
                    'time <=' => $startTime + 60,
                ])->all()->toArray();

            $refundFinances = $this->Finances->find('all')->toArray();

            if (count($refundFinances) > 2) {
                foreach ($refundFinances as $key => $refundFinance) {
                    $userId = $refundFinance->finance['user_id'];
                    $find = false;
                    unset($refundFinances[$key]);

                    foreach ($refundFinances as $refundFinance2) {
                        if ($refundFinance2->finance['user_id'] == $userId) {
                            $data[$key][] = $refundFinance2;
                            $find = True;
                        }
                    }

                    if ($find) {
                        $data[$key][] = $refundFinance;
                    }
                }
            }

            $startTime += 60;
        } while ($startTime < $endTime);

        return $this->toJsonResponse($data);
    }

    /**
     *
     */
    public function test()
    {
        return $this->testPostAction($this, 'duplicateCreateMatch', [
            'startH' => 2,
            'endH' => 3,
            'day' => 1,

        ]);

    }


    /**
     *
     */
    public function testModels()
    {
        $res = $this->Finances->financeRemover();
        $res = $this->Finances->matchEntranceFee(29, 0);

        $res = $this->Finances->adsIncome(1582852587, 1582872587);
        debug($res);
    }
}



