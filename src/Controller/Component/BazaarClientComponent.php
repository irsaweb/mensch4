<?php

/**
 * Created by PhpStorm.
 * User: Sina
 * Date: 12/25/2017
 * Time: 4:01 PM
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Http\Client;

class BazaarClientComponent extends Component
{

    var $package_name = 'com.NoaGames.Menchico';
    var $client_id = 'B7vQIFeQpIkksZjgtNKxkSeQR9IXhHhT8CH0pp9u';
    var $client_secret = 'wY3OsDA3fzFgjGkM8Kbm6cKupVgzdbpzV1Q8r8cg29AU3eMVuqQk75TjcDHW';
    var $redirect_uris = 'http://noagames.ir';
    var $refresh_token = 'SijOwAB84twlckYkErNkueUTIiOWo4';

    static $access_token;
    static $expires_in;

    function refreshToken()
    {
        if (!isset(self::$expires_in) || time() > self::$expires_in) {
            $params = array(
                'grant_type' => 'refresh_token',
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
                'refresh_token' => $this->refresh_token,
            );
            $responce = $this->sendPost('https://pardakht.cafebazaar.ir/devapi/v2/auth/token/', $params);

            self::$access_token = $responce['access_token'];
            self::$expires_in = ($responce['expires_in'] * 0.8) + time();
        }
    }

    /**
     * @param $product_id
     * @param $purchase_token
     * @return bool
     */
    function validateIAP($product_id, $purchase_token)
    {
        $this->refreshToken();

        $resp = $this->sendGet(sprintf("https://pardakht.cafebazaar.ir/devapi/v2/api/validate/%s/inapp/%s/purchases/%s?access_token=%s",
            $this->package_name,
            $product_id,
            $purchase_token,
            self::$access_token));

        $data = json_decode($resp, true);

        if (array_key_exists('kind', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //This function is not tested

    /**
     * @param $subscription_id
     * @param $purchase_token
     */
    function validateSubscriptions($subscription_id, $purchase_token)
    {
        $this->$this->refresh_token();

        $resp = $this->sendGet(sprintf("https://pardakht.cafebazaar.ir/devapi/v2/api/applications/%s/subscriptions/%s/purchases/%s?access_token=%s",
            $this->package_name,
            $subscription_id,
            $purchase_token,
            self::$access_token));

        $data = json_decode($resp, true);

        if (array_key_exists('kind', $data) && time() < $data['validUntilTimestampMsec']) {
            pr("valid");
        } else {
            pr("invalid");
        }

    }

    /**
     * @param $url
     * @return false|string
     */
    function sendGet($url)
    {
        $context = stream_context_create(array(
            'http' => array('ignore_errors' => true),
        ));
        return file_get_contents($url, false, $context);
    }

    /**
     * @param $url
     * @param $data
     * @return mixed
     */
    function sendPost($url, $data)
    {
        $HttpSocket = new Client();
        $responce = $HttpSocket->post($url, $data);
        return $responce->getJson();
    }
}
