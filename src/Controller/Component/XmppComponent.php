<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Fabiang\Xmpp\Client;
use Fabiang\Xmpp\Options;
use Fabiang\Xmpp\Protocol\Presence;
use Fabiang\Xmpp\Protocol\ProtocolImplementationInterface;
use Fabiang\Xmpp\Util\XML;
use Psr\Log\LoggerInterface;

/**
 * Class XmppComponent
 * @package App\Controller\Component
 */
class XmppComponent extends Component
{
    /**
     * @var string
     */
    private $user = 'admin';
    /**
     * @var string
     */
    private $pass = 'hamid@speed123';
    /**
     * @var string
     */
    public $server = 'mensch';
    /**
     * @var string
     */
    private $address = 'tcp://185.112.33.109:5222';
    /**
     * @var string
     */
    public static $prefix = 'fa';


    /**
     * @param $username
     * @param $password
     * @param $callback
     */
    public function registerUser($username, $password, $callback)
    {
        //todo: age niaz hast hazf shavad
        $ip = $this->getController()->getRequest()->clientIp();
        if (in_array($ip, ['127.0.0.1', '::1'])) {
          return true;
        }


        $options = new Options($this->address);
        $options->setUsername($this->user)
            ->setPassword($this->pas)
            ->setTo($this->server);

        $options->setContextOptions(['ssl' => ['verify_peer' => false, 'verify_peer_name' => false]]);


        $username = $username . "@" . $this->server;
        $client = new Client($options);
        $client->connect();

        $mes = new Register($username, $password, $this->server);
        $client->getOptions()->getImplementation()->registerListener(new ServerManager(array('OnRegister' => $callback)));
        $client->send($mes);
    }


    /**
     * @param $clanName
     * @param $callback
     */
    public function createClan($clanName, $callback)
    {
        $options = new Options($this->address);
        $options->setUsername($this->user)
            ->setPassword($this->pass)
            ->setTo($this->server);
        $options->setContextOptions(['ssl' => ['verify_peer' => false, 'verify_peer_name' => false]]);

//        $options->setLogger(new Logger());
        $clanFull = $clanName . '@conference.' . $this->server;
        $client = new Client($options);
        $client->connect();

        $channel = new Presence;
        $channel->setTo($clanFull)
            ->setNickname($this->user);

        $client->send($channel); // join to channel.

        //$client->disconnect();
        /*$client = new Client($options);
        $client->connect();

        $mes = new ClanConfig($clanFull,true);
        $client->getOptions()->getImplementation()->registerListener(new ServerManager(array(
            'OnConfigChange'=>$callback
        )));
        $client->send($mes);
        $client->disconnect();*/
    }


    /**
     * @param $clanName
     * @param $user
     * @param $callback
     */
    public function removeUserFromClan($clanName, $user, $callback)
    {
        $options = new Options($this->address);
        $options->setUsername($this->user)
            ->setPassword($this->pass)
            ->setTo($this->server);
        $options->setContextOptions(['ssl' => ['verify_peer' => false, 'verify_peer_name' => false]]);

        //$options->setLogger(new Logger());
        $clanFull = $clanName . '@conference.' . $this->server;
        $client = new Client($options);
        $client->connect();

        $channel = new Presence;
        $channel->setTo($clanFull)
            ->setNickname($this->user);

        $client->send($channel); // join to channel.

        $mes = new ClanAdmin($clanFull);
        $mes->setUser($user);

        $client->getOptions()->getImplementation()->registerListener(new ServerManager(array(
            'OnAdminChange' => $callback
        )));
        $client->send($mes);
    }


    /**
     * @param $clanName
     * @param $callback
     */
    public function removeClan($clanName, $callback)
    {
        $options = new Options($this->address);
        $options->setUsername($this->user)
            ->setPassword($this->pass)
            ->setTo($this->server);
        $options->setContextOptions(['ssl' => ['verify_peer' => false, 'verify_peer_name' => false]]);

//        $options->setLogger(new Logger());
        $clanFull = $clanName . '@conference.' . $this->server;
        $client = new Client($options);
        $client->connect();

        $channel = new Presence;
        $channel->setTo($clanFull)
            ->setNickname($this->user);

        $client->send($channel); // join to channel.

        $config = new ClanConfig($clanFull, false);
        $config->setPersistent(false);

        $client->getOptions()->getImplementation()->registerListener(new ServerManager(array(
            'OnConfigChange' => $callback
        )));
        $client->send($config);
    }


}

/**
 * Class Logger
 * @package App\Controller\Component
 */
class Logger implements LoggerInterface
{

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function emergency($message, array $context = array())
    {
        echo "emergency " . $message . "\n";
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function alert($message, array $context = array())
    {
        echo "alert " . $message . "\n";
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function critical($message, array $context = array())
    {
        echo "critical " . $message . "\n";
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function error($message, array $context = array())
    {
        echo "error " . $message . "\n";
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function warning($message, array $context = array())
    {
        echo "warning " . $message . "\n";
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function notice($message, array $context = array())
    {
        echo "notice " . $message . "\n";
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function info($message, array $context = array())
    {
        echo "info " . $message . "\n";
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function debug($message, array $context = array())
    {
        echo "debug " . $message . "\n";
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return null
     */
    public function log($level, $message, array $context = array())
    {
        echo "log " . $message . "\n";
    }
}

/**
 * Class Register
 * @package App\Controller\Component
 */
class Register implements ProtocolImplementationInterface
{
    /**
     * @var string
     */
    protected $username;
    /**
     * @var string
     */
    protected $password;
    /**
     * @var
     */
    protected $host;

    /**
     * Constructor.
     *
     * @param string $username
     * @param string $password
     */
    public function __construct($username, $password, $host)
    {
        $this->username = $username;
        $this->password = $password;
        $this->host = $host;
    }

    /**
     * Build XML message
     * @return type
     */
    public function toString()
    {
        $query = <<<EOT
            <iq type='set' id='%s' to='%s'>
            <command xmlns='http://jabber.org/protocol/commands' node='http://jabber.org/protocol/admin#add-user' action='execute'>
            <x xmlns='jabber:x:data' type='submit'>
            <field var='FORM_TYPE'><value>http://jabber.org/protocol/admin</value></field>
            <field var='accountjid'><value>%s</value></field>
            <field var='password'><value>%s</value></field>
            <field var='password-verify'><value>%s</value></field>
            </x></command></iq>
EOT;

        return XML::quoteMessage($query, XML::generateId(), $this->host, (string)$this->username, (string)$this->password, (string)$this->password);
    }
}

/**
 * Class ClanConfig
 * @package App\Controller\Component
 */
class ClanConfig implements ProtocolImplementationInterface
{
    /**
     * @var string
     */
    protected $clanName;
    /**
     * @var
     */
    protected $persistent;
    /**
     * @var bool
     */
    protected $default;

    /**
     * Constructor.
     *
     * @param string $clanName
     * @param bool $default
     */
    public function __construct($clanName, $default)
    {
        $this->clanName = $clanName;
        $this->default = $default;
    }

    /**
     * @param $val
     * @return $this
     */
    public function setPersistent($val)
    {
        $this->persistent = $val;
        return $this;
    }

    /**
     * Build XML message
     * @return type
     */
    public function toString()
    {
        // id, to, persistant
        $query = <<<EOT
<iq type='set' id='%s' to='%s'>
	<query xmlns='http://jabber.org/protocol/muc#owner'>
		<x xmlns='jabber:x:data' type='submit'>
			<field var='FORM_TYPE'>
				<value>http://jabber.org/protocol/muc#roomconfig</value>
			</field>
			<field var='muc#roomconfig_roomdesc'>
				<value/>
			</field>
			<field var='muc#roomconfig_persistentroom'>
				<value>%s</value>
			</field>
			<field var='muc#roomconfig_publicroom'>
				<value>1</value>
			</field>
			<field var='public_list'>
				<value>1</value>
			</field>
			<field var='muc#roomconfig_passwordprotectedroom'>
				<value>0</value>
			</field>
			<field var='muc#roomconfig_roomsecret'>
				<value/>
			</field>
			<field var='muc#roomconfig_maxusers'>
				<value>50</value>
			</field>
			<field var='muc#roomconfig_whois'>
				<value>moderators</value>
			</field>
			<field var='muc#roomconfig_presencebroadcast'>
				<value>moderator</value>
				<value>participant</value>
				<value>visitor</value>
			</field>
			<field var='muc#roomconfig_membersonly'>
				<value>0</value>
			</field>
			<field var='muc#roomconfig_moderatedroom'>
				<value>1</value>
			</field>
			<field var='members_by_default'>
				<value>1</value>
			</field>
			<field var='muc#roomconfig_changesubject'>
				<value>1</value>
			</field>
			<field var='allow_private_messages'>
				<value>1</value>
			</field>
			<field var='allow_private_messages_from_visitors'>
				<value>anyone</value>
			</field>
			<field var='allow_query_users'>
				<value>1</value>
			</field>
			<field var='muc#roomconfig_allowinvites'>
				<value>0</value>
			</field>
			<field var='muc#roomconfig_allowvisitorstatus'>
				<value>1</value>
			</field>
			<field var='muc#roomconfig_allowvisitornickchange'>
				<value>1</value>
			</field>
			<field var='muc#roomconfig_allowvoicerequests'>
				<value>1</value>
			</field>
			<field var='muc#roomconfig_voicerequestmininterval'>
				<value>1800</value>
			</field>
			<field var='muc#roomconfig_captcha_whitelist'/>
		</x>
	</query>
</iq>
EOT;

        $queryDefault = <<<EOT
            <iq type='set' id='%s' to='%s'>
            <query xmlns='http://jabber.org/protocol/muc#owner'>
            <x xmlns='jabber:x:data' type='submit'/>
            </query>
            </iq>
EOT;
        if ($this->default) {
            return XML::quoteMessage($queryDefault, XML::generateId(), $this->clanName);
        } else {
            return XML::quoteMessage($query, XML::generateId(), $this->clanName, $this->persistent ? 1 : 0);
        }
    }
}

/**
 * Class ClanAdmin
 * @package App\Controller\Component
 */
class ClanAdmin implements ProtocolImplementationInterface
{
    /**
     * @var string
     */
    protected $clanName;
    /**
     * @var
     */
    protected $user;

    /**
     * Constructor.
     *
     * @param string $clanName
     */
    public function __construct($clanName)
    {
        $this->clanName = $clanName;
    }

    /**
     * @param $val
     * @return $this
     */
    public function setUser($val)
    {
        $this->user = $val;
        return $this;
    }

    /**
     * Build XML message
     * @return type
     */
    public function toString()
    {
        // id, to, persistant
        $query = <<<EOT
<iq type='set' id='%s' to='%s'>
	<query xmlns='http://jabber.org/protocol/muc#admin'>
		<item nick='%s' role='none'/>
	</query>
</iq>
EOT;

        return XML::quoteMessage($query, XML::generateId(), $this->clanName, $this->user);
    }
}
