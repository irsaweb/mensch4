<?php

/**
 * Copyright 2014 Fabian Grutschus. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the copyright holders.
 *
 * @author    Fabian Grutschus <f.grutschus@lubyte.de>
 * @copyright 2014 Fabian Grutschus. All rights reserved.
 * @license   BSD
 * @link      http://github.com/fabiang/xmpp
 */namespace App\Controller\Component;



use Fabiang\Xmpp\EventListener\BlockingEventListenerInterface;
use Fabiang\Xmpp\Event\XMLEvent;
use Fabiang\Xmpp\EventListener\Stream\AbstractSessionEvent;

/**
 * Listener
 *
 * @package Xmpp\EventListener
 */
class ServerManager extends AbstractSessionEvent implements BlockingEventListenerInterface
{
    private $callback;
    private $lock;
    /**
     * ServerManager constructor.
     */
    public function __construct($callback)
    {
        $this->callback = $callback;
        $lock=0;
    }
    /**
     * {@inheritDoc}
     */
    public function isBlocking()
    {
        return $this->lock>0;
    }

    /**
     * {@inheritDoc}
     */
    public function attachEvents()
    {
        $output = $this->getOutputEventManager();
        $input = $this->getInputEventManager();

        $output->attach('{http://jabber.org/protocol/commands}command', array($this, 'RegisterRequest'));
        $input->attach('{http://jabber.org/protocol/commands}command', array($this, 'RegisterResponse'));

        $output->attach('{http://jabber.org/protocol/muc#owner}query', array($this, 'ClanConfigRequest'));
        $input->attach('{http://jabber.org/protocol/muc#owner}query', array($this, 'ClanConfigResponse'));

        $output->attach('{http://jabber.org/protocol/muc#admin}query', array($this, 'ClanAdminRequest'));
        $input->attach('{http://jabber.org/protocol/muc#admin}query', array($this, 'ClanAdminResponse'));
    }
    public function RegisterRequest(XMLEvent $event)
    {
        if($event->isStartTag()) {
            $this->lock++;
        }
    }
    public function RegisterResponse(XMLEvent $event)
    {
        if($event->isEndTag()) {
            if($event->getParameters()[0]->getAttribute('status')=="completed"){
                call_user_func($this->callback['OnRegister'], true);
            }else{
                call_user_func($this->callback['OnRegister'], false);
            }
            $this->lock--;
        }
    }

    public function ClanConfigRequest(XMLEvent $event)
    {
        if($event->isStartTag()) {
            $this->lock++;
        }
    }
    public function ClanConfigResponse(XMLEvent $event)
    {
        if($event->isEndTag()) {
            call_user_func($this->callback['OnConfigChange'], true);

            $this->lock--;
        }
    }

    public function ClanAdminRequest(XMLEvent $event)
    {
        if($event->isStartTag()) {
            $this->lock++;
        }
    }
    public function ClanAdminResponse(XMLEvent $event)
    {
        if($event->isEndTag()) {
            call_user_func($this->callback['OnAdminChange'], true);

            $this->lock--;
        }
    }
}
