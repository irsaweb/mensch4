<?php

/**
 * Created by PhpStorm.
 * User: Sina
 * Date: 2/6/2018
 * Time: 1:19 PM
 */namespace App\Controller\Component;

use Cake\Controller\Component;

use App\Network\Http\HttpSocket;
class GoogleClientComponent extends Component {

    var $package_name  = 'com.NoaGames.ProFC';
    var $client_id     = '962458765025-a87euoq34o1r4cvud0cimu48nkpg5v95.apps.googleusercontent.com';
    var $client_secret = 'hLxfHl11sLfH3H1BHk3ST3vC';
    var $redirect_uris = 'http://www.noagames.co';
    var $refresh_token = '4%2F0b60-1SIZmo6d4h3ZUzwdr7R8WfJJ-L-ENDH89E3VJg#';

    static $access_token;
    static $expires_in;

    function refreshToken()
    {
        if(!isset(self::$expires_in) || time() > self::$expires_in) {
            $params = array(
                'grant_type' => 'authorization_code',
                'code' => $this->refresh_token,
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
                'redirect_uri' => $this-> redirect_uris
            );
            $responce = $this->sendPost('https://accounts.google.com/o/oauth2/token', $params);
            $data = json_decode($responce,true);
            self::$access_token = $data['access_token'];
            self::$expires_in = ($data['expires_in'] * 0.8) + time();
        }
    }

    function validateIAP($product_id, $purchase_token)
    {
        $this->refreshToken();

        $resp = $this->sendGet(sprintf("https://www.googleapis.com/androidpublisher/v2/applications/%s/purchases/products/%s/tokens/%s?access_token=%s",
            $this->package_name,
            $product_id,
            $purchase_token,
            self::$access_token));

        $data = json_decode($resp, true);

        if(array_key_exists('kind', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    //This function is not tested
    function validateSubscriptions($subscription_id,$purchase_token)
    {
        $this->$this->refresh_token();

        $resp = $this->sendGet(sprintf("https://www.googleapis.com/androidpublisher/v2/applications/%s/purchases/subscriptions/%s/tokens/%s?access_token=%s",
            $this->package_name,
            $subscription_id,
            $purchase_token,
            self::$access_token));

        $data = json_decode($resp, true);

        if(array_key_exists('kind', $data) && time() < $data['expiryTimeMillis'])
        {
            pr("valid");
        }
        else
        {
            pr("invalid");
        }

    }

    function sendGet($url)
    {
        $context = stream_context_create(array(
            'http' => array('ignore_errors' => true),
        ));
        return file_get_contents($url, false, $context);
    }

    function sendPost($url, $data)
    {
        $HttpSocket = new Client();
        $responce = $HttpSocket->post($url, $data);
        return $responce->body();
    }
}