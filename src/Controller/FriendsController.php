<?php

namespace App\Controller;


/**
 * @property \App\Model\Table\FriendsTable $Friends
 */
class FriendsController extends AppController
{


    /*
    * state 0 means waiting for accepted
    * state 1 means accepted
    * @version 1.0.0
    */
    public function acceptFriendRequest()
    {
        $maxfriends = 200;
        $userId = $this->userInfo['userId'];
        $requestId = $this->request->getData("requestId");

        $requestInfo = $this->Friends->find()->where(['id' => $requestId])->first();

        if ($requestInfo === null) {
            return $this->toJsonResponse(null, 'fail', 'request is not found');
        }

        if ($requestInfo['requester_user_id'] == $userId) {
            return $this->toJsonResponse(null, 'fail', 'user cannot approve this request');
        }

        if ($requestInfo['state'] === 1) {
            return $this->toJsonResponse(null, 'fail', 'request has already been accepted');
        }



        $howmanyfriend = $this->Friends->getFriendCounts($userId);

        if ($howmanyfriend >= $maxfriends) {
            return $this->toJsonResponse(null, 'fail', 'more than max friends');
        }

        $requestInfo->date = time();
        $requestInfo->state = 1;
        $this->Friends->saveOrFail($requestInfo);
        return $this->toJsonResponse();
    }

    /**
     * @version 1.0.0
     */
    public function getFriends()
    {
        $userId = $this->userInfo['userId'];
        $length = $this->request->getData("length");
        $start = $this->request->getData("start");
        $friendsList = $this->Friends->getFriendList($userId, $start, $length);
        return $this->toJsonResponse($friendsList);
    }

    /**
     * send a friend request
     * state 0 means waiting for accepted (request pending)
     * @version 1.0.0
     */
    public function sendFriendRequest()
    {
        $secondUserId = $this->request->getData("friendUserId");
        $requesterUserId = $this->userInfo['userId'];

        $teamname = $this->Friends->Secondusers->userInfo($requesterUserId)['name'];

        if (in_array($secondUserId, array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'))) { //means not bot
            return $this->toJsonResponse(null, 'fail', 'can not request to bot');
        }
        $isFriendTogether = $this->Friends->find()->where([
            'OR' => [
                ['second_user_id' => $requesterUserId, 'requester_user_id' => $secondUserId],
                ['second_user_id' => $secondUserId, 'requester_user_id' => $requesterUserId]
            ]
        ])->first();


        if ($isFriendTogether or ($secondUserId === $requesterUserId)) {
            return $this->toJsonResponse(null, 'fail', 'They are friends, or request sent before');
        }
        $entity = $this->Friends->newEntity([
            'requester_user_id' => $requesterUserId,
            'second_user_id' => $secondUserId,
            'date' => time(),
            'state' => 0
        ]);
        $this->Friends->saveOrFail($entity);

        $customData = ['tipsy' => 'friendRequest'];
        //TODO use new system -> $this->PushNotification->sendPushNotification($secondUserId, $teamname, 'friend request', $customData);
        return $this->toJsonResponse();
    }

    /**
     * @version 1.0.0
     */
    public function deleteFriend()
    {
        $userId = $this->userInfo['userId'];
        $friendUserId = $this->request->getData("friendUserId");

        $isFriendTogether = $this->Friends->find()->where([
            "OR" => [
                ['AND' => [
                    'second_user_id' => $userId, 'requester_user_id' => $friendUserId]],
                ['AND' => ['second_user_id' => $friendUserId, 'requester_user_id' => $userId]]
            ],
            //     'Friend.state'=>1
        ])->first();

        if ($isFriendTogether === null) {
            return $this->toJsonResponse(null, 'fail', 'They are not friends');
        }

        $this->Friends->deleteOrFail($isFriendTogether);
        return $this->toJsonResponse();
    }


    public function test()
    {

        return $this->testPostAction($this, 'sendFriendRequest', [
            'friendUserId' => 23,
        ]);

    }


}
