<?php

namespace App\Controller;


use App\Config\AppSettings;
use App\Controller\Component\XmppComponent;
use Cake\ORM\TableRegistry;

/**
 * @property \App\Model\Table\ClansTable $Clans
 * @property \App\Model\Table\ShopitemsTable $Shopitems
 */
class ClansController extends AppController
{
    /**
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

    }


    /**
     * @version 1.0.0
     */
    public function createClan()
    {

        $userId = $this->userInfo['userId'];
        $name = $this->request->getData('name');
        $badge = $this->request->getData('badge');
        $require_trophy = (int)$this->request->getData('requireTrophy');
        $description = $this->request->getData("description");
        $description = $description ?? NULL;

        //check name lenght
        if (strlen($name) <= 3) {
            return $this->toJsonResponse('fail', null, 'Clan name too short');
        }

        //check user coin to can create clan
        $userInfo = $this->Clans->Userclans->Users->userInfo($userId);
        $userCoin = $userInfo['coin'];
        $userCreatorTrophy = $userInfo['totaltrophy'];
        if ($userCoin <= AppSettings::CreateClanFee) {
            $data = ['resource' => 'coin', 'amount' => AppSettings::CreateClanFee - $userCoin];
            return $this->toJsonResponse($data, 'not enough resource', 'not enough coin');
        }


        $newClan = $this->Clans->createClan($name, $badge, $require_trophy, $description, $userCreatorTrophy, $userId);

        if ($newClan === false) {
            return $this->toJsonResponse('fail', null, 'taraf clan darad!!!');

        }
        $clanInfo = $this->Clans->Userclans->getClanByUser($userId);

        $prefix = XmppComponent::$prefix;
        $clanId = $clanInfo['id'];

        $password = $clanInfo['pass'];
        $clanInfo['username'] = $prefix . $userId;
        $clanInfo['server'] = "mensch";
        $clanInfo['host'] = "185.112.33.109";
        $clanInfo['port'] = 5222;
        $clanInfo['room'] = $prefix . $clanId;
        $clanInfo['ClanMembers'] = $this->Clans->Userclans->getUserClan($clanId);
        $userData = array(
            'user_id' => $userId,
            'coin' => -AppSettings::CreateClanFee,
            'service' => 'create_clan'
        );

        $this->Clans->Userclans->Users->updateUserInfo($userData);

        $this->Xmpp->register_user($prefix . $userId, $password,
            function ($result) use ($clanId, $userId, $prefix) {
                if ($result) {
                    $this->Xmpp->create_clan($prefix . $clanId, function ($result2) use ($clanId, $userId) {
                        if ($result2) {
                        } else {
                        }
                    });
                } else {
                }
            }
        );

        return $this->toJsonResponse($clanInfo);
    }

    /**
     * @version 1.0.0
     */
    public function clanInfo()
    {
        $clanId = (int)$this->request->getData('clanId');
        $clanInfo = $this->Clans->getClanInfo($clanId);

        if(!$clanInfo) {
           return $this->toJsonResponse(null, 'fail','clan not found');
        }

        return $this->toJsonResponse($clanInfo);
    }


    /**
     * @version 1.0.0
     */
    public function editClan()
    {
        $userIdSuperadmin = $this->userInfo['userId'];
        $clanId = $this->request->getData('clanId');
        $name = $this->request->getData('name');
        $badge = $this->request->getData('badge');
        $require_trophy = (int)$this->request->getData("requireTrophy");
        $description = $this->request->getData("description");

        if (strlen($name) <= 3) {
            return $this->toJsonResponse(null, 'fail', 'Clan name too short');
        }

        $res = $this->Clans->editClan($clanId, $name, $badge, $require_trophy, $description, $userIdSuperadmin);
        if ($res) {
            return $this->toJsonResponse();
        }
        return $this->toJsonResponse(null, 'fail', 'Erroor');
    }

    /**
     * @version 1.0.0
     */
    public function promoteClan()
    {
        $clanId = $this->request->getData('clanId');
        $userId = $this->userInfo['userId'];
        $hours = 24;
        $time = time() + $hours * 3600;

        $userInfo = $this->Clans->Userclans->Users->userInfo($userId);
        $vipTime = $userInfo['vip'];
        $userCoin = $userInfo['coin'];
        $promoteClan = $userInfo['promote_clan'];

        $Shopitems = TableRegistry::getTableLocator()->get('Shopitems');
        $clanPromote = $Shopitems->getItem('promote_clan', 'inapp');
        $clanPromotePrice = $clanPromote['priceafterdiscount'];

        $responseAdditions = $this->addUserinfo([]);

        if ($userCoin >= $clanPromotePrice || ($vipTime > $time && $promoteClan != 1)) {
            if ($vipTime > $time) $clanPromotePrice = 0;
            $this->Clans->promoteClan($clanId, $userId, $time, $clanPromotePrice);
            $additions = $this->addUserinfo([]);
            return $this->toJsonResponse(null, 'success', 'Done', $responseAdditions);
        }

        if ($userCoin < $clanPromotePrice) {
            $data = ['resource' => 'coin', 'amount' => $clanPromotePrice - $userCoin];
            return $this->toJsonResponse($data, 'not enough resource', 'not enough coin', $responseAdditions);
        }

        return $this->toJsonResponse(null, 'fail', 'Vip promote used before', $responseAdditions);
    }

    /**
     * @version 1.0.0
     */
    public function clanRanking()
    {
        $userId = $this->userInfo['userId'];
        $start = 0;
        $length = 100;

        $myClan = $this->Clans->Userclans->getClanByUser($userId);
        $clanRank = $this->Clans->getClanRanking($myClan, $start, $length);
        return $this->toJsonResponse($clanRank);
    }

    /**
     * @version 1.0.0
     */
    public function searchClan()
    {
        $name = $this->request->getData("name");

        if (filter_var($name, FILTER_VALIDATE_INT)) {
            $condition = ['id' => (int)$name];
        } else {
            $condition = ['name LIKE' => "%$name%"];
        }

        $clanInfos = $this->Clans->find()->where($condition)->limit(25)->all();
        return $this->toJsonResponse($clanInfos);
    }


    /**
     * @version 1.0.0
     */
    public function joinableClan()
    {
        $requireTrophy = $this->request->getData("requireTrophy");
        $time = time();
        $clanInfoPromoted = $this->Clans->find()
            ->where(['promote >' => $time])
            ->order(['promote' => 'DESC'])
            ->all()->toArray();

        $clanInfos = $this->Clans->find()
            ->where(['require_trophy <=' => $requireTrophy, 'member <' => 50, 'promote <' => $time])
            ->order(['trophy' => 'DESC'])
            ->limit(25)
            ->disableHydration()
            ->all()->toArray();

        if ($clanInfoPromoted) {
            $clanInfos = array_merge($clanInfoPromoted, $clanInfos);
        }
        return $this->toJsonResponse($clanInfos);
    }

    /**
     * @return \Cake\Http\Response|mixed|null
     */
    public function test()
    {
       /*
        return $this->testPostAction($this, 'clanInfo', [
            'clanId' => '23'
        ]);*/


        $this->Clans->deleteUserClan($this->userInfo['userId']);
        return $this->testPostAction($this, 'createClan', [
            'name' => 'clan-saleh',
            'badge' => 2,
            'requireTrophy' => 100,
            'description' => 'salaaaam clan'
        ]);



//        $p = [13 , 179];
//        $res = $this->Clans->editClan($p[1], 'OoOoOo', 44, 12, 'GoooooR', $p[0]);
//        debug([$p,$res]);
//        $p = [14 , 42];
//        $res = $this->Clans->editClan($p[1], 'OoOoOo', 44, 12, 'GoooooR', $p[0]);
//        debug([$p,$res]);


        // $res = $this->Clans->updateTotalThrophy(47);

        // start test:$this->Clans->promoteClan()
//        $Shopitems = $this->loadModel('Shopitems');
//        $clanPromote = $Shopitems->getItem('promote_clan', 'inapp');
//        $clanPromotePrice = $clanPromote['priceafterdiscount'];
//        $res = $this->Clans->promoteClan('42', '14', 1594626083, $clanPromotePrice);
        // end test:$this->Clans->promoteClan()

        // debug($res);
    }


}
