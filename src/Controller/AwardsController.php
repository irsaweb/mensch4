<?php

namespace App\Controller;

use App\Model\Table\AwardsTable;
use Cake\ORM\Exception\PersistenceFailedException;

/**
 * @property \App\Model\Table\AwardsTable $Awards
 */
class AwardsController extends AppController
{


    /**
     * @version 1.0.0
     */
    public function winAward()
    {
        $userId = $this->userInfo['userId'];
        $type = $this->request->getData('type');
        $awardInfo = $this->Awards->winners($userId, $type);
        return $this->toJsonResponse($awardInfo);
    }

    /**
     * @version 1.0.0
     */
    public function getAward()
    {
        $userId = $this->userInfo['userId'];
        $type = $this->request->getData('type');
        $award = $this->Awards->winners($userId, $type);

        if ($award === null) {
            return $this->toJsonResponse(null, 'fail', 'No award won');
        }

        $userData = [
            'user_id' => $userId,
            'coin' => $award->coin,
            'service' => 'award'
        ];

        try {
            $return = $this->Awards->getConnection()->transactional(function () use ($userData, $award) {
                $award->done = 1;
                $this->Awards->saveOrFail($award);
                $this->Awards->Users->updateUserInfo($userData);
            });
        } catch (PersistenceFailedException $e) {
            return $this->toJsonResponse(null, 'fail', "rollbacked: " . $e->getEntity());
        }
        return $this->toJsonResponse(null, 'success', "Done", $this->addUserinfo());
    }


    /**
     * @version 1.0.0
     */
    public function pastWinners()
    {
        $type = $this->request->getData('type');
        $pastWinners = $this->Awards->find()
            ->where(['type' => $type])
            ->contain([
                'Users' => [
                    'fields' => ['id', 'name', 'ring', 'symbol', 'colour6', 'totaltrophy', 'vip']
                ]
            ])
            ->order(['Awards.id' => 'DESC'])->limit(30)->all()->toArray();

        $i = 0;
        $resultWinners = [];
        foreach ($pastWinners as $key => $winner) {

            $type = $winner['type'];
            if ($key == 0) {
                $resultWinners[$type][$i] = $winner;
                continue;
            }

            if ($pastWinners[$key]['time'] == $pastWinners[$key - 1]['time']) {
                $resultWinners[$type][$i] = $winner;
            } else {
                $i++;
                $resultWinners[$type][$i] = $winner;
            }
        }

        $remainTime = strtotime("next saturday");
        $data = [
            'awards' => [
                '1' => 5000,
                '2' => 2500,
                '3' => 1000,
            ],
            'pastWinners' => $resultWinners,
            'remainTime' => $remainTime
        ];
        return $this->toJsonResponse($data);
    }


    public function test()
    {
        //$res = $this->Awards->setWinners('weeklytrophy');
        //$res = $this->Awards->winners(12,'weekly')
        $this->request = $this->request->withParsedBody([
            'type' => 'weekly'
        ]);
        $res = $this->pastWinners();
        debug($res);

    }


}
