<?php


namespace App\Controller;

use App\Config\AppSettings;
use Cake\Cache\Cache;


/**
 * @property \App\Model\Table\UsermatchesTable $Usermatches
 */
class UsermatchesController extends AppController
{


    /*
     * state
     * -1 -> friendly match (created)
     * 0  -> started  (lobby)
     * 1  -> finished
     * 2  -> Canceled
     */


    /**
     * @version 1.0.0
     */
    public function userMatch()
    {
        $userId = $this->userInfo['userId'];
        $matchInfo = $this->Usermatches->userMatchInfo($userId);

        if (!$matchInfo) {
            return $this->toJsonResponse(null);
        }

        $serverId = $matchInfo['server_id'];
        $matchState = $matchInfo['state'];
        $isAlive = $this->Usermatches->Matches->Servers->isServerAlive($serverId);

        if ($isAlive && $matchState == 1 && $matchState == 2) {
            //means not alive + not done + not refund}
            return $this->toJsonResponse(null, 'fail', 'rollback:not alive + not done + not refund');
        }

        try {
            $this->Usermatches->getConnection()->transactional(function () use (&$matchInfo) {
                foreach ($matchInfo['usermatches'] as $key => $val) {
                    $matchInfo['usermatches'][$key]['state'] = 2;
                    $matchId = $matchInfo['id'];
                    $tableType = $matchInfo['table_type'];
                    $userId = $matchInfo['usermatches'][$key]['user_id'];

                    $tableEntranceFee = $this->Usermatches->Users->Finances->matchEntranceFee($userId, $matchId);

                    if ($tableEntranceFee == 0) {
                        $tableEntranceFee = AppSettings::TableEntranceCost[$tableType]['coin'];
                    }

                    $userData = array(
                        'user_id' => $userId,
                        'match_id' => $matchId,
                        'coin' => $tableEntranceFee,
                        'service' => 'entrance_fee_refund',
                        'description' => "matchId=$matchId by userMatch"
                    );

                    $this->Usermatches->Users->updateUserInfo($userData);
                    $this->Usermatches->changeStateByUser($userId, 2);  //cancel userMatch
                }

                $this->Usermatches->Matches->changeState($matchId, 2); //cancel  match


                //،TODO table Type az jadval usermatch darbiarim bezarim to match (az ek versioni be bad baiad hazf beshe)

                /*                foreach($matchInfo['Usermatch'] as $key1=>$usermatch) {
                                    if($usermatch['user_id']==$userId){
                                        $userTableType=$matchInfo->usermatch[$key1]['table_type'];
                                        break;
                                    }
                                }
                                if($userTableType!=NULL)
                                     $matchInfo->match['table_type']=$userTableType;
                */
            });

        } catch (Exception $e) {
            return $this->toJsonResponse(null, 'fail', 'rollback');
        }

        return $this->toJsonResponse($matchInfo);
    }


    /**
     * Just called from server
     * @version 1.0.0
     */
    public function userMatchByUserId()
    {
        $userId = $this->request->getData('userId');
        $matchInfo = $this->Usermatches->userMatchInfo($userId);
        if(!$matchInfo){
            return $this->toJsonResponse(null,'fail','this user not exist match');
        }
        return $this->toJsonResponse($matchInfo);
    }


    /**
     * Just called from server
     * @version 1.0.0
     */
    public function updateUserMatchState()
    {
        $matchId = (int)$this->request->getData("matchId");
        $userId = (int)$this->request->getData("userId");
        $state = (int)$this->request->getData("state");  //Started = 0, Finished = 1, Canceled = 2
        $rank = (int)$this->request->getData('rank');
        $table = $this->request->getData('tableType');//'beginner', 'intermadiate', 'professional'
        $playerCount = (int)$this->request->getData('playerCount');  //4 or 6
        $penalty = (int)$this->request->getData('penalty'); //true or false
        $resign = (int)$this->request->getData('resign'); //true or false
        $groupMode = $this->request->getData('groupMode'); //'NoGroup', 'TwoVsTwo', 'TwoVsTwoVsTwo' ,'ThreeVsThree'

        if (isset($groupMode) && array_key_exists($groupMode, AppSettings::AwardInfo)) {
            $award = AppSettings::AwardInfo[$groupMode][$playerCount][$table][$rank];
        } else {
            $award = AppSettings::AwardInfo['NoGroup'][$playerCount][$table][$rank];
        }

        $trophy = $award['trophy'];
        $xp = $award['xp'];
        $coin = $award['coin'];
        $status = 'normal';

        if (isset($penalty) and strtolower($penalty) == "true") {
            $xp = 0;
            $coin = 0;
            $trophy = -15; //penalty trophy
            $status = 'disconnected';
        }

        if (isset($resign) and strtolower($resign) == "true") {
            $xp = 0;
            $coin = 0;
            $trophy = -20; //resign trophy
            $rank = -1;
            $status = 'resign';
        }

        $userInfo = $this->Usermatches->Users->userInfo($userId);
        $dailytrophy = $userInfo['dailytrophy'];
        $weeklytrophy = $userInfo['weeklytrophy'];
        $totaltrophy = $userInfo['totaltrophy'];

        if ($totaltrophy + $trophy < 0) {
            $trophy = -$totaltrophy;
        }

        if ($weeklytrophy + $trophy < 0) {
            $trophy = -$weeklytrophy;
        }

        if ($dailytrophy + $trophy < 0) {
            $trophy = -$dailytrophy;
        }

        $data = [
            'user_id' => $userId,
            'xp' => $xp,
            'match_id' => $matchId,
            'totaltrophy' => $trophy,
            'weeklytrophy' => $trophy,
            'dailytrophy' => $trophy,
            'coin' => $coin,
            'service' => 'win_game'
        ];
        $dataUserMatch = ['state' => $state, 'rank' => $rank, 'status' => $status];

        try {
            $return = $this->Usermatches->getConnection()->transactional(function () use ($data, $userId, $dataUserMatch, $matchId) {
                $this->Usermatches->Users->updateUserInfo($data);
                $clanInfo = $this->Usermatches->Users->Userclans->getClanByUser($userId);

                if ($clanInfo) {
                    //todo: aya user mitone bi clan bashe?!
                    $clanId = $clanInfo['id'];
                    // todo: aghar false bod exeption nemikhad?
                    $this->Usermatches->Users->Userclans->Clans->updateTotalThrophy($clanId);
                }

                if (!$this->Usermatches->updateAll($dataUserMatch, ['user_id' => $userId, 'match_id' => $matchId])) {
                    //throw new \Exception('recordi update nashood!');
                }
            });

            Cache::delete("$userId._.True", 'userInfo');
            return $this->toJsonResponse();
        } catch (Exception $e) {
            return $this->toJsonResponse(null, 'fail', 'rollback');

        }


    }


    public function test()
    {

        return $this->testPostAction($this, 'updateUserMatchState', [
            'matchId' => 434976,
            'userId' => 29,
            'state' => 0,
            'rank' => 1,
            'tableType' => 'beginner',
            'playerCount' => 4,
            'penalty' => true,
            'groupMode' => 'NoGroup',
            'resign' => true
        ]);


        return $this->testPostAction($this, 'userMatch', [
            'userId' => 14
        ]);
        //$res = $this->Usermatches->avgWinRank(1);
        //$res = $this->Usermatches->userMatchInfo(61160);
        debug($res);
    }


}
