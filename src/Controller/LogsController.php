<?php

namespace App\Controller;

use Cake\Event\Event;
use Cake\Event\EventInterface;


/**
 * @property \App\Model\Table\LogsTable $Logs
 */
class LogsController extends AppController
{


    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['submitLog']);
    }


    /**
     * @version 1.0.0
     */
    public function submitLog()
    {
        if (isset($this->userInfo['userId']))
            $userId = $this->userInfo['userId'];

        $tag = $this->request->getData("tag");
        $content = $this->request->getData("content");
        $version = $this->request->getData("version");

        $data = $this->Logs->newEntity([
            'user_id' => $userId,
            'tag' => $tag,
            'content' => $content,
            'version' => $version,
            'time' => time()
        ]);

        try {
            $this->Logs->saveOrFail($data);
        } catch (\Cake\ORM\Exception\PersistenceFailedException $e) {
            echo $e->getEntity();
        }

        return $this->toJsonResponse(null);
    }

}
