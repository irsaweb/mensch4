<?php

namespace App\Controller;

use App\Config\AppSettings;
use App\Model\Table\ServersTable;
use ArrayObject;
use Cake\Event\EventInterface;
use Exception;

/**
 * @property \App\Model\Table\MatchesTable $Matches
 */
class MatchesController extends AppController
{

    /**
     * @param EventInterface $event
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['matchReport']);
    }

    /*
     * state
     * -1 -> friendly match (created)
     * 0  -> started  (lobby)
     * 1  -> finished
     * 2  -> Canceled
     */

    /**
     * Just called from server
     * @version 1.0.0
     */
    public function updateMatchState()
    {
        $matchId = $this->request->getData("matchId");
        $state = $this->request->getData("state");  //0,1,2

        try {
            $this->Matches->getConnection()->transactional(function () use ($matchId, $state) {
                $this->Matches->updateAll(['endtime' => time(), 'state' => $state], ['id' => $matchId]);

                if ($state == 2) {
                    $matchInfo = $this->Matches->matchInfo($matchId);
                    $tableType = $matchInfo['table_type'];
                    $userMatches = $this->Matches->Usermatches->userTakeApartInMatch($matchId);

                    foreach ($userMatches as $user) {
                        $userId = $user['user_id'];
                        $tableEntranceFee = $this->Matches->Usermatches->Users->Finances->matchEntranceFee($userId, $matchId);
                        if ($tableEntranceFee == 0) {   //for old version
                            $tableEntranceFee = AppSettings::TableEntranceCost[$tableType]['coin'];
                        }
                        $userData = [
                            'user_id' => $userId,
                            'match_id' => $matchId,
                            'coin' => $tableEntranceFee,
                            'service' => 'entrance_fee_refund',
                            'description' => "matchId=$matchId by unity server"
                        ];

                        $this->Matches->Usermatches->Users->updateUserInfo($userData);
                        $this->Matches->Usermatches->changeStateByUser($userId, 2);  //means match ended for the user
                    }
                }
            });

            return $this->toJsonResponse();
        } catch (Exception $e) {
            return $this->toJsonResponse(null, 'fail', ' rollback');
        }

    }

    /**
     * * createMatch() has two logic or task
     * 1- start friendly match
     * 2- create match
     *
     * @version 1.0.0
     * @todo Hatman bayad check shavad
     */
    public function createMatch()
    {
        $params = new ArrayObject();
        $params->setFlags(ArrayObject::STD_PROP_LIST | ArrayObject::ARRAY_AS_PROPS);

        $params->matchId = (int)$this->request->getData('matchId');
        $params->type = $this->request->getData('type');  //classic,modern
        $params->player = $this->request->getData('player'); //4,6 (Max player)
        $params->requirePlayer = $this->request->getData('requirePlayer'); //2,3,4,5,6
        $params->tableType = strtolower($this->request->getData('tableType')); //'beginner', 'intermadiate', 'professional'
        $params->networkVersion = $this->request->getData('networkVersion');
        $params->port = $this->request->getData('port');
        $params->groupMode = $this->request->getData('groupMode'); //'NoGroup','TwoVsTwo','TwoVsTwoVsTwo','ThreeVsThree'
        $params->takeApartUserIds = $this->request->getData('takeApartUserIds');
        $params->takeApartUsersNames = $this->request->getData('takeApartUsersNames');
        $params->requestedTableTypes = $this->request->getData('requestedTableTypes');
        $params->friendlyMatchOwnerId = $this->request->getData('friendlyMatchOwnerId');

        $params->takeApartUserIdsArray = explode(',', str_replace(" ", "", $params->takeApartUserIds));
        $params->takeApartUsersNamesArray = explode(',', str_replace(" ", "", $params->takeApartUsersNames));

        if (isset($params->requestedTableTypes))
            $params->requestedTableTypesArray = explode(',', $params->requestedTableTypes);

        if ($params->matchId === 0)
            $params->matchId = false;

        /*
         * start task 1: friendly match
         */
        if ($params->matchId && $params->tableType == 'friendly') {
            $friendlyMatchInfo = $this->Matches->startFriendlyMatch($params->matchId);

            if (!$friendlyMatchInfo) {
                return $this->toJsonResponse(null, 'fail', 'task1: rollback');
            }

            return $this->toJsonResponse(['matchId' => $params->matchId], 'success', 'start the friendly match');
            //todo taklif in code  moshakhas nist
//            $friendlyMatchOwnerId = $friendlyMatchInfo->friendlymatch['owner_id'];
//            $userDatas[] = array(
//                'user_id' => $friendlyMatchOwnerId,
//                'coin' => -$coin,
//                'service' => 'entrance_fee',
//                'description' => "Friendly match created fee $groupMode $tableType"
//            );

        }


        /*
         * start task 2: create match
         */

        $params->userMatches = [];
        $params->userDatas = [];

        foreach ($params->takeApartUserIdsArray as $key => $userId) {
            if (isset($params->requestedTableTypesArray)) {
                $params->userTableType = strtolower($params->requestedTableTypesArray[$key]);
            } else {
                $params->userTableType = strtolower($params->tableType);
            }

            $params->userMatches[] = [
                'user_id' => $userId,
                'name' => $params->takeApartUsersNamesArray[$key],
                'table_type' => $params->userTableType
            ];

            $coin = AppSettings::TableEntranceCost[$params->userTableType]['coin'];

            $params->userDatas[] = [
                'user_id' => $userId,
                'coin' => -$coin,
                'service' => 'entrance_fee',
                'description' => "$params->groupMode  $params->userTableType"
            ];
        }

        try {
            $return = $this->Matches->getConnection()->transactional(function () use ($params) {
                $ip = ServersTable::getServerIP();
                $server = $this->Matches->Servers->serverInfo($ip, $params->port, $params->networkVersion);
                if($server === null){
                    return false;
                }
                $matchId = $this->Matches->createMatch($params->type, $server->id, $params->player, $params->player, $params->tableType, $params->groupMode, $params->userMatches, 0);

                if (!empty($params->userDatas)) {
                    foreach ($params->userDatas as $userData) {
                        $userData['match_id'] = $matchId;

                        $this->Matches->Usermatches->Users->updateUserInfo($userData);
                    }
                }

                return $matchId;
            });
        } catch (Exception $e) {
            throw $e;
        }

        if ($return === false) {
            return $this->toJsonResponse(null, 'fail', 'task2: rollback');
        }
        return $this->toJsonResponse(['matchId' => $return], 'success', 'new match is created');

    }


    /**
     * @version 1.0.0
     */
    public function matchReport()
    {
        ini_set('memory_limit', '8048M');
        set_time_limit(0);

        @$startTime = $this->request->getData("startTime");
        @$endTime = $this->request->getData("endTime");

        @$startDay = $this->request->getData("startDay");
        @$startMonth = $this->request->getData("startMonth");

        @$endDay = $this->request->getData("endDay");
        @$endMonth = $this->request->getData("endMonth");

        if (isset($startDay) and isset($startMonth) and isset($endDay) and isset($endMonth)) {
            $startTime = strtotime(date("Y-$startMonth-$startDay 00:00:00"));
            $endTime = strtotime(date("Y-$endMonth-$endDay 23:59:59"));

        }

        $statistics = array();
        $startTimeDay = $startTime;

        do {
            $endTimeDay = $startTimeDay + 24 * 3600;
            $matchInfos = $this->Matches->find()->where([
                'starttime >=' => $startTimeDay,
                'starttime <=' => $endTimeDay,
                'state' => 1
            ])->contain(['Usermatches'])->toArray();

            if (empty($matchInfos)) {

                return $this->toJsonResponse(null, 'fail', 'No match with these inputs');
                break;
            }

            foreach ($matchInfos as $key => $matchInfo) {
                $matchCapacity = $matchInfo['player']; //4 ,6
                //$matchInfo->match['player'];
                $statistics[$key][$matchCapacity]['NoGroup']['startTimeDay'] = $startTimeDay;
                $statistics[$key][$matchCapacity]['NoGroup']['endTimeDay'] = $endTimeDay;
                $statistics[$key][$matchCapacity]['NoGroup']['total'] = 0;
                $statistics[$key][$matchCapacity]['NoGroup'][1] = 0;
                $statistics[$key][$matchCapacity]['NoGroup'][2] = 0;
                $statistics[$key][$matchCapacity]['NoGroup'][3] = 0;
                $statistics[$key][$matchCapacity]['NoGroup'][4] = 0;

                $statistics[$key][$matchCapacity]['TwoVsTwo']['startTimeDay'] = $startTimeDay;
                $statistics[$key][$matchCapacity]['TwoVsTwo']['endTimeDay'] = $endTimeDay;
                $statistics[$key][$matchCapacity]['TwoVsTwo']['total'] = 0;
                $statistics[$key][$matchCapacity]['TwoVsTwo'][1] = 0;
                $statistics[$key][$matchCapacity]['TwoVsTwo'][2] = 0;
                $statistics[$key][$matchCapacity]['TwoVsTwo'][3] = 0;
                $statistics[$key][$matchCapacity]['TwoVsTwo'][4] = 0;

                $groupMode = $matchInfo['group_mode'];
                $howManyUserTakeApart = count($matchInfo['usermatches']);

                if ($groupMode == 'NoGroup') {
                    $statistics[$key][$matchCapacity][$groupMode][$howManyUserTakeApart]++;
                    $statistics[$key][$matchCapacity][$groupMode]['total']++;
                } elseif ($groupMode == 'TwoVsTwo') {
                    $statistics[$key][$matchCapacity][$groupMode][$howManyUserTakeApart]++;
                    $statistics[$key][$matchCapacity][$groupMode]['total']++;
                } else {
                    $statistics[$key][$matchCapacity][$groupMode][$howManyUserTakeApart]++;
                    $statistics[$key][$matchCapacity][$groupMode]['total']++;
                }
            }

            if (isset($statistics[$key][4]['NoGroup']['total']) && $statistics[$key][4]['NoGroup']['total'] > 0) {
                $statistics[$key][4]['NoGroup'][1] = round($statistics[$key][4]['NoGroup'][1] / $statistics[$key][4]['NoGroup']['total'] * 100, 2);
                $statistics[$key][4]['NoGroup'][2] = round($statistics[$key][4]['NoGroup'][2] / $statistics[$key][4]['NoGroup']['total'] * 100, 2);
                $statistics[$key][4]['NoGroup'][3] = round($statistics[$key][4]['NoGroup'][3] / $statistics[$key][4]['NoGroup']['total'] * 100, 2);
                $statistics[$key][4]['NoGroup'][4] = round($statistics[$key][4]['NoGroup'][4] / $statistics[$key][4]['NoGroup']['total'] * 100, 2);
            }

            if (isset($statistics[$key][6]['NoGroup']['total']) && $statistics[$key][6]['NoGroup']['total'] > 0) {
                $statistics[$key][6]['NoGroup'][1] = round($statistics[$key][6]['NoGroup'][1] / $statistics[$key][6]['NoGroup']['total'] * 100, 2);
                $statistics[$key][6]['NoGroup'][2] = round($statistics[$key][6]['NoGroup'][2] / $statistics[$key][6]['NoGroup']['total'] * 100, 2);
                $statistics[$key][6]['NoGroup'][3] = round($statistics[$key][6]['NoGroup'][3] / $statistics[$key][6]['NoGroup']['total'] * 100, 2);
                $statistics[$key][6]['NoGroup'][4] = round($statistics[$key][6]['NoGroup'][4] / $statistics[$key][6]['NoGroup']['total'] * 100, 2);
            }

            if (isset($statistics[$key][4]['TwoVsTwo']['total']) && $statistics[$key][4]['TwoVsTwo']['total'] > 0) {
                $statistics[$key][4]['TwoVsTwo'][1] = round($statistics[$key][4]['TwoVsTwo'][1] / $statistics[$key][4]['TwoVsTwo']['total'] * 100, 2);
                $statistics[$key][4]['TwoVsTwo'][2] = round($statistics[$key][4]['TwoVsTwo'][2] / $statistics[$key][4]['TwoVsTwo']['total'] * 100, 2);
                $statistics[$key][4]['TwoVsTwo'][3] = round($statistics[$key][4]['TwoVsTwo'][3] / $statistics[$key][4]['TwoVsTwo']['total'] * 100, 2);
                $statistics[$key][4]['TwoVsTwo'][4] = round($statistics[$key][4]['TwoVsTwo'][4] / $statistics[$key][4]['TwoVsTwo']['total'] * 100, 2);
            }

            if (isset($statistics[$key][6]['TwoVsTwo']['total']) && $statistics[$key][6]['TwoVsTwo']['total'] > 0) {
                $statistics[$key][6]['TwoVsTwo'][1] = round($statistics[$key][6]['TwoVsTwo'][1] / $statistics[$key][6]['TwoVsTwo']['total'] * 100, 2);
                $statistics[$key][6]['TwoVsTwo'][2] = round($statistics[$key][6]['TwoVsTwo'][2] / $statistics[$key][6]['TwoVsTwo']['total'] * 100, 2);
                $statistics[$key][6]['TwoVsTwo'][3] = round($statistics[$key][6]['TwoVsTwo'][3] / $statistics[$key][6]['TwoVsTwo']['total'] * 100, 2);
                $statistics[$key][6]['TwoVsTwo'][4] = round($statistics[$key][6]['TwoVsTwo'][4] / $statistics[$key][6]['TwoVsTwo']['total'] * 100, 2);
            }

            $startTimeDay = $startTime + 24 * 3600;

        } while ($endTimeDay < $endTime);

        if (count($statistics) > 0) {
            return $this->toJsonResponse($statistics);
        }
    }


    /**
     * @return \Cake\Http\Response|mixed|null
     */
    public function test()
    {


//        return $this->_testTask1();
        // $res = $this->_testTask2();


        return $this->testPostAction($this, 'matchReport', [
//            'startTime' => 1582852587,
//            'endTime' => 1582872587,
            'startDay' => 17,
            'startMonth' => 02,
            'endDay' => 17,
            'p' => 02,
        ], false);

        //  $res =  $this->Matches->changeState(323, 0);
    }

    protected function _testTask1()
    {

        return $this->testPostAction($this, 'createMatch', [
            'type' => 'classic',
            'player' => 4,
            'takeApartUserIds' => "5, 6, 7, 104",
            'takeApartUsersNames' => "hamid, sina, hassan, tina",
            'networkVersion' => 13,
            'port' => 7023,
            'groupMode' => 'NoGroup',
            //  'matchId' => 500000,
            'matchId' => 437531,
            'tableType' => 'friendly'
        ], false);

    }

    protected function _testTask2()
    {
        // test task 2;
        $this->request = $this->request->withParsedBody([
            'type' => 'classic',
            'player' => 4,
            'takeApartUserIds' => "5, 6, 7, 104",
            'takeApartUsersNames' => "hamid, sina, hassan, tina",
            'networkVersion' => 13,
            'port' => 7023,
            'groupMode' => 'NoGroup',
            'tableType' => 'beginner',
//            'matchId' =>14,
//            'tableType' => 'friendly'
        ]);

        return $this->createMatch();
    }
}
