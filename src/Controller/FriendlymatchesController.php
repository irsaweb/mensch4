<?php


namespace App\Controller;


use Cake\Event\EventInterface;

/**
 * @property \App\Model\Table\FriendlymatchesTable $Friendlymatches
 */
class FriendlymatchesController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['test']);
    }

    /*
     * state
     * 0  -> Created  (Clan)
     * 1  -> Started
     * 2  -> Canceled by Owner
     * 3  -> Canceled by cron job
     */

    /**
     *
     * @version 1.0.0
     */
    public function createFriendlyMatch()
    {
        $userId = $this->userInfo['userId'];
        $type = $this->request->getData("type");  //classic,modern
        $player = $this->request->getData("player"); //4,6 (Max player)
        $requirePlayer = $this->request->getData("requirePlayer"); //2,3,4,5,6
        $groupNumber = 0;//$this->request->getData("groupNumber"); //0,1,2,3,4,5
        $tableType = "friendly";
        $userNetworkVersion = $this->request->getData("networkVersion");
        $groupMode = $this->request->getData("groupMode"); //'NoGroup','TwoVsTwo','TwoVsTwoVsTwo','ThreeVsThree'
        $creatorTeamName = $this->request->getData("takeApartUsersName");
        $aliveTime = time() - 60;

        $gameServer = $this->Friendlymatches->Matches->Servers->findServer($userId, $userNetworkVersion, $aliveTime);

        if (!$gameServer) {
            return $this->toJsonResponse(null, 'fail', 'Maintenance mode');
        }

        $serverId = $gameServer['id'];

        $userMatch[0]['user_id'] = $userId;
        $userMatch[0]['name'] = $creatorTeamName;
        $userMatch[0]['group_number'] = $groupNumber;
        $userMatch[0]['table_type'] = $tableType;

        $clanInfo = $this->Friendlymatches->Users->Userclans->getClanByUser($userId);
        if(!$clanInfo){
            return $this->toJsonResponse(null, 'faill', 'The user does not have a clan');
        }
        $clanId = $clanInfo['id'];

        $usersActiveInMatch = $this->Friendlymatches->Matches->Usermatches->userMatchInfoSimple($userId);
        $latestNetworkVersion = $this->Friendlymatches->Matches->Servers->latestServerNetworkVersion();

        if ($latestNetworkVersion >= $userNetworkVersion) {
            return $this->toJsonResponse(null, 'fail', 'Update to the latest game version');
        }

        if ($usersActiveInMatch === null && $usersActiveInMatch['state'] != 0) {
            //means the user has a match right now
            return $this->toJsonResponse(['matchId' => $usersActiveInMatch['match_id']], 'success', 'user has a match right now');
        }
        $matchId = $this->Friendlymatches->Matches->createMatch($type, $serverId, $requirePlayer, $player, $tableType, $groupMode, $userMatch, NULL, -1);

        $dataEntity = $this->Friendlymatches->newEntity([
            'match_id' => $matchId,
            'owner_id' => $userId,
            'clan_id' => $clanId,
            'networkversion' => $userNetworkVersion,
            'time' => time() + 10
        ]);
        $this->Friendlymatches->saveOrFail($dataEntity);
        $this->Friendlymatches->sendPushToActiveClanUsers($clanId, 'friendlyMatch created', $creatorTeamName, $matchId);
        return $this->toJsonResponse(['matchId' => $dataEntity->id], 'success', 'user has a match right now');
    }


    /**
     * @return \Cake\Http\Response
     * @version 1.0.0
     */
    public function getFriendlyMatch()
    {
        $clanId = $this->request->getData("clanId");
        $mappedResponse = $this->Friendlymatches->getFriendlyMatch($clanId);
        return $this->toJsonResponse($mappedResponse);
    }


    /**
     * @version 1.0.0
     */
    public function joinFriendlyMatch()
    {
        $userId = $this->userInfo['userId'];
        $requestGroupNumber = $this->request->getData("requestGroupNumber"); //0,2,3,4,5
        $friendlyMatchId = $this->request->getData("freindlyMatchId");
        $takeApartUsersName = $this->request->getData("takeApartUsersName");
        $networkVersion = (int)$this->request->getData("networkVersion");
        $tableType = "friendly";

        $friendlyMatch = $this->Friendlymatches->findFriendlyMatch($friendlyMatchId);
        if (!$friendlyMatch) {
            return $this->toJsonResponse(null, 'fail', 'No friendly match');
        }

        $matchId = $friendlyMatch['match_id'];
        $friendlyMatchClanId = $friendlyMatch['clan_id'];
        $friendlyMatchNetworkVersion = (int)$friendlyMatch['networkversion'];

        $clanInfo = $this->Friendlymatches->Users->Userclans->getClanByUser($userId);
        $clanId = $clanInfo['id'];

        $usersActiveInMatch = $this->Friendlymatches->Matches->Usermatches->userMatchInfoSimple($userId);
        $capacityCheck = $this->Friendlymatches->Matches->hasEnoughCapacity($matchId, $requestGroupNumber);

        if ($friendlyMatchNetworkVersion !== $networkVersion) {
            return $this->toJsonResponse(null, 'fail', 'Update to the latest game version');
        }

        if ($usersActiveInMatch == NULL) {
            return $this->toJsonResponse(null, 'fail', 'You joined this match before');
        }

        if ($usersActiveInMatch['match_id'] == $matchId && $usersActiveInMatch['Usermatch'] == 0) {
            return $this->toJsonResponse(null, 'fail', 'You joined this match before');
        }

        if ($capacityCheck === false) {
            return $this->toJsonResponse(null, 'fail', $capacityCheck);
        }

        if ($friendlyMatchClanId == $clanId) {
            $matchId = $friendlyMatch['match_id'];
            $EntityUserMatch = $this->Friendlymatches->Matches->Usermatches->newEntity([
                'user_id' => $userId,
                'match_id' => $matchId,
                'name' => $takeApartUsersName,
                'group_number' => $requestGroupNumber,
                'table_type' => $tableType
            ]);

            $this->Friendlymatches->Matches->Usermatches->saveOrFail($EntityUserMatch);
            $this->Friendlymatches->sendPushToActiveClanUsers($clanId, 'friendlyMatch join', NULL, $matchId);

            return $this->toJsonResponse();
        }

        return $this->toJsonResponse(null, 'fail', 'Try to join wrong match');


    }


    /**
     * @version 1.0.0
     */
    public function leaveFriendlyMatch()
    {
        $userId = $this->userInfo['userId'];
        $friendlyMatchId = $this->request->getData("freindlyMatchId");

        $freindlyMatch = $this->Friendlymatches->findFriendlyMatch($friendlyMatchId);

        if ($freindlyMatch === null) {
            return $this->toJsonResponse(null, 'fail', 'No friendly match');
        }

        $friendlyMatchClanId = $freindlyMatch['clan_id'];
        $friendlyOwnerUserId = $freindlyMatch['owner_id'];
        $matchId = $freindlyMatch['match_id'];

        $clanInfo = $this->Friendlymatches->Users->Userclans->getClanByUser($userId);
        $clanId = $clanInfo['id'];

        $usersActiveInMatch = $this->Friendlymatches->Matches->Usermatches->userMatchInfoSimple($userId);

        if ($usersActiveInMatch === null) {
            return $this->toJsonResponse(null, 'fail', 'You cancel this match before');
        }

        if ($usersActiveInMatch['usermatch']['match_id'] !== $matchId && $usersActiveInMatch['usermatch']['state'] == 2) {
            return $this->toJsonResponse(null, 'fail', 'You cancel this match before');
        }
        $usermatchId = $usersActiveInMatch['id'];
        if ($friendlyMatchClanId !== $clanId) {
            return $this->toJsonResponse(null, 'fail', 'Try to join wrong match');
        }

        if ($friendlyOwnerUserId == $userId) {
            $this->Friendlymatches->getConnection()->transactional(function () use ($friendlyMatchId, $matchId) {
                $this->Friendlymatches->changeState($friendlyMatchId, 2);
                $this->Friendlymatches->Matches->changeState($matchId, 2);
                $this->Friendlymatches->Matches->Usermatches->deleteAll(['match_id' => $matchId]);
            });
        } else {
            $this->Friendlymatches->Matches->Usermatches->deleteAll(['id' => $usermatchId]);
        }


        $this->Friendlymatches->sendPushToActiveClanUsers($clanId, 'friendlyMatch leave', NULL, $matchId);

        return $this->toJsonResponse();
    }


    /*
     * Just call by sever in order to validate userMatchId
     * @version 1.0.0
     * @return \Cake\Http\Response
     */
    public function friendlyMatchUserMatchValidate()
    {
        $userId = $this->request->getData("userId");
        $userMatch = $this->Friendlymatches->Matches->Usermatches->userMatchInfoSimple($userId);

        if ($userMatch === null) {
            return $this->toJsonResponse(null, 'fail', 'No friendly match');
        }

        $matchId = $userMatch['match_id'];
        $matchInfo = $this->Friendlymatches->Matches->matchInfo($matchId, [-1]);

        if ($matchInfo && $matchInfo->match['table_type'] == 'friendly') {
            return $this->toJsonResponse($matchInfo);
        }

        return $this->toJsonResponse(null, 'fail', 'No friendly match');
    }


    /**
     * @version 1.0.0
     */
    public function freshFriendlyMatch()
    {
        $userId = $this->userInfo['userId'];
        $freindlyMatchId = $this->request->getData("freindlyMatchId");
        $this->Friendlymatches->updateAll(['time' => time()], ['id' => $freindlyMatchId, 'owner_id' => $userId]);
        return $this->toJsonResponse();
    }

    /**
     * @return \Cake\Http\Response|mixed|null
     */
    public function test()
    {

        $this->userInfo['userId'] = 14;
        return $this->testPostAction($this, 'createFriendlyMatch', [
            'type' => 'classic',
            'player' => 4,
            'networkVersion' => 18,
            'groupMode' => 'TwoVsTwo',
            'takeApartUsersName' => 'aaaa',
            'requirePlayer' => 4
        ]);

         $res = $this->Friendlymatches->closeExpiredFriendlyMatch();

       // dd($res);
        return $this->testPostAction($this, 'getFriendlyMatch', [
            'clanId' => 29,
        ]);

        return $this->testPostAction($this, 'friendlyMatchUserMatchValidate', [
            'userId' => 29,
        ]);

        return $this->testPostAction($this, 'createFriendlyMatch', [
            'type' => 'classic',
            'player' => 4,
            'networkVersion' => 18,
            'groupMode' => 'TwoVsTwo',
            'takeApartUsersName' => 'aaaa',
            'requirePlayer' => 4
        ]);

        return $this->testPostAction($this, 'joinFriendlyMatch', [
            'freindlyMatchId' => 5270,
            'takeApartUsersName' => 'asdasdas',
            'requestGroupNumber' => 1,
            'networkVersion' => 16
        ]);

        //return $this->testPostAction($this, 'leaveFriendlyMatch', ['freindlyMatchId' => 6]);

        //return $this->testPostAction($this, 'friendlyMatchUserMatchValidate', ['userId' => 14]);

        //$res = $this->Friendlymatches->getFriendlyMatch(23, 362481);

    }


}
