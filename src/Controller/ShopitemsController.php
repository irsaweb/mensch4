<?php

namespace App\Controller;


use App\Config\AppSettings;

/**
 * @property \App\Model\Table\ShopitemsTable $Shopitems
 */
class ShopitemsController extends AppController
{

    /**
     * get list of items
     * @version 1.0.0
     */
    public function shopItemList()
    {
        $storename = $this->request->getData("storename");
        $includeLevel = (int) $this->request->getData("includeLevel",0);
        $storenameArray = explode(',', $storename);
        $cond = [
            'storename IN' => $storenameArray,
            'active' => 1
        ];

        if (!$includeLevel) {
            $cond['currency !='] = 'Level';
        }

        $shopItemList = $this->Shopitems->find()
            ->where($cond)
            ->select(['id', 'itemtype', 'itemname', 'storeid', 'coin', 'more', 'discount', 'price', 'priceafterdiscount', 'currency', 'storename', 'newitem', 'sort'])
            ->all()
            ->toArray();

        return $this->toJsonResponse($shopItemList);
    }

    /**
     * @version 1.0.0
     */
    public function shopItemRemoteConfig()
    {
        //TODO VIP put in controller as const
        $config = [
            'vipDay' => AppSettings::VipInfo['day'],
            'vipCoin' => AppSettings::VipInfo['coin']
        ];
        return $this->toJsonResponse($config);
    }


    public function test()
    {

        //   $res = $this->Shopitems->getItem('promote_clan','inapp');
        //$res = $this->Shopitems->getItemByCurrency('Toman');


        //debug($res);
    }

}
