<?php

namespace App\Controller;

/**
 * @property \App\Model\Table\BlockedusersTable $Blockedusers
 */
class BlockedusersController extends AppController
{


    /**
     * get Blocked User List
     * @version 1.0.0
     */
    public function getBlockedUser()
    {
        $userId = $this->userInfo['userId'];
        $blockedUserIds = $this->request->getData("blockedUserIds");// 5,6,109,23
        $takeApartUserIdsArray = explode(',', $blockedUserIds);
        $blockedUserList = $this->Blockedusers->getBlockedUserList($userId, $takeApartUserIdsArray);
        return $this->toJsonResponse($blockedUserList);
    }


    /**
     * blocking users
     * @param userId
     * @param blockedUserId
     * @version 1.0.0
     */
    public function blockUser()
    {
        $userId = $this->userInfo['userId'];
        $blockedUserId = $this->request->getData("blockedUserId");
        $data = ['user_id' => $userId, 'blocked_user_id' => $blockedUserId];

        $isBlockedBefore = $this->Blockedusers->find('all')->where($data)->first();
        if ($isBlockedBefore !== null) {
            return $this->toJsonResponse(null, 'fail', 'User is blocked before');
        }

        $data['time'] = time();
        $blockEntity = $this->Blockedusers->newEntity($data);
        if (!$this->Blockedusers->save($blockEntity)) {

            return $this->toJsonResponse(null, 'fail', $blockEntity->getErrors());
        };
        $this->Blockedusers->save($blockEntity);
        return $this->toJsonResponse(null,'succes','User is now blocked');
    }


    /**
     * blocking users
     * @param userId
     * @param blockedUserId
     * @version 1.0.0
     */
    public function deleteBlockedUser()
    {
        $userId = $this->userInfo['userId'];
        $blockedUserId = $this->request->getData("blockedUserId");
        $data = ['user_id' => $userId, 'blocked_user_id' => $blockedUserId];

        $isBlocked = $this->Blockedusers->find('all')->where($data)->first();

        if ($isBlocked !== null) {
            $this->Blockedusers->delete($isBlocked);
            return $this->toJsonResponse(null);
        } else {
            return $this->toJsonResponse(null, 'fail', 'User is not block');
        }

    }
}
