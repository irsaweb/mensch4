<?php

namespace App\Controller;

use App\Config\AppSettings;
use Cake\Event\Event;
use Cake\Event\EventInterface;
use Cake\ORM\Exception\PersistenceFailedException;
use Cake\ORM\TableRegistry;


/**
 *
 * @property \App\Model\Table\BanksTable $Banks
 * @property \App\Controller\Component\BazaarClientComponent $BazaarClient
 * @property \App\Controller\Component\GoogleClientComponent $GoogleClient
 */
class BanksController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('BazaarClient');
        $this->loadComponent('GoogleClient');
    }

    /**
     * @param Event $event
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
    }


    /**
     * state  0=waiting | 1=accepted
     * @version 1.0.0
     *
     */
    public function showUserPaymentList()
    {
        $userId = $this->userInfo['userId'];
        if ($this->request->getData('userId')) {
            $userId = $this->request->getData('userId');
        }
        $starttime = $this->request->getData('starttime', 0);
        $endtime = $this->request->getData('endtime', 0);
        $itemStoreIds = $this->request->getData('itemStoreIds');

        $data = [
            'user_id' => $userId,
            'starttime' => $starttime,
            'endtime' => $endtime,
            'state' => array(0, 1)
        ];

        if (!is_null($itemStoreIds)) {
            $data['item_store_id'] = $itemStoreIds;
        }

        $result = $this->Banks->getPaymentList($data);
        return $this->toJsonResponse($result);
    }


    /**
     * get all payments use for admin
     * @version 1.0.0
     */
    public function showPaymentList()
    {
        $starttime = $this->request->getData('starttime', 0);
        $endtime = $this->request->getData('endtime', 0);
        $state = $this->request->getData('state');
        $state = explode(',', $state);

        $data = [
            'state' => $state,
            'starttime' => $starttime,
            'endtime' => $endtime
        ];
//todo: getPaymentList niaz be user_id dare! ama inj11???
        $result = $this->Banks->getPaymentList($data);

        if ($result) {
            return $this->toJsonResponse($result);
        }
        return $this->toJsonResponse(null, 'fail', 'no payment with this condition');
    }


    /**
     * submitMobilePayment()
     * @throws \Exception
     * * @version 1.0.0
     */
    public function submitMobilePayment()
    {
        //todo(hamid) حتما از عملکرد این تغییرات مطمئن بشو
        $userId = $this->userInfo['userId'];
        $storeId = $this->request->getData("productId");
        $purchaseToken = $this->request->getData("purchaseToken");
        $storeName = $this->request->getData("storeName", 'aa');

        $serverPurchase = false;
        if ($storeName === "cafebazaar")
            $serverPurchase = $this->BazaarClient->validateIAP($storeId, $purchaseToken);
        else if ($storeName === "googleplay")
            $serverPurchase = $this->GoogleClient->validateIAP($storeId, $purchaseToken);


        if (!$serverPurchase) {
            return $this->toJsonResponse(null, 'fail', 'this purchase is not valid');
        }

        $paymentInfo = $this->Banks->findByToken($purchaseToken)->first();

        if ($paymentInfo === null) {
            return $this->toJsonResponse(null, 'fail', 'already used this token');
        }


        $Shopitems = TableRegistry::getTableLocator()->get('Shopitems');
        $item = $Shopitems->getItem($storeId, $storeName);

        if (!$item) {
            return $this->toJsonResponse(null, 'fail', " $storeId is not exist");
        }
        $price = $item['priceafterdiscount'];
        $coin = $item['coin'];

        $data = [
            'user_id' => $userId,
            'item_store_id' => $storeId,
            'store_name' => $storeName,
            'money' => $price,
            'token' => $purchaseToken,
            'paidtime' => time(),
            'ip' => $this->request->clientIp(),
            'state' => 1
        ];
        //todo(hamid) منظق این بخش هنوز فکر میکنم مشکل داشته باشه
        if ($coin > 0) {
            $dataUserUpdate = [
                'user_id' => $userId,
                'coin' => $coin,
                'service' => 'buy_coin',
                'description' => "$storeName $storeId"
            ];
        }

        if ($storeId == 'vip_pack_1') {
            $dataUserUpdate = [
                'user_id' => $userId,
                'coin' => AppSettings::VipInfo['coin'],
                'vip' => AppSettings::VipInfo['day'] * 24 * 3600,
                'promote_clan' => -1, //reset promote clan
                'service' => 'buy_vip',
                'description' => "$storeName $storeId"
            ];
        }

        $response = $this->addUserinfo([]);
        try {
            $this->Banks->getConnection()->transactional(function () use ($data, $dataUserUpdate) {
                $this->Banks->saveToBank($data);
                $this->Banks->Users->updateUserInfo($dataUserUpdate);
            });
            $response = $this->addUserinfo([]);
            return $this->toJsonResponse(null, 'success', "Done", $response);

        } catch (PersistenceFailedException $e) {
            return $this->toJsonResponse(null, 'fail', "rollbacked: " . $e->getEntity(), $response);
        }
    }


    public function test()
    {

        $params = ['productId' => 'dice_1', 'purchaseToken' => '34343434sdfsdfsdfdsf', 'storeName' => 'cafebazaar'];

//        return $this->testPostAction($this, 'showUserPaymentList', [
//            'userId' => '14',
//            'starttime' => 0,
//            'endtime' => 2,
//            'itemStoreIds' => '1,2',
//        ]);
//
//
//        return $this->testPostAction($this, 'showPaymentList', [
//            'starttime' => 0,
//            'endtime' => 2,
//            'state' => '1,2',
//        ]);


        return $this->testPostAction($this, 'submitMobilePayment', [
            'productId' => 'coin_pack_1',
            'purchaseToken' => 'StrGq1l-uTPkcpcH',
            'storeName' => 'cafebazaar',
        ]);

        //$this->Banks->fixBuyItem();
//        $data = ['user_id' => 13, 'starttime' => 0, 'endtime' => 0, 'state' => array(0, 1)];
//        debug($this->Banks->getPaymentList($data));
        // debug($this->Banks->findByToken('305914942629250263')->first());
        // echo json_encode($itemes);
    }
}
