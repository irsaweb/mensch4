<?php

namespace App\Controller;

use App\Config\AppSettings;
use App\Lib\ChangeLang;
use App\Lib\Fcm;
use App\Model\Entity\User;
use App\Network\Email\Email;
use Cake\Cache\Cache;
use Cake\Console\ShellDispatcher;
use Cake\Database\Expression\QueryExpression;
use Cake\Database\Query;
use cake\Event\Event;
use Cake\Utility\Hash;
use Exception;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @property \App\Model\Table\ShopitemsTable $Shopitems
 */
class UsersController extends AppController
{
    /**
     * beforeFilter
     *
     * @param mixed $event
     * @return void
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated([
            'spinnerInfo',
            'version',
            'getFcmChannelIds',
            'adsAgency',
            'test',
            'test1',
            'tableInfo'
        ]);
    }

    /**
     *
     */
    public function acoSync()
    {


        $command = '-app ' . APP . ' AclExtras.AclExtras aco_sync';
        $args = explode(' ', $command);
        $dispatcher = new \Cake\Console\ShellDispatcher($args, false);
        try {
            $dispatcher->dispatch();
            echo "Synced";
        } catch (Exception $e) {
            pr($e);
        }
    }

    /**
     *
     */
    public function initAcosDB()
    {

        $group = $this->User->Group;

        // Allow superadmins to everything
        $group->id = 1;
        $this->Acl->allow($group, 'controllers');

        // allow admins to go admin page except payment section
        $group->id = 2;
        $this->Acl->allow($group, 'controllers');
        $this->Acl->deny($group, 'controllers/banks/showPaymentList');
        $this->Acl->deny($group, 'controllers/admins/paymentlist');

        // allow managers to go admin page except payment section
        $group->id = 3;
        $this->Acl->allow($group, 'controllers');


        // allow partners to see finance part
        $group->id = 4;
        $this->Acl->deny($group, 'controllers');


        // allow users to go every where except admin and server
        $group->id = 5;
        $this->Acl->allow($group, 'controllers');

        $this->Acl->deny($group, 'controllers/users/joinGame');
        $this->Acl->deny($group, 'controllers/users/buyForServer');
        $this->Acl->deny($group, 'controllers/Usermatches/userMatchByUserId');
        $this->Acl->deny($group, 'controllers/Matches/updateMatchState');
        $this->Acl->deny($group, 'controllers/Matches/createMatch');
        $this->Acl->deny($group, 'controllers/Servers/alive');
        $this->Acl->deny($group, 'controllers/Servers/serverInfo');
        $this->Acl->deny($group, 'controllers/Servers/editActiveServer');

        // allow bots nothing !
        $group->id = 6;
        $this->Acl->deny($group, 'controllers');


        // allow server
        $group->id = 7;
        $this->Acl->deny($group, 'controllers');

        $this->Acl->allow($group, 'controllers/users/joinGame');
        $this->Acl->allow($group, 'controllers/users/login');
        $this->Acl->allow($group, 'controllers/users/botList');
        $this->Acl->allow($group, 'controllers/users/tableInfo');
        $this->Acl->allow($group, 'controllers/users/buyForServer');
        $this->Acl->allow($group, 'controllers/Usermatches/userMatchByUserId');
        $this->Acl->allow($group, 'controllers/Usermatches/updateUserMatchState');
        $this->Acl->allow($group, 'controllers/Matches/createMatch');
        $this->Acl->allow($group, 'controllers/Matches/updateMatchState');
        $this->Acl->allow($group, 'controllers/shopitems/shopItemList');
        $this->Acl->allow($group, 'controllers/Banks/showUserPaymentList');
        $this->Acl->allow($group, 'controllers/Servers/editActiveServer');
        $this->Acl->allow($group, 'controllers/Servers/alive');
        $this->Acl->allow($group, 'controllers/Servers/serverInfo');
        $this->Acl->allow($group, 'controllers/Servers/serverRemoteConfig');
        $this->Acl->allow($group, 'controllers/Usermatches/avgWinRank');
        $this->Acl->allow($group, 'controllers/Friendlymatches/friendlyMatchUserMatchValidate');


        // we add an exit to avoid an ugly "missing views" error message
        echo "all done";
        exit;
    }

    /**
     * getUserInfo
     * //server only
     * @return \Cake\Http\Response|void
     * @version 1.0.0
     */
    public function getUserInfo()
    {
        $userId = $this->userInfo['userId'];
        $avgWinRank = $this->request->getData('avgWinRank');
        $avgWinRank = isset($avgWinRank) ? ($avgWinRank === 'True') : false;
        $data = $this->Users->userInfo($userId, false, $avgWinRank);

        if (!empty($data)) {
            $data['serverTime'] = time();
            return $this->toJSONResponse($data);
        } else {
            return $this->toJSONResponse($userId, 'fail', 'userId is not valid');
        }
    }


    /**
     * @version 1.0.0
     */
    public function getUserInfoClient()
    {
        $userId = $this->userInfo['userId'];
        $data = $this->Users->userInfo($userId);

        if (!empty($data)) {
            $data['serverTime'] = time();
            return $this->toJSONResponse($data);
        } else {
            return $this->toJSONResponse($userId, 'fail', 'userId is not valid');
        }
    }


    /**
     * tableInfo
     *
     * @return \Cake\Http\Response|void
     * @version 1.0.0
     */
    public function tableInfo()
    {
        $version = $this->request->getData('version');

        if (isset($version) && $version == 2) {
            $award = AppSettings::AwardInfo;
        } else {
            $award = AppSettings::AwardInfo['NoGroup'];
        }

        $data['entranceInfo'] = AppSettings::TableEntranceCost;
        $data['awardInfo'] = $award;
        return $this->toJsonResponse($data);
    }


    /**
     * updateProfile
     *
     * @return \Cake\Http\Response
     * @version 1.0.0
     */
    public function updateProfile()
    {
        $userId = $this->userInfo['userId'];
        $newName = $this->request->getData('name');
        //$howManySpace=preg_match('/\s/',$name);  //space
        $howManySpace = substr_count($newName, ' ');
        $nameLen = strlen($newName) - $howManySpace; //letters

        $addUserinfo = $this->addUserinfo();

        if ($nameLen <= 3) {
            return $this->toJSONResponse($userId, 'fail', 'Name should contain 3 letters at least', $addUserinfo);
        }

        $userInfo = $this->Users->userInfo($userId);
        $name = $userInfo['name'];
        $editNameCount = $userInfo['edit_name_count'];
        $userCoin = $userInfo['coin'];
        $coinNeed = (pow($editNameCount, 2) + $editNameCount) * 50;

        if ($name === null) {
            $this->Users->updateAll(['name' => "'$newName'"], ['id' => $userId]);
            Cache::delete("$userId._.False", 'userInfo');
            Cache::delete("$userId._.True", 'userInfo');
            return $this->toJSONResponse(null, 'success', 'Done', $addUserinfo);
        }
        if ($userCoin <= 100) {
            return $this->toJSONResponse(['resource' => 'coin', 'amount' => $coinNeed - $userCoin], 'success', 'Done', $addUserinfo);
        }

        $newEditNameCount = $editNameCount + 1;
        $userData = [
            'user_id' => $userId,
            'coin' => -$coinNeed,
            'service' => 'change_name'
        ];

        $this->Users->updateUserInfo($userData);
        $this->Users->updateAll(['name' => $newName, 'edit_name_count' => $newEditNameCount], ['id' => $userId]);
        Cache::delete("$userId._.False", 'userInfo');
        Cache::delete("$userId._.True", 'userInfo');
        return $this->toJSONResponse();
    }

    /**
     * changeLanguage
     *
     *  langs: ['fa','en','tur']
     *
     * @return \Cake\Http\Response|void
     * @version 1.0.0
     */
    public function changeLanguage()
    {
        $langsSupport = ['fa', 'en', 'tur'];
        $userId = $this->userInfo['userId'];
        $newLanguage = $this->request->getData('language');

        if (!in_array($newLanguage, $langsSupport)) {
            return $this->toJSONResponse($userId, 'fail', "this $newLanguage is not valid");
        }
        $user = $this->Users->get($userId);
        $user->language = $newLanguage;

        $user = $this->Users->save($user);

        $language = new ChangeLang();
        $language->startToggleLg($newLanguage);   //change language on the fly
        Cache::delete("$userId._.True", 'userInfo');

        $userInfo = $user->toArray();
        $userInfo['servertime'] = time();
        return $this->toJSONResponse(null, 'success', 'Done', ['userInfo' => $userInfo]);
    }

    /**
     * freeCoin
     *
     * @param $serviceName telegram|instagram
     * @return \Cake\Http\Response|void
     * @version 1.0.0
     */
    public function freeCoin()
    {
        $freeCoin = [
            'telegram' => 100,
            'instagram' => 150
        ];

        $userId = $this->userInfo['userId'];
        $serviceName = $this->request->getData('serviceName');

        // $serviceName = 'telegr11am'; // for test

        if (!array_key_exists($serviceName, $freeCoin)) {
            return $this->toJSONResponse(null, 'fail', "<$serviceName> service is not valid");
        }

        $User = $this->Users->get($userId);
        $serviceFieldName = $serviceName . '_claimed';

        if ($User->{$serviceFieldName} == true) {
            return $this->toJSONResponse(null, 'fail', 'free coin taken before');
        }

        $User->coin = $User->coin + $freeCoin[$serviceName];
        $User->{$serviceFieldName} = true;

        $this->Users->save($User);
        Cache::delete("$userId._.True", 'userInfo');

        $userInfo = $User->toArray();
        $userInfo['servertime'] = time();
        return $this->toJSONResponse(null, 'success', 'Done', ['userInfo' => $userInfo]);
    }

    /**
     * rankInfo
     *
     * @return \Cake\Http\Response
     * @version 1.0.0
     *
     */
    public function rankInfo()
    {
        $gameType = (int)$this->request->getData('gameType');  //4 or 6
        $table = $this->request->getData('table');  //friendly
        $groupMode = $this->request->getData('groupMode'); //'NoGroup', 'TwoVsTwo', 'TwoVsTwoVsTwo' ,'ThreeVsThree',friendly

        if (isset($groupMode)) {
            $awardInfo = AppSettings::AwardInfo[$groupMode][$gameType][$table];
        } else {
            $awardInfo = AppSettings::AwardInfo['NoGroup'][$gameType][$table];
        }
        return $this->toJsonResponse($awardInfo);
    }


    /**
     *
     * @version 1.0.0
     */
    public function spinnerInfo()
    {
        $spinnerInfo = Hash::extract(AppSettings::SpinnerInfo, "{n}.value");
        return $this->toJsonResponse(['spinnerInfo' => $spinnerInfo]);
    }


    /**
     * spin
     *
     * @return \Cake\Http\Response
     * @version 1.0.0
     */
    public function spin()
    {

        $userId = $this->userInfo['userId'];
        $userInfo = $this->Users->userInfo($userId);
        $time = time();
        $spinTime = $userInfo['spintime'];
        $spinVideo = $userInfo['spinvideo'];
        $vipTime = $userInfo['vip'];

        if ($spinTime < $time and $spinVideo == 1) {
            $this->Users->updateAll(['spinvideo' => 0], ['id' => $userId]);
        }

        if ($spinTime < $time or $spinVideo == 0) {
            $spinnerInfoWeight = [];
            foreach (AppSettings::SpinnerInfo as $spinner) {
                $chance = $spinner['chance'];
                $coin = $spinner['value'];

                for ($i = 0; $i < $chance; $i++) {
                    $spinnerInfoWeight[] = $coin;
                }
            }

            $rand = rand(0, count($spinnerInfoWeight) - 1);

            $coin = $spinnerInfoWeight[$rand];

            foreach (AppSettings::SpinnerInfo as $key => $spinner2) {
                if ($spinner2['value'] == $coin) {
                    break;
                }
            }

            $nextSpinTime = $time + AppSettings::TimeBetweenSpins;
            if ($vipTime > $time) {
                $nextSpinTime = $time + AppSettings::TimeBetweenSpins / 2;
            }

            $userData = [
                'user_id' => $userId,
                'coin' => $coin
            ];

            if ($spinTime < $time) {
                $updateField = ['spintime' => $nextSpinTime];
                $userData['service'] = 'spin';
            } else {
                $updateField = ['spinvideo' => 1];
                $userData['service'] = 'spin_ads';
            }

            $this->Users->updateAll($updateField, ['id' => $userId]);
            $this->Users->updateUserInfo($userData);

            return $this->toJsonResponse(['spin' => $key]);
        }
        return $this->toJsonResponse(null, 'fail', 'Please wait for next spin');
    }

    /**
     * This function set uuid for sending push notification one signal
     *
     * @throws Exception
     */
    public function setUuid()
    {
        $uuid = $this->request->getData('uuid'); //for unlink send NULL
        $userId = $this->userInfo['userId'];

        $User = $this->Users->get($userId);
        $User->uuid = $uuid;

        if ($this->Users->save($User)) {
            Cache::delete("$userId._.True", 'userInfo');
            return $this->toJsonResponse();
        } else {
            return $this->toJsonResponse(false, 'error', 'An error in the save operation');
        }

    }


    /**
     * version
     * check if there is a new version announce users
     * min version means at least user should have this version
     *
     * @return \Cake\Http\Response|void
     * @version 1.0.0
     */
    public function version()
    {
        //$this->autoRender = false; // We don't render a view in this example
        //$this->request->allowMethod('ajax'); // No direct access via browser URL

        $version = [];
        $version['googleplay']['UpdateLink'] = "linkkkkkkk";
        $version['googleplay']['min'] = '1.0.0.0';
        $version['googleplay']['latest'] = '1.0.0.3';
        $version['googleplay']['serverversion'] = 13;

        $version['cafebazaar']['updatelink'] = "https://cafebazaar.ir/app/com.NoaGames.Menchico";
        $version['cafebazaar']['min'] = '2.0.0.28';
        $version['cafebazaar']['latest'] = '2.0.0.28';
        $version['cafebazaar']['serverversion'] = 16; //minmum network version

        $version['selfpublish']['updatelink'] = "http://www.noagames.co";
        $version['selfpublish']['min'] = '1.0.0.0';
        $version['selfpublish']['latest'] = '1.0.0.3';
        $version['selfpublish']['serverversion'] = 13;

        return $this->toJSONResponse($version);

    }


    /**
     * data for ranking page
     * ranking
     *
     * @return \Cake\Http\Response
     * @version 1.0.0
     */
    public function ranking()
    {
        $start = 0;
        $length = 100;
        $userId = $this->userInfo['userId'];

        $rank = Cache::read("rank_info_page_" . $userId, 'veryshort');

        if (!$rank) {
            $rank['daily'] = $this->Users->ranking($userId, $length, $start, 'dailytrophy');
            $rank['weekly'] = $this->Users->ranking($userId, $length, $start, 'weeklytrophy');
            $rank['total'] = $this->Users->ranking($userId, $length, $start, 'totaltrophy');
            Cache::write("rank_info_page_".$userId, $rank, 'veryshort');
        }

        return $this->toJSONResponse($rank);
    }


    /**
     * botList
     *
     * @return \Cake\Http\Response
     * @version 1.0.1
     */
    public function botList()
    {
        ///      'order' => 'rand()', vase chie?
        $botNames = $this->Users->find()
            ->where(['name IS Not' => null])
            ->select(['name'])
            ->order('rand()')
            ->limit(50)
            ->all()->toArray();
        $botNames = Hash::extract($botNames, '{n}.name');
        return $this->toJSONResponse(['botNames' => $botNames]);
    }


    /**
     * genarete serverTime
     *
     * @version 1.0.0
     */
    public function serverTime()
    {
        return $this->toJSONResponse(['serverTime' => time()]);
    }


    /**
     * getFcmChannelIds
     *
     * @return \Cake\Http\Response|void
     * @version 1.0.0
     */
    public function getFcmChannelIds()
    {
        //use App\Lib\Fcm;
        $this->Fcm = new Fcm();
        $data = $this->Fcm->notficationChannelIds();
        return $this->toJsonResponse($data);
    }


    /**
     * updateCustomInfo
     *
     * @return \Cake\Http\Response|void
     * @version 1.0.0
     * @todo no test
     */
    public function updateCustomInfo()
    {

        $userId = $this->userInfo['userId'];
        $storeName = $this->request->getData('storeName');
        $colour4 = $this->request->getData('colour4');
        $colour6 = (int)$this->request->getData('colour6');
        $symbol = $this->request->getData('symbol');
        $dice = $this->request->getData('dice');
        $ring = $this->request->getData('ring');

        //$colour4='(select*from(select(sleep(2)))a)';

        $itemStoreIdsArray = [
            $dice,
            $symbol,
            $ring
        ];

        $time = time();

        $this->loadModel('Shopitems');
        //check if user bought custom items or not
        $userMustVip = false;
        $userMustBuy = false;

        foreach ($itemStoreIdsArray as $key => $itemStoreId) {
            //  debug([$itemStoreId, $storeName]);
            $shopItem = $this->Shopitems->getItem($itemStoreId, $storeName);
            if ($shopItem) {
                $item[$key] = $shopItem;

                if ($item[$key]['currency'] == 'Vip') {

                    $userInfo = $this->Users->userInfo($userId);
                    $vipUserTime = $userInfo['vip'];

                    if ($vipUserTime < $time) {
                        $userMustVip = true;
                        break;
                    }
                }

                if ($item[$key]['priceafterdiscount'] > 0) {
                    $boughtItem = $this->Users->Banks->findItem($userId, $itemStoreId);
                    if (!$boughtItem) {
                        $userMustBuy = true;
                        break;
                    }
                }
            } else {
                $userMustBuy = true;
            }
        }

        if (!$userMustBuy && !$userMustVip) {

            $this->Users->updateAll([
                'colour4' => $colour4,
                'colour6' => $colour6,
                'dice' => "'$dice'",
                'symbol' => "'$symbol'",
                'ring' => "'$ring'"
            ], ['id' => $userId]);

            Cache::delete("$userId._.False", 'userInfo');
            Cache::delete("$userId._.True", 'userInfo');

            return $this->toJsonResponse(null, 'success', 'Done', $this->addUserinfo());
        }

        return $this->toJsonResponse(null, 'fail', 'buy item', $this->addUserinfo());
    }


    /**
     * buyForUser
     *
     * @return void
     * @version 1.0.0
     */
    public function buyForUser()
    {
        $userId = $this->userInfo['userId'];
        return $this->__buyShopItem($userId);
    }

    /**
     * __buyShopItem
     *
     * @param mixed $userId
     * @return \Cake\Http\Response|void
     * @version 1.0.0
     */
    private function __buyShopItem($userId)
    {

        $count = (int)abs($this->request->getData('count'));
        $itemStoreIds = $this->request->getData('storeIds');
        $storeName = $this->request->getData('storeName');
        $itemStoreIdsArray = explode(',', $itemStoreIds);

        if ($count <= 0) {
            $count = 1;
        }

        $this->loadModel('Shopitems');
        $itemCoinPrice = 0;
        $desc = '';
        $boughtItemBefore = null;

        foreach ($itemStoreIdsArray as $key => $itemStoreId) {
            $item[$key] = $this->Shopitems->getItem($itemStoreId, $storeName);
            $itemCoinPrice += $item[$key]['priceafterdiscount'];
            //$desc += $item[$key]['storeid'].' '; //،TODO check it not working
            //check user bought before
            if (!$item[$key]['consumable'] and $item[$key]['currency'] == 'Coin') {
                $boughtItemBefore = $this->Users->Banks->findItem($userId, $itemStoreId);
                if ($boughtItemBefore) {
                    break;
                }
            }
        }

        $totalPrice = $itemCoinPrice * $count;
        $userInfo = $this->Users->userInfo($userId);
        $userCoin = $userInfo['coin'];
        $userInGameChat = $userInfo['in_game_chat'];

        if ($userInGameChat == 0 && $itemStoreId == 'in_game_chat' && count($itemStoreIdsArray) == 1) {
            $totalPrice = 0;
        }

        if ($boughtItemBefore) {
            return $this->toJsonResponse(null, 'fail', 'item bought before');
        }

        if ($item[0]['currency'] == 'Coin') {

            if ($userCoin <= $totalPrice) {
                $data = ['resource' => 'coin', 'amount' => $totalPrice - $userCoin, [$totalPrice, $userCoin]];
                return $this->toJsonResponse($data, "not enough resource", 'not enough coin');
            }

            foreach ($itemStoreIdsArray as $itemStoreId) {
                $data[] = array(
                    'user_id' => $userId,
                    'item_store_id' => $itemStoreId,
                    'store_name' => $storeName,
                    'money' => 0,
                    'token' => 0,
                    'ip' => $this->request->clientIp(),
                    'paidtime' => time(),
                    'state' => 1,
                    'timereg' => 0,
                    'bankname' => ''
                );
            }

            $time = 0;
            if ($itemStoreId == 'in_game_chat') {
                $service = 'in_game_chat';
                $time = 24 * 3600;
            } else {
                $service = 'shop_item';
            }

            $userData = [
                'user_id' => $userId,
                'coin' => -$totalPrice,
                'service' => $service,
                'in_game_chat' => $time,
                'description' => $desc
            ];

            try {
                $res = $this->Users->getConnection()->transactional(function () use ($itemStoreId, $data, $userData) {
                    $this->Users->Banks->saveToBank($data);
                    $this->Users->updateUserInfo($userData);
                    return true;
                });

                if ($res) {
                    return $this->toJsonResponse(null, 'success', 'Done', ['userInfo' => $this->Users->userInfo($userId)]);
                }

            } catch (\Exception $e) {
                return $this->toJsonResponse($e->getMessage(), 'fail', 'rollback');
            }
        }

        if ($item[0]['currency'] === 'Video') {

            $watchAdsForItem = $this->Users->Banks->findItem($userId, $itemStoreId);
            if (!$watchAdsForItem) {
                $data[] = [
                    'user_id' => $userId,
                    'item_store_id' => $itemStoreId,
                    'store_name' => $storeName,
                    'money' => 0,
                    'token' => 0,
                    'watch_video' => 1,
                    'ip' => $this->request->clientIp(),
                    'paidtime' => time(),
                    'state' => 0,
                    'timereg' => 0,
                    'bankname' => ''
                ];
                $this->Users->Banks->saveToBank($data);

            } else {

                $watchAdsCount = $watchAdsForItem['watch_video'];
                $howManyNeedWatchAds = $this->Shopitems->getItem($itemStoreId, $storeName)['priceafterdiscount'];

                if ($howManyNeedWatchAds == $watchAdsCount + 1) {

                    $this->Users->Banks->updateAll(
                        ['watch_video' => $watchAdsCount + 1, 'state' => 1],
                        ['id' => $watchAdsForItem['id']]
                    );
                } else {
                    $this->Users->Banks->updateAll(
                        ['watch_video' => $watchAdsCount + 1],
                        ['id' => $watchAdsForItem['id']]
                    );
                }
            }
            return $this->toJsonResponse(null, 'success', 'Done', ['userInfo' => $this->Users->userInfo($userId)]);
        }

        return $this->toJsonResponse(null, 'fail', 'not response....');
    }

    /**
     * buyForServer
     *
     * @return void
     * @version 1.0.0
     */
    public function buyForServer()
    {
        $userId = $this->request->getData('userId');
        $this->__buyShopItem($userId);
    }


    /**
     * userIdToNickName
     *
     * @return \Cake\Http\Response|void
     * @version 1.0.0
     */
    public function userIdToNickName()
    {
        $userIds = $this->request->getData('userIds');
        $userIdsArray = explode(',', $userIds);
        $users = $this->Users->find()->where(['id IN' => $userIdsArray])->select(['id', 'name'])->all();
        return $this->toJsonResponse($users);
    }

    /**
     * @version 1.0.0
     */
    public function hasPromotion()
    {
        $userId = $this->userInfo['userId'];
        $storeName = $this->request->getData('storeName');

        $promotion = $this->Users->hasPromotion($userId, $storeName);
        return $this->toJsonResponse($promotion);
    }

    /**
     * adsAgency
     *
     * @return \Cake\Http\Response
     * @version 1.0.0
     */
    public function adsAgency()
    {
        $storename = $this->request->getData("storename");
        $vsersion = $this->request->getData("version");

        if ($storename === 'cafebazaar') {
            $agency = [
                'ads' => [
                    '0' => ['name' => 'Tapsell', 'priority' => 1, 'enable' => 0],
                    //'2' => ['name' => 'Tapligh', 'priority' => 2, 'enable' => 1],                                           ),*/
                    '1' => ['name' => 'UnityAds', 'priority' => 4, 'enable' => 1],
                    '2' => ['name' => 'Chartboost', 'priority' => 3, 'enable' => 0],
                    '3' => ['name' => 'CrossPromotion', 'priority' => 2, 'enable' => 1],
                    '4' => ['name' => 'Deema', 'priority' => 5, 'enable' => 1]
                ]
            ];
        } else {
            $agency = [
                'ads' => [
                    '0' => ['name' => 'UnityAds', 'priority' => 1, 'enable' => 1],
                    '1' => ['name' => 'Chartboost', 'priority' => 2, 'enable' => 0]
                ]
            ];
        }
        return $this->toJsonResponse($agency);
    }

    /**
     * watchAds
     *
     * @return \Cake\Http\Response
     * @version 1.0.0;
     */
    public function watchAds()
    {

        $userId = $this->userInfo['userId'];
        $canWatch = filter_var($this->request->getData('canWatch'), FILTER_VALIDATE_BOOLEAN); //true,false

        $coin = 50;
        $howManyCanWatch = 5;
        $time = time();
        $userInfo = $this->Users->userInfo($userId);
        $adsWatched = $userInfo['ads_watched'];

        if ($adsWatched <= $howManyCanWatch) {

            if (!$canWatch) {
                $userData = [
                    'user_id' => $userId,
                    'coin' => $coin,
                    'ads_watched' => 1,
                    'service' => 'watch_ads'
                ];
                $this->Users->updateUserInfo($userData);
            }

            $nextTime = $time;
            if ($adsWatched == $howManyCanWatch) {
                $todayM = date('m', $time);  //today month
                $todayD = date('d', $time);  //today day
                $todayY = date('Y', $time);  //today year
                $nextTime = mktime(0, 0, 0, $todayM, $todayD + 1, $todayY); //End of Today time stamp
            }
        } else {
            $todayM = date('m', $time);  //today month
            $todayD = date('d', $time);  //today day
            $todayY = date('Y', $time);  //today year
            $nextTime = mktime(0, 0, 0, $todayM, $todayD + 1, $todayY); //End of Today time stamp
        }
        $data = [
            'nextTime' => $nextTime,
            'coin' => $coin
        ];
        $response = $this->addUserinfo([]);
        return $this->toJsonResponse($data, 'success', 'Done', $response);
    }


    /**
     * setTutorialStep
     *
     * @return \Cake\Http\Response
     * @version 1.0.0
     */
    public function setTutorialStep()
    {
        $userId = $this->userInfo['userId'];
        $item = (int)$this->request->getData("item");
        $step = (int)$this->request->getData("step");
        $isFinish = (int)$this->request->getData("isFinish"); // true or false

        $this->Users->updateAll(
            ['tutorial_item' => $item, 'tutorial_step' => $step, 'tutorial_finish' => $isFinish],
            ['id' => $userId]
        );

        Cache::delete("$userId._.True", 'userInfo');
        $response = $this->addUserinfo([]);
        return $this->toJsonResponse(null, 'success', 'Done', $response);
    }

    /**
     * profile
     *
     * @return \Cake\Http\Response|void
     * @version 1.0.0
     */
    public function profile()
    {

        $userId = $this->request->getData("userId");
        $profile = Cache::read("profile_info_$userId", 'veryshort');

        if ($profile) {
            return $this->toJsonResponse($profile);
        }


        $userInfo = $this->Users->userInfo($userId);
        $clanInfo = $this->Users->Userclans->getClanByUser($userId);
        $matchInfo = $this->Users->Usermatches->userMatchInfo($userId, 0);

        $allNogroup = $this->Users->Usermatches->howManyWin($userId, 'NoGroup', [1, 2, 3, 4]);
        $winNogroup = $this->Users->Usermatches->howManyWin($userId, 'NoGroup', 1);
        $allTwoVsTwo = $this->Users->Usermatches->howManyWin($userId, 'TwoVsTwo', [1, 2, 3, 4]);
        $winTwoVsTwo = $this->Users->Usermatches->howManyWin($userId, 'TwoVsTwo', 1);

        $profile['userInfo'] = $userInfo;
        $profile['matchInfo'] = $matchInfo;
        $profile['clanInfo'] = $clanInfo;
        $profile['matchStatus'] = [
            'NoGroup' => [
                'all' => $allNogroup,
                'win' => $winNogroup,
            ],
            'TwoVsTwo' => [
                'all' => $allTwoVsTwo,
                'win' => $winTwoVsTwo,
            ]
        ];

        Cache::write("profile_info_$userId", $profile, 'veryshort');
        return $this->toJsonResponse($profile);
    }

    /**
     * userInfoCacheRemover
     * @version 1.0.0
     */
    public function userInfoCacheRemover()
    {
        $userId = $this->request->getData("userId");
        Cache::delete("$userId._.False", 'userInfo');
        Cache::delete("$userId._.True", 'userInfo');
        return $this->toJsonResponse();
    }


    /**
     * searching user according team name and id
     * @version 1.0.0
     */
    public function searchUser()
    {
        $userIdentifier = $this->request->getData('userIdentifier');  //name or id
        $start = (int)$this->request->getData("start");
        $length = (int)$this->request->getData("length");

        $userInfo = $this->Users->searchUser($start, $length, $userIdentifier);

        if ($userInfo['count'] !== 0) {
            $userInfo['countFirst'] = $start;
            ksort($userInfo);
            return $this->toJsonResponse($userInfo);
        }

        return $this->toJsonResponse('null', 'fail', 'no user with this input');
    }

    /**
     * @version 1.0.0
     */
    public function userReport()
    {
        ini_set('memory_limit', '16048M');
        set_time_limit(0);
        @$userId = $this->request->getData("userId");
        $dateFilter = $this->request->getData("dateFilter"); //daily, weekly, monthly
        @$startTime = $this->request->getData("startTime");
        @$endTime = $this->request->getData("endTime");

        @$startDay = $this->request->getData("startDay");
        @$startMonth = $this->request->getData("startMonth");

        @$endDay = $this->request->getData("endDay");
        @$endMonth = $this->request->getData("endMonth");

        if (isset($startDay) and isset($startMonth) and isset($endDay) and isset($endMonth)) {
            $startTime = strtotime(date("Y-$startMonth-$startDay 00:00:00"));
            $endTime = strtotime(date("Y-$endMonth-$endDay 23:59:59"));
        }


        if ($dateFilter == 'daily') {
            $stepTime = 24 * 3600;
        } elseif ($dateFilter == 'weekly') {
            $stepTime = 7 * 24 * 3600;
        } else if ($dateFilter == 'monthly') {
            $stepTime = 31 * 24 * 3600;
        } else {
            $stepTime = 365 * 24 * 3600;
        }

        $statistics = array();
        $startTimeReport = $startTime;

        $i = 0;
        do {
            $endTimeStep = $startTimeReport + $stepTime;
            $userInfos = $this->Users->find()->where(function (QueryExpression $exp, Query $q) use ($startTimeReport, $endTimeStep) {
                return $exp->between('registerdate', $startTimeReport, $endTimeStep);
            })
                ->limit(10)
                ->toArray();


            foreach ($userInfos as $userInfo) {
                $userIds[] = $userInfo['id'];
                if ($userInfo['tutorial_finish'] == 0) {
                    $userIdsDontFinishTutorial[] = $userInfo['id'];
                }
            }
            $statistics[$i]['users'] = $userIds;
            $statistics[$i]['finishTutorialRate'] = number_format(100 - (count($userIdsDontFinishTutorial) / count($userIds)) * 100, 2);
            $statistics[$i]['startTime'] = $startTimeReport;
            $statistics[$i]['endTime'] = $endTimeStep;
            $statistics[$i]['report'] = $this->Users->Finances->financeReport($userIds, $startTimeReport, $endTimeStep);
            $statistics[$i]['report'] = $this->Users->Banks->bankReport($userIds, $startTimeReport, $endTimeStep);
            //todo bayad chek shavad
            $statistics[$i]['report'] = $this->Users->Banks->bankReport2($startTimeReport, $endTimeStep);
            $startTimeReport = $endTimeStep;
            $i++;

        } while ($endTimeStep < $endTime);

        return $this->toJsonResponse($statistics);
    }


    /**
     * unlink google account
     * @version 1.0.0
     */
    public function unlinkGoogle()
    {
        $userId = $this->userInfo['userId'];
        $this->Users->updateAll(['gmail_account' => NULL], ['id' => $userId]);

        Cache::delete("$userId._.False", 'userInfo');
        Cache::delete("$userId._.True", 'userInfo');

        return $this->toJsonResponse('data');
    }


    /**
     *  unlink Phone number
     * @version 1.0.0
     */
    public function unlinkPhone()
    {
        $userId = $this->userInfo['userId'];
        $this->Users->updateAll(['phone_number' => NULL, 'unique_id_sms' => NULL], ['id' => $userId]);

        Cache::delete("$userId._.False", 'userInfo');
        Cache::delete("$userId._.True", 'userInfo');

        return $this->toJsonResponse('data');
    }

    /**
     *
     */
    public function test1()
    {
        $this->userInfo['userId'] = 13;
        return $this->testPostAction($this, 'hasPromotion', [
            'storeName' => 'cafebazzar'
        ]);


        return $this->testPostAction($this, 'updateCustomInfo', [
            'storeName' => 'inapp',
            'colour4' => 0,
            'colour6' => 2,
            'symbol' => 'symbol_12',
            'dice' => 'dice_12',
            'ring' => 'ring_35'
        ]);
    }


    /**
     *
     */
    public function test()
    {
        $this->autoRender = false;
        echo "Hello World-cake4";
//        $this->userInfo['userId'] = 89847;
//        return $this->testPostAction($this, 'buyForUser', [
//            'count' => 1,
//            'storeIds' => 'dice_22', // 'dice_5',
//            'storeName' => 'inapp'
//        ]);

        /*        return $this->testPostAction($this, 'setTutorialStep', [
                    'step' => 4,
                    'item' => 1,
                    'isFinish' => 1
                ]);*/
        /*
                return $this->testPostAction($this, 'searchUser', [
                    'userIdentifier' => 'مریم',
                    'start' => 0,
                    'length' => 10
                ]);
                */
        //    return $res;
        //      return $this->testPostAction($this, 'profile', [
        //           'userId' => 14
        //    ]);

//        $res = $this->Users->userInfo(1, true, true);
        //debug($res);
////		$colour6 = $this->request->getData("colour6"];
//        $userId = 69;
//        $colour6 = 8;
//        $colour4 = 'aaaaaaaaaaaaaaaaaaa';
//
//
//        $data[] = [
//            'id' => 1,
//            'user_id' => $userId,
//            'item_store_id' => 'dice_5',
//            'store_name' => 'inappaaaaaaa',
//            'money' => '0',
//            'token' => 0,
//            'ip' => $this->request->clientIp(),
//            'paidtime' => time(),
//            'state' => 1
//        ];
//        $this->Users->Bank->saveToBank($data);
    }

}


