<?php

namespace App\Controller;

use App\Config\AppSettings;
use App\Controller\Component\Logger;
use App\Lib\Kavenegar;
use App\Lib\NoaEncryption;
use Cake\Error\Debugger;
use Cake\Event\EventInterface;
use Cake\Http\Client;
use Cake\Http\Client\Adapter\Stream;
use Cake\Log\Log;

use Google_Client;

/**
 * Class AuthsmsController
 * @property \App\Model\Table\AuthTable $Auth
 * @property \App\Model\Table\VerificationcodesTable $Verificationcodes
 * @property \Authentication\Controller\Component\AuthenticationComponent $Authentication
 */
class AuthController extends AppController
{
    /**
     * @var string
     */
    protected $GOOGLE_CLIENT_ID = "207559745475-gmek15t8h56ofq1hae1rbloldqbse697.apps.googleusercontent.com";

    /**
     * @inheritDoc
     */

    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Users');
    }

    /**
     * @param EventInterface $event
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated([
            'test',
            'unauth',
            'login',
            'mobileSmsLogin',
            'googleLogin',
        ]);
    }

    /**
     * @return \Cake\Http\Response
     * @version 1.0.0
     */
    public function logout()
    {
        $this->Authentication->logout();
        return $this->toJsonResponse(null, 'fail', 'you are logouted');

    }

    /**
     * @return \Cake\Http\Response
     * @version 1.0.0
     */
    public function loggedIn()
    {
        if ($this->request->is('post')) {
            return $this->toJsonResponse(null, 'success', 'Hello User! you are logged In :)');
        }
        return $this->render('loggedIn');
    }

    /**
     * @return \Cake\Http\Response
     * @version 1.0.0
     */
    public function unauth()
    {
        return $this->toJsonResponse(null, 'fail', 'Authentication is required to continue');
    }

    /**
     * @return bool
     * @version 1.0.0
     */
    private function _checkUserIsLoggin()
    {
        $AuthStatus = $this->Authentication->getResult();
        return $AuthStatus->isValid();
    }

    /**
     * @param $userID
     * @return \Cake\Http\Response
     */
    public function _getUserInfo($userID)
    {

        $data = $this->Auth->userInfo($userID, false);
        if (!empty($data)) {
            $data['serverTime'] = time();
            return $this->toJSONResponse($data);
        } else {
            return $this->toJSONResponse($userId, 'fail', 'userId is not valid');
        }
    }


    /**
     * @return \Cake\Http\Response
     * @version 1.0.0
     */
    public function login()
    {

        /**
         * check user is loggin
         */

        /*
        if ($this->_checkUserIsLoggin()) {
            $this->redirect('/users/getUserInfo');
        }
        */

        /**
         * check Request Method is must POST
         */
        if (!$this->request->is('post')) {
            return $this->toJsonResponse(null, 'fail', 'Request Method Is Not Allowed');
        }

        /**
         * checking Identity fields is valid
         */
        $usertoken = $this->request->getData('unique_id_enc');

        if ($usertoken === null) {
            return $this->toJsonResponse(null, 'fail', 'Your username or password was incorrect');
        }

        /**
         * for test purpose
         */
        $ip = $this->request->clientIp();
        $encryption = new NoaEncryption;
        if (in_array($ip, ['127.0.0.1', '::1']) && $this->isUUID($usertoken)) {
            $usertoken = $encryption->encrypt($usertoken);
        }
        $uniqueId = $encryption->decrypt($usertoken);

        /**
         * check token is valid
         */

        if (!$this->isUUID($uniqueId)) {
            return $this->toJsonResponse(null, 'fail', 'unique_id_enc not exist');
        }

        /**
         * check unique_id is exist?
         */

        $userFound = $this->Auth->userExistByUniqueID($uniqueId);

        /**
         * if user not exist, register a new user
         */
        if ($userFound === null) {
            $userFound = $this->Auth->userRegisterByUniqueID($uniqueId);
        }

        if (!empty($userFound)) {
            $this->Authentication->setIdentity($userFound);
            $this->Auth->setLastLoginIn($userFound->id);
            return $this->_getUserInfo($userFound->id);

        }
        return $this->toJsonResponse(null, 'fail', 'response not set! why?!');
    }


    /**
     * @param $phoneNumber
     * @return bool
     * @version 1.0.0
     */
    private function isPhoneNumber($phoneNumber)
    {
        return (bool)preg_match('/^(((98)|(\+98)|(0098)|0)(9){1}[0-9]{9})+$/', $phoneNumber) || (bool)preg_match('/^(9){1}[0-9]{9}+$/', $phoneNumber);
    }

    /**
     * @param $string
     * @return bool
     */
    private function isUUID($string)
    {
        $countChrs = strlen($string);
        if ($countChrs == 32 || $countChrs == 36) {
            return true;
        }
        return false;
    }

    /**
     * @version 1.0.0
     */
    public function sendVerificationCode()
    {

        $userId = $this->userInfo['userId'];
        $phoneNumber = $this->request->getData('phoneNumber');
        $time = time();


        if (!$this->isPhoneNumber($phoneNumber)) {
            return $this->toJsonResponse(null, 'fail', 'phone number is not valid');
        }

        $code1 = rand(10, 20);
        $code2 = $code1 * rand(1, 3);
        $finalCode = "$code1" . "$code2"; //human readable code

        $this->loadModel('Verificationcodes');
        $codeSendBeforeCount = $this->Verificationcodes->find()
            ->where(['user_id' => $userId, 'time >' => $time - 600])->count();

        if ($codeSendBeforeCount >= 3) {
            return $this->toJsonResponse(null, 'fail', 'too many verification code sent');
        }

        $codeSendBefore = $this->Verificationcodes->find()
            ->where(['user_id' => $userId, 'phone_number' => $phoneNumber, 'time >' => $time - 300])->first();

        if ($codeSendBefore !== null) {
            $finalCode = $codeSendBefore['code']; //better UX
        }

        $data = $this->Verificationcodes->newEntity([
            'user_id' => $userId,
            'phone_number' => $phoneNumber,
            'code' => $finalCode,
            'time' => $time
        ]);

        if ($codeSendBefore == NULL || $codeSendBefore['time'] < $time - 100) {
            $this->Verificationcodes->saveOrFail($data);
        }

        $Kavenegar = new Kavenegar;
        $Kavenegar->send($phoneNumber, $finalCode, 'Verify');

        return $this->toJsonResponse(null, 'success', 'Done', $this->addUserinfo());
    }


    /**
     * @version 1.0.0
     */
    public function verifyCode()
    {
        $userId = $this->userInfo['userId'];
        $code = $this->request->getData('code');
        $uniqueIdSms = $this->request->getData('uniqueIdSms');
        $time = time();
        $this->loadModel('Verificationcodes');
        $verificationInfo = $this->Verificationcodes->find()
            ->where([
                'user_id' => $userId,
                'time >' => $time - 300])
            ->order(['id DESC'])
            ->first();

        if ($verificationInfo === null) {
            return $this->toJsonResponse(null, 'fail', 'Verification code is not valid');
        }

        $verificationCode = $verificationInfo['code'];
        $failTry = $verificationInfo['fail_try'];

        if ($verificationCode != $code || $failTry > 5) {
            $this->Verificationcodes->incrementFailTry($verificationInfo['id']);
            return $this->toJsonResponse(null, 'fail', 'Verification code expired');
        }

        $userPhone = $this->Auth->userExistByPhone($verificationInfo['phone_number']);

        if ($userPhone === null) {
            $this->Auth->userLinkToPhone($userId, $verificationInfo['phone_number'], $uniqueIdSms);
            return $this->toJsonResponse(['isNewUniqueId' => true], 'success', 'Done', $this->addUserinfo());
        }

        return $this->toJsonResponse(['isNewUniqueId' => false], 'success', 'Done', $this->addUserinfo());
    }


    /**
     * @version 1.0.0
     */
    public function mobileSmsLogin()
    {
        /**
         * check user is loggin
         */
        if ($this->_checkUserIsLoggin()) {
            return $this->redirect(['controller' => 'Auth', 'action' => 'loggedIn'], 308);
        }

        /**
         * check Request Method is must POST
         */
        if (!$this->request->is('post')) {
            return $this->toJsonResponse(null, 'fail', 'Request Method Is Not Allowed');
        }


        $phoneNumber = $this->request->getData('phoneNumber');
        $uniqueIdSms = $this->request->getData('uniqueIdSms');

        if (!$this->isPhoneNumber($phoneNumber)) {
            return $this->toJsonResponse(null, 'fail', 'phone number is not valid');
        }

        $userFound = $this->Auth->userExistByPhone($phoneNumber);
        if ($userFound === null) {
            return $this->toJsonResponse(null, 'fail', 'phone number is not exist');
        }

        if ($userFound->unique_id_sms !== $uniqueIdSms) {
            return $this->toJsonResponse(null, 'fail', 'phone number or uniqueIdSms is not valid');
        }

        //phone number registered user exist so log in  (just when we wanna login directly by phone number)
        $this->Authentication->setIdentity($userFound);
        $this->Auth->setLastLoginIn($userFound->id);
        return $this->_getUserInfo($userFound->id);
    }


    /**
     * @version 1.0.0
     */
    public function googleConnect()
    {

        $id_token = $this->request->getData('id_token');
        $userId = $this->userInfo['userId'];

        if ($id_token === null) {
            return $this->toJsonResponse(null, 'fail', 'id_token not set');
        }

        $ip = $this->request->clientIp();

        //todo: in bayad bad az test delete shavad
        if (in_array($ip, ['127.0.0.1', '::1'])) {
            $payload['email'] = 'saleh.souzanchi@gmail.com';
            $payload['email_verified'] = true;
        } else {
            // Specify the CLIENT_ID of the app that accesses the backend
            $client = new Google_Client(['client_id' => $this->GOOGLE_CLIENT_ID]);
            $payload = $client->verifyIdToken($id_token);
        }

        if (!$payload) {
            return $this->toJsonResponse(null, 'fail', 'Invalid ID token');
        }

        $email = $payload['email'];

        if (!$email && $payload['email_verified'] != 'true') {
            return $this->toJsonResponse(null, 'fail', 'google server could not be reached');
        }

        $foundUserByEmail = $this->Auth->userExistByEmail($email);

        if ($foundUserByEmail === null) {
            $this->Auth->userLinkToEmail($userId, $email);
            return $this->toJsonResponse(['isNewConnect' => true], 'success', 'Done');
        }

        return $this->toJsonResponse(['isNewConnect' => false], 'success', 'Done');
    }

    /**
     * $id_token = "eyJhbGciOiJSUzI1NiIsImtpZCI6Ijg0NmRkYWY4OGNjMTkyMzUyMjFjOGVlMGRmYTNiYjQ2YWUyN2JmZmMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI2ODM1MDA5NTY2MzMtZDdnbmF2b3RiMTdudDN0M2dzOGFjZm0wcjAwMjZmMnUuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI2ODM1MDA5NTY2MzMtY2NwMG82YnRkNXVlbW9mY2t2OW4yc2NjamsxMWhtdDAuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDU3ODQ2MjEzMjEwOTk4NDE0MjEiLCJlbWFpbCI6Im5haW1pLmhhbWlkQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoiSGFtaWQgTmFlaW1pIiwicGljdHVyZSI6Imh0dHBzOi8vbGg1Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tSFJ2WER6Z0QwaEUvQUFBQUFBQUFBQUkvQUFBQUFBQUFBRXcvM0FPRXhsTFJnSUUvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6IkhhbWlkIiwiZmFtaWx5X25hbWUiOiJOYWVpbWkiLCJsb2NhbGUiOiJlbiIsImlhdCI6MTU2MDI1OTYyNywiZXhwIjoxNTYwMjYzMjI3fQ.WAWjs9ZKO4iJNwwj6UkwxrzT-goUpM7QbjsvF-NG9RVp-aMr_IpWqYiu1JIXdei7V8-6ytvT1x2LVDPtT64-772NRSLaShEZPQzVlgQj1FvDxXq6E_1fqW_XtDiCnWeKIzGR1iLeniXDzBInN8IE6tUp-5DbwSCbVTQiUxEj3xj9KlWyNt5RG_wHSMPhvdxyMmCiMAXKCQUH73rFWtvMwsXmF3zx5mHBIO0PiO0s0DMOe97xKsiEHPkdHuH--z4DaFRK5nI0OC6ThfT6SBeLGuWvVvA5ad04VK3ziFCfVgwf0j3GK-Cjr2ik0Pbb6kKfQAB6Io7kqnwJT6WYAw7_kQ";
     * @version 1.0.0
     */
    public function googleLogin()
    {
        /**
         * check user is loggin
         */
        if ($this->_checkUserIsLoggin()) {
            return $this->redirect(['controller' => 'Auth', 'action' => 'loggedIn'], 308);

        }

        /**
         * check Request Method is must POST
         */
        if (!$this->request->is('post')) {
            return $this->toJsonResponse(null, 'fail', 'Request Method Is Not Allowed');
        }


        $id_token = $this->request->getData('id_token');

        if ($id_token === null) {
            return $this->toJsonResponse(null, 'fail', 'id_token not set');
        }

        // Specify the CLIENT_ID of the app that accesses the backend
        $client = new Google_Client(['client_id' => $this->GOOGLE_CLIENT_ID]);
        //$payload = $client->verifyIdToken($id_token);

        $payload['email'] = 'saleh.souzanchi@gmail.com';

        if (!$payload) {
            return $this->toJsonResponse(null, 'fail', 'Invalid ID token');
        }

        $email = $payload['email'];


        if (!$email && $payload['email_verified'] != 'true') {
            return $this->toJsonResponse(null, 'fail', 'google server could not be reached');
        }

        //todo benazar ezafe miad ,check shavad
//        $this->request->data['User']['gmail_account'] = $email;
//        $this->request->data['User']['valid'] = true;
//        if (isset($this->request->data['User']['gmail_account']) || isset($this->request->data['User']['unique_id'])) {
//            $this->_login($email);
//        } else {
//
//            $response = array(
//                'status' => "fail",
//                'data' => NULL,
//                'message' => __('no username password or unique_id')
//            );
//            $this->set('response', $response);
//        }


        $userFound = $this->Auth->userExistByEmail($email);
        if ($userFound === null) {
            return $this->toJsonResponse(['found' => false], 'success', 'There is not any account with this email');
        }

        $this->Authentication->setIdentity($userFound);
        $this->Auth->setLastLoginIn($userFound->id);
        return $this->_getUserInfo($userFound->id);

    }

    /**
     *
     */
    public function test()
    {
        $this->_testNoaEncryption();
        //  return $this->_registerUser('12311aaa11111111');
        // dd( $this->Auth->userExistByEmail('zshmah616@gmail.com'));


        //      return $this->testPostAction($this, 'login', [
//            'usertoken' => 'ca98ac703180b05496134eb898e03c9f'
//        ]);

    }

    public function _testNoaEncryption()
    {
        $encryption = new NoaEncryption;
        $originUniqueId = "3faf4298306daf8749ff82eab7c7852f";

        pr($this->isUUID($originUniqueId));
        $originUniqueIdEncrypt = $encryption->encrypt($originUniqueId);
        pr($this->isUUID($originUniqueIdEncrypt));
        pr([
            'id' => $originUniqueId,
            'encrypted' => $originUniqueIdEncrypt
        ]);

        pr([
            'encrypted string' => $originUniqueIdEncrypt,
            'decrypt string' => $encryption->decrypt($originUniqueIdEncrypt)
        ]);

    }

}
