<?php

namespace App\Controller;

use App\Model\Table\ServersTable;
use Cake\Event\Event;

/**
 * Groups Controller
 *
 * @property \App\Model\Table\ServersTable $Servers
 */
class ServersController extends AppController
{

    protected $ip;

    /**
     * @param Event $event
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['serverInfo','test']);
        $this->ip = ServersTable::getServerIP();
    }


    /**
     * @version 1.0.0
     */
    public function alive()
    {
        $port = $this->request->getData("port");
        $networkVersion = $this->request->getData("networkVersion");
        @$backup = $this->request->getData("backup");
        @$test = $this->request->getData("test");
            @$pid = $this->request->getData("pid");
        @$start = filter_var($this->request->getData("start"), FILTER_VALIDATE_BOOLEAN); //boolean (true or false)
        $allowIps = array('194.5.175.143', '2a01:4f8:121:2303::2', '178.63.72.206', '185.97.116.175', '185.112.33.107', '185.112.33.109');

        if (!isset($backup))
            $backup = 0;

        if (!isset($start))
            $start = FALSE;

        if (!isset($pid))
            $pid = 0;

        if (!isset($test))
            $test = 0;

        if (!in_array($this->ip, $allowIps)) {
            return $this->toJsonResponse(null, 'fail', 'This ip is not allowed');
        }

        $this->Servers->updateServer($this->ip, $port, $networkVersion, $backup, $start, $pid, $test);
        return $this->toJsonResponse();
    }


    /**
     * @version 1.0.0
     */
    function multiplayerIp()
    {
        $networkVersion = $this->request->getData('networkVersion');
        $aliveTime = time() - 60;
        $userId = $this->userInfo['userId'];
        $gameServer = $this->Servers->findServer($userId, $networkVersion, $aliveTime);

        if (!$gameServer) {
            return $this->toJsonResponse(null, 'fail', 'Maintenance mode');
        }

        /*        if($userId==106 || $userId==29){

                    $server['iran']=array(
                        array(
                            'ip'=>'2a01:4f8:121:2303::2',
                            'port'=>'7009'
                        ),
                    );
                    $response = array(
                        'status' => "success",
                        'data' =>$server,
                        'message' => __('Done')
                    );
                    echo json_encode($response);

                    return;
                }*/


        $server = array();

        $server['iran'] = array(
            $gameServer
        );

        $server['int'] = array(
            $gameServer
        );

        return $this->toJsonResponse($server);
    }

    /**
     * @version 1.0.0
     */
    public function serverRemoteConfig()
    {
        $config = [
            'lobbyWaitTime' => 54,
            'tagetFPS' => 30,
            'serverTickRate' => 15,
            'maxConnection' => 16000,
        ];
        return $this->toJsonResponse($config);
    }

    /**
     * @version 1.0.0
     */
    public function serverInfo()
    {
        $ip = $this->request->getData('ip');

        if (!isset($ip)) {
            $ip = $this->ip;
        }
        $condition = array();
        if ($ip != 0) {
            $condition = array(
                'ip' => $ip
            );
        }
        //if $ip=0 the condition will remove
        $gameServers = $this->Servers->find()->where($condition)->all()->toArray();

        $createNewServer = False;
        foreach ($gameServers as $key => $gameServer) {

            $gameServerId = $gameServer['id'];
            $howManyMatchIsRunOnServer = $this->Servers->Matches->howManyMatchIsRun($gameServerId);
            $howManyUserOnServer = $this->Servers->Matches->Usermatches->howManyUserOnServer($gameServerId);
            $gameServers[$key]['howManyMatchIsRun'] = $howManyMatchIsRunOnServer;
            $gameServers[$key]['howManyUser'] = $howManyUserOnServer;

            if ($howManyMatchIsRunOnServer <= 2) {
                $createNewServer = TRUE;
            }

        }
        return $this->toJsonResponse(['gameServers' => $gameServers, 'createNewServer' => $createNewServer]);
    }


    /**
     * @version 1.0.0
     */
    public function editActiveServer()
    {
        $delete = filter_var($this->request->getData("delete"), FILTER_VALIDATE_BOOLEAN); //boolean (true or false)
        $active = $this->request->getData("active");
        $gameServerId = $this->request->getData("gameServerId");

        if ($delete) {
            $this->Servers->deleteAll(['id' => $gameServerId]);
        } else {
            $this->Servers->updateAll(['active' => $active], ['id' => $gameServerId]);
        }
        return $this->toJsonResponse();
    }

    public function test()
    {

       // $res = $this->Servers->findServer(14,18,time());
        $res = $this->Servers->latestServerNetworkVersion();
        dd($res);

        return $this->testPostAction($this, 'multiplayerIp', [
            'networkVersion' => 18,

        ]);

        //Cake\Http\ServerRequest::clientIp()
        $res = ServersTable::getServerIP();
        // $res = $this->Servers->_checkServer();
        //$res = $this->Servers->serverInfo('185.112.33.109',7023,13)->id;
        // debug($res);
        //return $this->toJsonResponse(null);

        //return $this->testPostAction($this, 'serverInfo', []);
    }
}
