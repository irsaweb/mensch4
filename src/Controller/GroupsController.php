<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * @property \App\Model\Table\GroupsTable $Groups
 */
class GroupsController extends AppController {

    public $actsAs = array('Acl' => array('type' => 'requester'));

    public function parentNode() {
        return null;
    }
    public function beforeFilter(\Cake\Event\EventInterface $event) {
        parent::beforeFilter($event);

        // For CakePHP 2.1 and up
        $this->Auth->allow();
    }


}
