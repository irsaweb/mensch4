<?php

namespace App\Controller;

/**
 * @property \App\Model\Table\ReportsTable $Reports
 */
class ReportsController extends AppController
{


    /**
     * @version  1.0.0
     */
    public function submitReport()
    {

        $userId = $this->userInfo['userId'];
        $issue = $this->request->getData("issue");
        $content = $this->request->getData("content");
        $reportedUserId = $this->request->getData("reportedUserId");
        $reportedClanId = $this->request->getData("reportedClanId");

        if ($userId < 10) {  //means not bots
            return $this->toJsonResponse(null, 'fail', 'Bots can not submit report');
        }

        if ($this->Reports->submittedBefore($userId, $reportedUserId, $issue)) {
            return $this->toJsonResponse(null, 'fail', 'You reported this user before');
        }

        $data = array(
            'user_id' => $userId,
            'reported_user_id' => $reportedUserId,
            'reported_clan_id' => $reportedClanId,
            'issue' => $issue,
            'content' => $content,
            'time' => time()
        );
        if ($content == '' && $issue == 'inGameChat') $data['state'] = 1;  //if reported somebody wrong we dont count it

        $report = $this->Reports->newEntity($data);
        $this->Reports->saveOrFail($report);
        return $this->toJsonResponse();

    }

    public function test()
    {

        //$res = $this->Reports->submittedBefore(89753,59283,'name');
        //  $res = $this->Reports->muteReportedUser();

//        return $this->testPostAction($this, 'submitReport', [
//            'issue' => 'clan-saleh',
//            'content' => 2,
//            'reportedUserId' => 144,
//            'reportedClanId' => 122
//        ]);

    }


}
