<?php

namespace App\Controller;

use App\Controller\Component\XmppComponent;
use Cake\Event\EventInterface;

/**
 * @property \App\Model\Table\UserclansTable $Userclans
 * @property \App\Controller\Component\XmppComponent $Xmpp
 */
class UserclansController extends AppController
{


    /**
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Xmpp');
    }

    /**
     * @param EventInterface $event
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['joinClanFix', 'test']);
    }

    /**
     * getUserclans
     * @version 1.0.0
     */
    public function getUserclans()
    {
        $clanId = $this->request->getData("clanId");
        $userClans = $this->Userclans->getUserClan($clanId);
        return $this->toJsonResponse($userClans);
    }

    /**
     * @version 1.0.0
     */
    public function myClan()
    {
        $userId = $this->userInfo['userId'];
        $prefix = XmppComponent::$prefix;
        $clanInfo = $this->Userclans->getClanByUser($userId);

        if ($clanInfo) {
            $clanInfo['username'] = $prefix . $userId;
            $clanInfo['server'] = "mensch";
            $clanInfo['host'] = "185.112.33.109";
            $clanInfo['port'] = 5222;
            $clanInfo['room'] = $prefix . $clanInfo['id'];
            $clanInfo['ClanMembers'] = $this->Userclans->getUserClan($clanInfo['id']);
        } else {
            $clanInfo = null;
        }
        return $this->toJsonResponse($clanInfo);
    }


    /**
     * @version 1.0.0
     */
    public function joinClan()
    {
        $userId = $this->userInfo['userId'];
        $clanId = $this->request->getData('clanId');
        $prefix = XmppComponent::$prefix;
        $addToClan = $this->Userclans->addUserClan($userId, $clanId, 'user');

        if (!$addToClan[0]) {
            if ($addToClan[1] == 'kicked') {
                $failMessage = __('kicked');
            } elseif ($addToClan[1] == 'full') {
                $failMessage = __('clan is full');
            } else {
                $failMessage = $addToClan[1];
            }
            return $this->toJsonResponse(null, 'fail', $failMessage);
        }

        $clanInfo = $this->Userclans->getClanByUser($userId);

        $clanId = $clanInfo['id'];
        $password = $clanInfo['pass'];

        $clanInfo['username'] = $prefix . $userId;
        $clanInfo['server'] = "mensch";
        $clanInfo['host'] = "185.112.33.109";
        $clanInfo['port'] = 5222;
        $clanInfo['room'] = $prefix . $clanId;
        $clanInfo['ClanMembers'] = $this->Userclans->getUserClan($clanId);

        $this->Xmpp->registerUser($prefix . $userId, $password,
            function ($result) use ($clanId, $userId, $prefix) {
                if ($result) {
                    $this->Xmpp->create_clan($prefix . $clanId, function ($result2) use ($clanId, $userId) {
                    });
                } else {
                    //   $res = array('error'=>1);
                }
            }
        );
        return $this->toJsonResponse($clanInfo);
    }

    /**
     * @version 1.0.0
     */
    public function kickUserclan()
    {
        $userId = $this->userInfo['userId'];
        $kickUserId = $this->request->getData('kickUserId');

        if ($this->Userclans->kickUserClan($userId, $kickUserId)) {
            return $this->toJsonResponse();
        } else {
            return $this->toJsonResponse(null, 'fail', 'rollback');
        }
    }


    /**
     * @version 1.0.0
     */
    public function changeRoleUserclan()
    {
        $userId = $this->userInfo['userId'];
        $role = $this->request->getData('role');
        $promoteUserId = $this->request->getData('promoteUserId');

        if (!in_array($role, array('superadmin', 'admin', 'manager', 'user'))) {
            return $this->toJsonResponse(null, 'fail', "this $role is not valid");
        }
        $res = $this->Userclans->changeUserclanRole($userId, $promoteUserId, $role);

        if (!$res) {
            return $this->toJsonResponse(null, 'fail', "rollback");
        }

        return $this->toJsonResponse();
    }


    /**
     * @version 1.0.0
     */
    public function leaveClan()
    {
        $userId = $this->userInfo['userId'];
        $clanId = $this->request->getData('clanId');
        $res = $this->Userclans->leaveUserClan($userId, $clanId);
        //todo(hamid): این بخش را من اضافه کردم
        if ($res) {
            return $this->toJsonResponse(null, 'success', 'You left the clan');
        }
        return $this->toJsonResponse(null, 'fail', "rollback");
    }


    /**
     * @version 1.0.0
     */
    public function removeFormKicklist()
    {
        $userId = $this->userInfo['userId'];
        $kickUserId = $this->request->getData("kickUserId");
        if ($this->Userclans->removeFormKicklist($userId, $kickUserId)) {
            return $this->toJsonResponse();
        }
        return $this->toJsonResponse(null, fail, 'rollback');
    }


    /**
     * @version 1.0.0
     */
    public function joinClanFix()
    {
        ini_set('memory_limit', '2048M');
        $userClans = $this->Userclans->find()
            ->where(['repair' => 0])
            ->limit(30)
            ->all();

        foreach ($userClans as $userClan) {

            $userClanID = $userClan['id'];
            $userId = $userClan['user_id'];
            $prefix = XmppComponent::$prefix;

            $clanInfo = $this->Userclans->getClanByUser($userId);

            $clanId = $clanInfo['id'];

            $password = $clanInfo['pass'];
            $clanInfo['username'] = $prefix . $userId;
            $clanInfo['server'] = "mensch";
            $clanInfo['host'] = "185.112.33.109";
            $clanInfo['port'] = 5222;
            $clanInfo['room'] = $prefix . $clanId;
            $clanInfo['ClanMembers'] = $this->Userclans->getUserClan($clanId);


            $this->Xmpp->registerUser($prefix . $userId, $password,
                function ($result) use ($clanId, $userId, $prefix) {
                    if ($result) {
                        $this->Xmpp->create_clan($prefix . $clanId, function ($result2) use ($clanId, $userId) {
                        });
                    } else {
                        //$res = array('error'=>1);
                    }
                }
            );

            $this->Userclans->updateAll(['repair' => 1], ['id' => $userClanID]);
            return $this->toJsonResponse($userId);
        }
    }


    public function test()
    {
        //return $this->testPostAction($this, 'kickUserclan', ['kickUserId' => '25']);

        debug($this->Userclans->getClanByUser(88));

        //   $res = $this->joinClanFix();

        // $res = $this->Userclans->_checkLeader(42);

        /*        $this->request = $this->request->withParsedBody([
                    'clanId' => 67,
                ]);
                $this->Userclans->deleteAll(['user_id' => 14]);
                $res = $this->joinClan();*/


        //$res = $this->Userclans->addUserClan(14, 67, 'user');
        //$res = $this->myClan();
        //$res = $this->Userclans->getUserRoll(1417, 23);
        //$res = $this->Userclans->getClan(1400554);
        //  $res = $this->Userclans->getUserClan(42);

        debug($res);
        // return $res;
    }

}
