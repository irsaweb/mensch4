<?php
namespace App\Controller\Component\Auth;

use App\Controller\Component\Auth\BaseAuthenticate;

class OpenidAuthenticate extends BaseAuthenticate {
    public function authenticate(Request $request, Response $response) {
        $userModel = $this->settings['userModel'];
        list(, $model) = pluginSplit($userModel);

        $fields = $this->settings['fields'];

        if(!isset($request->data[$model]['valid'])){   // prevent only username authentication from the secound method.
            return false;
        }

        if (!$this->_checkFields($request, $model, $fields)) {
            return false;
        }
        return $this->_myFindUser(
            $request->data[$model][$fields['username']]
        );
    }

    protected function _myFindUser($email) {
        $userModel = $this->settings['userModel'];
        list(, $model) = pluginSplit($userModel);
        $fields = $this->settings['fields'];

        $conditions = array(
            $model . '.' . $fields['emailAccount'] => $email
        );

        if (!empty($this->settings['scope'])) {
            $conditions = array_merge($conditions, $this->settings['scope']);
        }
        $user=TableRegistry::get($userModel);
        $user->addBehavior('Containable');
        $result = $user->find('first', array(
            'conditions' => $conditions,
            'recursive' => $this->settings['recursive'],
            'contain' => $this->settings['contain'],
        ));

        if (!empty($result[$model])) {
            $userObj = $result[$model];
            unset($result[$model]);
            return array_merge($userObj, $result);
        }else{
            $conditions = array(
                $model . '.' . $fields['username'] => $email
            );
            $userFind = $user->find('first', array(
                'conditions' => $conditions,
                'recursive' => $this->settings['recursive'],
                'contain' => $this->settings['contain'],
            ));
            if (!empty($userFind[$model])) {
                $user->unbindModelAll();
                $user->updateAll(array($userModel. '.' .$fields['emailAccount'] => "'$email'"),array($userModel. '.' .'id'=>$userFind[$model]['id']));

                $userObj = $userFind[$model];
                unset($userFind[$model]);
                return array_merge($userObj, $userFind);
            }else{
                return false;
            }
        }
    }

    protected function _checkFields(Request $request, $model, $fields) {
        if (empty($request->data[$model])) {
            return false;
        }
        foreach (array($fields['username']) as $field) {
            $value = $request->data($model . '.' . $field);
            if (empty($value) && $value !== '0' || !is_string($value)) {
                return false;
            }
        }
        return true;
    }
}

