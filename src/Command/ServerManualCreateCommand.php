<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

/**
 * ServerManualCreate command.
 * @property \App\Model\Table\ServersTable $Servers
 */
class ServerManualCreateCommand extends Command
{
    /**
     * @var string
     */
    protected $modelClass = 'Servers';

    /**
     * @return string
     */
    public static function defaultName(): string
    {
        return 'server manual_create';
    }

    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/4/en/console-commands/commands.html#defining-arguments-and-options
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser = parent::buildOptionParser($parser);
        $parser->addArgument('ip', [
            'help' => 'server ip',
            'required' => true
        ]);
        $parser->addArgument('port', [
            'help' => 'server port number',
            'required' => true
        ]);

        $parser->addArgument('network', [
            'help' => 'server network version',
            'required' => true
        ]);

        return $parser;
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|void|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        ini_set('memory_limit', '2048M');
        set_time_limit(200);
        $serverIp = $args->getArgument('ip');
        $port = $args->getArgument('port');
        $networkVersion = $args->getArgument('network');

        $startTime = time();
        shell_exec("nohup /var/www/clients/client0/web1/private/mensch/server/$networkVersion/gameserver -ip $serverIp -port $port  > /var/log/unity/server$port.log &");

        $time = time() - $startTime;
        $io->out("time=$time" . "s");
    }
}
