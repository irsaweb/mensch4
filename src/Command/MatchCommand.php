<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

/**
 * Match command.
 * @property \App\Model\Table\MatchesTable&\Cake\ORM\Association\BelongsTo $Matches
 */
class MatchCommand extends Command
{

    protected $modelClass = 'Matches';


    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/4/en/console-commands/commands.html#defining-arguments-and-options
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser = parent::buildOptionParser($parser);
        $parser->addArgument('matchFix', [
            'help' => 'this is match fix'
        ]);
        $parser->addArgument('userMatchFix', [
            'help' => 'and this is user match fix'
        ]);
        $parser->addArgument('closeExpiredFriendlyMatch', [
            'help' => 'and this is user match fix'
        ]);

        return $parser;
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|void|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $name = $args->getArgumentAt(0);

        if ($name !== null && method_exists($this, $name)) {
            $startTime = time();
            $this->{$name}();
            $time = time() - $startTime;
            $io->out("time=$time" . "s");
        } else {
            $io->out("This {$name} is not valid.");
        }

    }


    /**
     *
     */
    public function matchFix()
    {
        ini_set('memory_limit', '2048M');
        set_time_limit(200);
        $this->Matches->matchFix();
    }


    /**
     *
     */
    public function userMatchFix()
    {
        ini_set('memory_limit', '2048M');
        set_time_limit(200);
        $this->Matches->Usermatches->userMatchFix();
    }


    /**
     *
     */
    public function closeExpiredFriendlyMatch()
    {
        ini_set('memory_limit', '2048M');
        set_time_limit(200);
        $this->Matches->Friendlymatches->closeExpiredFriendlyMatch();
    }
}
