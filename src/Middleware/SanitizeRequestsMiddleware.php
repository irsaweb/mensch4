<?php
declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * SanitizeRequests middleware
 */
class SanitizeRequestsMiddleware implements MiddlewareInterface
{


    /**
     * Process method.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request.
     * @param \Psr\Http\Server\RequestHandlerInterface $handler The request handler.
     * @return \Psr\Http\Message\ResponseInterface A response.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {

        $QueryParams = $request->getQueryParams();

        if (!empty($QueryParams)) {
            $QueryParams = $this->clean($QueryParams);
        }
        $request = $request->withQueryParams($QueryParams);

        $requestData = $request->getParsedBody();
        if (!empty($requestData)) {
            $requestData = $this->clean($requestData);
        }
        $request = $request->withParsedBody($requestData);

        return $handler->handle($request);
    }


    /**
     * @param $data
     * @return array|string|string[]
     */
    public static function clean($data)
    {
        if (empty($data)) {
            return $data;
        }

        if (is_array($data)) {
            foreach ($data as $key => $val) {
                $data[$key] = self::clean($val);
            }
            return $data;
        }
        $data = self::StringInputCleaner($data);
        $data = self::html($data);
        $data = self::mysqlCleaner($data);
        $data = str_replace("\\\$", "$", $data);
        return $data;
    }


    /**
     * @param $string
     * @return string
     */
    public static function html($string)
    {
        static $defaultCharset = 'UTF-8';;
        $options = array(
            'charset' => $defaultCharset,
            'quotes' => ENT_QUOTES,
            'double' => true
        );

        $string = strip_tags($string);

        return htmlentities($string, $options['quotes'], $options['charset'], $options['double']);
    }

    /**
     * @param $data
     * @return mixed
     */
    public static function StringInputCleaner($data)
    {
        //remove space bfore and after
        $data = trim($data);
        //remove slashes
        $data = stripslashes($data);
        $data = filter_var($data, FILTER_SANITIZE_STRING);
        return $data;
    }

    /**
     * @param $value
     * @return string
     */
    public static function mysqlCleaner($value)
    {
        $search = array("\\", "\x00", "\n", "\r", "'", '"', "\x1a");
        $replace = array("\\\\", "\\0", "\\n", "\\r", "\'", '\"', "\\Z");

        return stripslashes(str_replace($search, $replace, $value));
    }


}
