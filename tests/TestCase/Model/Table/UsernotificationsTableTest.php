<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsernotificationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsernotificationsTable Test Case
 */
class UsernotificationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UsernotificationsTable
     */
    public $Usernotifications;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Usernotifications',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Usernotifications') ? [] : ['className' => UsernotificationsTable::class];
        $this->Usernotifications = TableRegistry::getTableLocator()->get('Usernotifications', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Usernotifications);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
