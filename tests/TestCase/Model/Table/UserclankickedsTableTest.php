<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserclankickedsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserclankickedsTable Test Case
 */
class UserclankickedsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserclankickedsTable
     */
    public $Userclankickeds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Userclankickeds',
        'app.Users',
        'app.Clans',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Userclankickeds') ? [] : ['className' => UserclankickedsTable::class];
        $this->Userclankickeds = TableRegistry::getTableLocator()->get('Userclankickeds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Userclankickeds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
