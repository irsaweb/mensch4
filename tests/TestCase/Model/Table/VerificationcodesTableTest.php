<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VerificationcodesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VerificationcodesTable Test Case
 */
class VerificationcodesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\VerificationcodesTable
     */
    protected $Verificationcodes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Verificationcodes',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Verificationcodes') ? [] : ['className' => VerificationcodesTable::class];
        $this->Verificationcodes = $this->getTableLocator()->get('Verificationcodes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Verificationcodes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
