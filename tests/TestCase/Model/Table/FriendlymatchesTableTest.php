<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FriendlymatchesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FriendlymatchesTable Test Case
 */
class FriendlymatchesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FriendlymatchesTable
     */
    public $Friendlymatches;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Friendlymatches',
        'app.Matches',
        'app.Owners',
        'app.Clans',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Friendlymatches') ? [] : ['className' => FriendlymatchesTable::class];
        $this->Friendlymatches = TableRegistry::getTableLocator()->get('Friendlymatches', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Friendlymatches);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
