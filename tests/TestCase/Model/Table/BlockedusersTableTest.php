<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BlockedusersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BlockedusersTable Test Case
 */
class BlockedusersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BlockedusersTable
     */
    public $Blockedusers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Blockedusers',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Blockedusers') ? [] : ['className' => BlockedusersTable::class];
        $this->Blockedusers = TableRegistry::getTableLocator()->get('Blockedusers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Blockedusers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
