<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsermatchesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsermatchesTable Test Case
 */
class UsermatchesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UsermatchesTable
     */
    public $Usermatches;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Usermatches',
        'app.Matches',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Usermatches') ? [] : ['className' => UsermatchesTable::class];
        $this->Usermatches = TableRegistry::getTableLocator()->get('Usermatches', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Usermatches);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
