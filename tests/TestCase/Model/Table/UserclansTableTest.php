<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserclansTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserclansTable Test Case
 */
class UserclansTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserclansTable
     */
    public $Userclans;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Userclans',
        'app.Users',
        'app.Clans',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Userclans') ? [] : ['className' => UserclansTable::class];
        $this->Userclans = TableRegistry::getTableLocator()->get('Userclans', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Userclans);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
