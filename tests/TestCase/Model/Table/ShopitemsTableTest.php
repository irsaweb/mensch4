<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ShopitemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ShopitemsTable Test Case
 */
class ShopitemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ShopitemsTable
     */
    public $Shopitems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Shopitems',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Shopitems') ? [] : ['className' => ShopitemsTable::class];
        $this->Shopitems = TableRegistry::getTableLocator()->get('Shopitems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Shopitems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
