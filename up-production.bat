echo "### CODE ###";
git pull

composer upgrade --prefer-dist --no-dev --optimize-autoloader --no-interaction

echo "### CLEANUP ###";
bin/cake clear cache

echo "### CACHE WARMING ###;
bin/cake schema_cache build
