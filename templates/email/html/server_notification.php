<?php

if(!isset($_SERVER['SERVER_NAME'])){
    $_SERVER['SERVER_NAME']='www.noagames.ir';
}

if ($_SERVER['SERVER_NAME'] == 'localhost') {
    $baseurl       = "http://{$_SERVER['SERVER_NAME']}/mensch/";
    $serverurl     = "http://{$_SERVER['SERVER_NAME']}/mensch/";
}else{
    $baseurl       = "http://{$_SERVER['SERVER_NAME']}/mensch/";
    $serverurl     = "http://{$_SERVER['SERVER_NAME']}/mensch/";
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<div id="main" style="padding: 0; margin:0; background-color: #e1e1e1;  font-family: B Yekan;">
    <div class="header" style="width:100%; height:80px; background-color:#0e2006; text-align: center;">
        <img class="logo" src="<?php echo $baseurl ?>img/template/menchico-logo.png" style="margin-top: 5px; height: 85px;">
    </div>
    <div class="page" style="position: relative; max-width:600px; padding:0 8px; margin:0 auto;">
        <div class="box" style="width:100%; background-color:#bec3c7; margin: 10px auto;">
            <div class="box-title" style="font-size: 14pt; height: 30px; line-height: 30px; color: white; background-color: #0e90d2; padding: 0 16px;"><?php echo __("Important"); ?></div>
            <div class="box-content" style="font-size: 12pt; padding: 8px 16px;">
                <p class="content">
                    <?php echo __("Take some actions"); ?>
                </p>
            </div>
        </div>
        <div class="btn-containar" style="margin: 50px auto; text-align: center;">
            <p class="content">
                Server is on backup
                <br/>
                Id: <?php echo $serverId ?>
                <br/>
                Is Backup: <?php echo $isBackup ?>
                <br/>
                Pid: <?php echo $serverPid ?>
                <br/>
                Networkversion: <?php echo $networkVersion ?>
                <br/>
                Ip: <?php echo $serverIp ?>
                <br/>
                Match Count On Server: <?php echo $howManyMatchIsRun ?>

            </p>
        </div>

    </div>
    <div class="footer" style="position: relative; height: 70px; margin-top: 10px; text-align: center; background-color:#0e2006;">
        <img src="<?php echo $baseurl ?>img/template/NoaGamesLogo.png" style="height: 40px; line-height: 40px; margin-top: 15px;">
    </div>
</div>
</body>
</html>