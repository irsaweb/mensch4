<?php

namespace App\Config;

/**
 * Class MenschConfigs
 */
class AppSettings
{
    const AGENT = 'HTTP_MENSCHAGENT';

    const TimeBetweenSpins = 21600;

    const CreateClanFee = 500;

    const VipInfo = [
        'day' => 7,
        'coin' => 1000
    ];

    const  TableEntranceCost = [
        'beginner' => [
            'coin' => 50
        ],
        'intermediate' => [
            'coin' => 1000
        ],
        'professional' => [
            'coin' => 15000
        ],
        'friendly' => [
            'coin' => 0
        ]
    ];

    const SpinnerInfo = [
        '0' => [
            'value' => 50,
            'chance' => 30
        ],
        '1' => [
            'value' => 100,
            'chance' => 20
        ],
        '2' => [
            'value' => 150,
            'chance' => 10
        ],
        '3' => [
            'value' => 200,
            'chance' => 10
        ],
        '4' => [
            'value' => 250,
            'chance' => 5
        ],
        '5' => [
            'value' => 300,
            'chance' => 5
        ],
        '6' => [
            'value' => 450,
            'chance' => 5
        ],
        '7' => [
            'value' => 600,
            'chance' => 5
        ],
        '8' => [
            'value' => 50,
            'chance' => 30
        ],
        '9' => [
            'value' => 100,
            'chance' => 20
        ],
        '10' => [
            'value' => 150,
            'chance' => 10
        ],
        '11' => [
            'value' => 200,
            'chance' => 10
        ],
        '12' => [
            'value' => 250,
            'chance' => 5
        ],
        '13' => [
            'value' => 300,
            'chance' => 5
        ],
        '14' => [
            'value' => 450,
            'chance' => 5
        ],
        '15' => [
            'value' => 600,
            'chance' => 5
        ]
    ];


    const AwardInfo = [
        'NoGroup' => [
            '4' => [
                'beginner' => [
                    '1' => [
                        'xp' => 30,
                        'trophy' => 24,
                        'coin' => 150
                    ],
                    '2' => [
                        'xp' => 10,
                        'trophy' => 12,
                        'coin' => 25
                    ],
                    '3' => [
                        'xp' => 5,
                        'trophy' => -5,
                        'coin' => 10
                    ],
                    '4' => [
                        'xp' => 0,
                        'trophy' => -7,
                        'coin' => 0
                    ]

                ],
                'intermediate' => [
                    '1' => [
                        'xp' => 50,
                        'trophy' => 36,
                        'coin' => 2000
                    ],
                    '2' => [
                        'xp' => 20,
                        'trophy' => 18,
                        'coin' => 500
                    ],
                    '3' => [
                        'xp' => 10,
                        'trophy' => -7,
                        'coin' => 100
                    ],
                    '4' => [
                        'xp' => 0,
                        'trophy' => -10,
                        'coin' => 0
                    ]
                ],
                'professional' => [
                    '1' => [
                        'xp' => 65,
                        'trophy' => 72,
                        'coin' => 25000
                    ],
                    '2' => [
                        'xp' => 35,
                        'trophy' => 36,
                        'coin' => 5000
                    ],
                    '3' => [
                        'xp' => 15,
                        'trophy' => -14,
                        'coin' => 1000
                    ],
                    '4' => [
                        'xp' => 0,
                        'trophy' => -20,
                        'coin' => 0
                    ]
                ],
                'friendly' => [
                    '1' => [
                        'xp' => 0,
                        'trophy' => 0,
                        'coin' => 0
                    ],
                    '2' => [
                        'xp' => 0,
                        'trophy' => 0,
                        'coin' => 0
                    ],
                    '3' => [
                        'xp' => 0,
                        'trophy' => 0,
                        'coin' => 0
                    ],
                    '4' => [
                        'xp' => 0,
                        'trophy' => -0,
                        'coin' => 0
                    ]
                ]
            ],
            '6' => [
                'beginner' => [
                    '1' => [
                        'xp' => 40,
                        'trophy' => 32,
                        'coin' => 150
                    ],
                    '2' => [
                        'xp' => 20,
                        'trophy' => 16,
                        'coin' => 50
                    ],
                    '3' => [
                        'xp' => 7,
                        'trophy' => 6,
                        'coin' => 20
                    ],
                    '4' => [
                        'xp' => 0,
                        'trophy' => -5,
                        'coin' => 0
                    ],
                    '5' => [
                        'xp' => 0,
                        'trophy' => -7,
                        'coin' => 0
                    ],
                    '6' => [
                        'xp' => 0,
                        'trophy' => -10,
                        'coin' => 0
                    ]

                ],
                'intermediate' => [
                    '1' => [
                        'xp' => 40,
                        'trophy' => 42,
                        'coin' => 2000
                    ],
                    '2' => [
                        'xp' => 30,
                        'trophy' => 21,
                        'coin' => 700
                    ],
                    '3' => [
                        'xp' => 12,
                        'trophy' => 14,
                        'coin' => 200
                    ],
                    '4' => [
                        'xp' => 0,
                        'trophy' => -7,
                        'coin' => 0
                    ],
                    '5' => [
                        'xp' => 0,
                        'trophy' => -10,
                        'coin' => 0
                    ],
                    '6' => [
                        'xp' => 0,
                        'trophy' => -14,
                        'coin' => 0
                    ]
                ],
                'professional' => [
                    '1' => [
                        'xp' => 40,
                        'trophy' => 84,
                        'coin' => 25000
                    ],
                    '2' => [
                        'xp' => 40,
                        'trophy' => 42,
                        'coin' => 10000
                    ],
                    '3' => [
                        'xp' => 17,
                        'trophy' => 28,
                        'coin' => 2000
                    ],
                    '4' => [
                        'xp' => 0,
                        'trophy' => -14,
                        'coin' => 0
                    ],
                    '5' => [
                        'xp' => 0,
                        'trophy' => -20,
                        'coin' => 0
                    ],
                    '6' => [
                        'xp' => 0,
                        'trophy' => -28,
                        'coin' => 0
                    ]
                ],

                'friendly' => [
                    '1' => [
                        'xp' => 0,
                        'trophy' => 0,
                        'coin' => 0
                    ],
                    '2' => [
                        'xp' => 0,
                        'trophy' => 0,
                        'coin' => 0
                    ],
                    '3' => [
                        'xp' => 0,
                        'trophy' => 0,
                        'coin' => 0
                    ],
                    '4' => [
                        'xp' => 0,
                        'trophy' => 0,
                        'coin' => 0
                    ],
                    '5' => [
                        'xp' => 0,
                        'trophy' => 0,
                        'coin' => 0
                    ],
                    '6' => [
                        'xp' => 0,
                        'trophy' => 0,
                        'coin' => 0
                    ]
                ]
            ]
        ],
        'TwoVsTwo' => [
            '4' => [
                'beginner' => [
                    '1' => [
                        'xp' => 30,
                        'trophy' => 24,
                        'coin' => 150
                    ],
                    '2' => [
                        'xp' => 10,
                        'trophy' => 0,
                        'coin' => 0
                    ]
                ],
                'intermediate' => [
                    '1' => [
                        'xp' => 50,
                        'trophy' => 36,
                        'coin' => 2000
                    ],
                    '2' => [
                        'xp' => 20,
                        'trophy' => 0,
                        'coin' => 0
                    ]
                ],
                'professional' => [
                    '1' => [
                        'xp' => 65,
                        'trophy' => 72,
                        'coin' => 25000
                    ],
                    '2' => [
                        'xp' => 35,
                        'trophy' => 0,
                        'coin' => 0
                    ]
                ],
                'friendly' => [
                    '1' => [
                        'xp' => 0,
                        'trophy' => 0,
                        'coin' => 0
                    ],
                    '2' => [
                        'xp' => 0,
                        'trophy' => 0,
                        'coin' => 0
                    ]
                ]
            ]

        ]
    ];

}
